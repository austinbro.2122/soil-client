module.exports = {
	setupFilesAfterEnv: ['./tests/setup.ts'],
	verbose: true,
	testMatch: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'],
	preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
	resetModules: true,
	resetMocks: true,
	moduleFileExtensions: ['js', 'ts', 'vue', 'd.ts'],
};
