import {
	SamplingCollection,
	Sampling,
	Sample,
	Location,
	LocationCollection,
	SoDepth,
	SoDepthValue,
	Area,
	Field,
	Address,
	ContactPoint,
	Stratification,
	Algorithm,
	StratificationInput,
	StratificationResult,
	ResourceMeta,
} from '@/store/types';
import { Point, Polygon } from 'geojson';

export function createMockResourceMeta({
	referenceIds,
	submissionId = '1',
	groupId = '2',
	submittedAt = new Date(0).toISOString(),
}: Partial<ResourceMeta> = {}): ResourceMeta {
	return referenceIds
		? {
				referenceIds,
				submissionId,
				groupId,
				submittedAt,
		  }
		: {
				submissionId,
				groupId,
				submittedAt,
		  };
}

function createMockAlgorithm({
	name = 'mock name',
	alternateName = 'mock alternateName',
	codeRepository = 'mock codeRepository',
	version = 'mock version',
	doi = 'mock doi',
} = {}): Algorithm {
	return {
		name,
		alternateName,
		codeRepository,
		version,
		doi,
	};
}

function createMockStratificationInput({ name = 'mock name', value = 'mock value' } = {}): StratificationInput {
	return {
		name,
		value,
	};
}

function createMockStratificationResult({ name = 'mock name', value = 'mock value' } = {}): StratificationResult {
	return {
		name,
		value,
	};
}

export function createMockStratification({
	id = 'mock id',
	name = 'mock name',
	agent = 'mock agent',
	dateCreated = 'mock dateCreated',
	provider = 'mock provider',
	object = 'mock object',
	featureOfInterest = 'mock featureOfInterest',
	algorithm = createMockAlgorithm(),
	input = [createMockStratificationInput()],
	result = [createMockStratificationResult()],
	meta = createMockResourceMeta(),
} = {}): Stratification {
	return {
		id,
		name,
		agent,
		dateCreated,
		provider,
		object,
		featureOfInterest,
		algorithm,
		input,
		result,
		meta,
	};
}

function createMockPoint({ coordinates = [0, 1] } = {}): Point {
	return {
		type: 'Point',
		coordinates,
	};
}

function createMockPolygon({
	coordinates = [
		[
			[40, 80],
			[41, 81],
		],
	],
} = {}): Polygon {
	return {
		type: 'Polygon',
		coordinates,
	};
}

export function createMockSoDepthValue({ value = 0, unit = 'CM' } = {}): SoDepthValue {
	return {
		value,
		unit,
	};
}

export function createMockSoDepth({
	minValue = createMockSoDepthValue({ value: 0 }),
	maxValue = createMockSoDepthValue({ value: 30 }),
} = {}): SoDepth {
	return {
		minValue,
		maxValue,
	};
}

export function createMockSample({
	id = 'mock id',
	name = 'mock sample name',
	sampleOf = 'mock samplingCollection id',
	results = ['mock result id'],
	resultOf = 'mock sampling id',
	soDepth = createMockSoDepth(),
} = {}): Sample {
	return {
		id,
		name,
		sampleOf,
		results,
		resultOf,
		soDepth,
	};
}

export function createMockSampling({
	name = 'mock sampling name',
	id = 'mock sampling id',
	resultTime = 'mock result time',
	geometry = createMockPoint(),
	featureOfInterest = 'mock location id',
	memberOf = 'mock sampling collection id',
	results = ['mock sample id'],
	comment = 'mock comment',
	procedures = ['mock procedure id'],
	properties = {},
	soDepth = createMockSoDepth(),
} = {}): Sampling {
	return {
		name,
		id,
		resultTime,
		geometry,
		featureOfInterest,
		memberOf,
		results,
		comment,
		procedures,
		properties,
		soDepth,
	};
}

export function createMockSamplingCollection({
	name = 'mock sampling collection name',
	id = 'mock samplingCollection id',
	comment = 'mock comment',
	creator = 'mock creator',
	featureOfInterest = 'mock feature of interest',
	participants = ['mock participant'],
	object = 'mock location collection id',
	members = ['mock sampling id'],
	soDepth = createMockSoDepth(),
	sampleDepths = [
		createMockSoDepth({
			minValue: createMockSoDepthValue({ value: 0 }),
			maxValue: createMockSoDepthValue({ value: 15 }),
		}),
		createMockSoDepth({
			minValue: createMockSoDepthValue({ value: 15 }),
			maxValue: createMockSoDepthValue({ value: 30 }),
		}),
	],
	dateCreated = 'mock dateCreated',
	dateModified = 'mock dateModified',
	meta = createMockResourceMeta(),
} = {}): SamplingCollection {
	return {
		name,
		id,
		comment,
		creator,
		featureOfInterest,
		participants,
		object,
		members,
		soDepth,
		sampleDepths,
		dateCreated,
		dateModified,
		meta,
	};
}

export function createMockLocation({
	id = 'mock location id',
	properties = {},
	geometry = createMockPoint(),
} = {}): Location {
	return {
		type: 'Feature',
		id,
		properties,
		geometry,
	};
}

export function createMockLocationCollection({
	id = 'mock location collection id',
	object = 'mock object',
	resultOf = 'mock resultOf',
	featureOfInterest = 'mock featureOfInterest',
	features = ['mock feature'],
} = {}): LocationCollection {
	return {
		id,
		object,
		resultOf,
		featureOfInterest,
		features,
	};
}

export function createMockArea({ id = 'mock area id', properties = {}, geometry = createMockPolygon() } = {}): Area {
	return {
		type: 'Feature',
		id,
		properties,
		geometry,
	};
}

export function createMockAddress({
	streetAddress = 'mock streetAddress',
	addressLocality = 'mock addressLocality',
	addressRegion = 'mock addressRegion',
	addressCountry = 'mock addressCountry',
	postalCode = 'mock postalCode',
} = {}): Address {
	return {
		streetAddress,
		addressLocality,
		addressRegion,
		addressCountry,
		postalCode,
	};
}

export function createMockContactPoint({
	telephone = 'mock telephone',
	email = 'mock email',
	name = 'mock name',
	contactType = 'mock contactType',
	organization = 'mock organization',
} = {}): ContactPoint {
	return {
		telephone,
		email,
		name,
		contactType,
		organization,
	};
}

export function createMockField({
	id = 'mock field id',
	name = 'mock field name',
	alternateName = 'mock alternate name',
	producerName = 'mock producer name',
	address = createMockAddress(),
	contactPoints = [createMockContactPoint()],
	areas = [] as string[],
	meta = createMockResourceMeta(),
} = {}): Field {
	return {
		id,
		name,
		alternateName,
		producerName,
		address,
		contactPoints,
		areas,
		meta,
	};
}
