import Vue from 'vue';
import Vuetify from 'vuetify';
import CompositionApi from '@vue/composition-api';
import '@testing-library/jest-dom';

window.URL.createObjectURL = function () {
	return '';
};

Date.prototype.getTimezoneOffset = function () {
	return 0;
};

Vue.use(Vuetify);
Vue.use(CompositionApi);
