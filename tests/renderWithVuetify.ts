import { render } from '@testing-library/vue';
import Vuetify from 'vuetify';

const renderWithVuetify: typeof render = function (component, options, callback) {
	const root = document.createElement('div');
	root.setAttribute('data-app', 'true');

	return render(
		component,
		{
			container: document.body.appendChild(root),
			// for Vuetify components that use the $vuetify instance property
			vuetify: new Vuetify(),
			...options,
		},
		callback
	);
};

export { renderWithVuetify };
