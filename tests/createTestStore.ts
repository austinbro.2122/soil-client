import { createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import createStore from '@/store';

function createTestStore() {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	const store = createStore();
	return store;
}

export default createTestStore;
