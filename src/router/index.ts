import Vue from 'vue';
import { Store } from 'vuex';
import VueRouter, { RouteConfig, NavigationGuard } from 'vue-router';
import multiguard from 'vue-router-multiguard';
import AppHeader from '@/components/common/AppHeader.vue';
import FieldsView from '@/views/field/FieldsView.vue';
import FieldShow from '@/views/field/FieldShow.vue';
import SamplingCollectionCreate from '@/views/sampling-collection/SamplingCollectionCreate.vue';
import SamplingCollectionEdit from '@/views/sampling-collection/SamplingCollectionEdit.vue';
import SamplingCollectionShow from '@/views/sampling-collection/SamplingCollectionShow.vue';
import SamplingCollectionList from '@/views/sampling-collection/SamplingCollectionList.vue';
import Debug from '@/views/debug/Debug.vue';
import Admin from '@/views/admin/Admin.vue';
import Login from '@/views/auth/Login.vue';
import Register from '@/views/auth/Register.vue';
import Profile from '@/views/auth/Profile.vue';
import UserEdit from '@/views/auth/UserEdit.vue';
import ForgotPassword from '@/views/auth/ForgotPassword.vue';
import ResetPassword from '@/views/auth/ResetPassword.vue';
import Invitation from '@/views/auth/Invitation.vue';
import { RootState } from '@/store/types';

Vue.use(VueRouter);

const createLoginRequiredGuard = (store: Store<RootState>): NavigationGuard => async (to, from, next) => {
	if (!store.getters['auth/isLoggedIn']) {
		next({ name: 'auth-login', params: { redirect: to.path } });
	} else {
		next();
	}
};

const createGetResourcesGuard = (store: Store<RootState>): NavigationGuard => async (to, from, next) => {
	store.dispatch('resources/getAllResources');
	next();
};

const createRoutes = ({
	loginRequiredGuard,
	getResourcesGuard,
}: {
	loginRequiredGuard: NavigationGuard;
	getResourcesGuard: NavigationGuard;
}): Array<RouteConfig> => [
	{ path: '/', redirect: '/fields' },
	{
		path: '/fields',
		name: 'Fields',
		components: {
			default: FieldsView,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Fields',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/field/:id',
		name: 'Field Show',
		components: {
			default: FieldShow,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Field',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/create-sampling-collection',
		name: 'Sampling Collection Create',
		components: {
			default: SamplingCollectionCreate,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Collect New',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/sampling-collection/:samplingCollectionId/edit',
		name: 'Sampling Collection Edit',
		components: {
			default: SamplingCollectionEdit,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Collect',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/sampling-collection/:id',
		name: 'Sampling Collection Show',
		components: {
			default: SamplingCollectionShow,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Sampling Collection',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/sampling-collection/',
		name: 'Sampling Collection List',
		components: {
			default: SamplingCollectionList,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Sampling Collections',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/debug',
		name: 'Debug',
		components: {
			default: Debug,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Debug Options',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/admin',
		name: 'Admin',
		components: {
			default: Admin,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Administration',
			},
		},
		beforeEnter: multiguard([loginRequiredGuard, getResourcesGuard]),
	},
	{
		path: '/auth/login',
		name: 'auth-login',
		components: {
			default: Login,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Login',
			},
		},
	},
	{
		path: '/auth/register',
		name: 'auth-register',
		components: {
			default: Register,
			appHeader: AppHeader,
		},
		props: {
			default: true,
			appHeader: {
				title: 'Register',
			},
		},
	},
	{
		path: '/auth/profile',
		name: 'auth-profile',
		components: {
			default: Profile,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Profile',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/auth/forgot-password',
		name: 'auth-forgot-password',
		components: {
			default: ForgotPassword,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Forgot Password',
			},
		},
	},
	{
		path: '/auth/reset-password',
		name: 'auth-reset-password',
		components: {
			default: ResetPassword,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Reset Password',
			},
		},
	},
	{
		path: '/users/:id/edit',
		name: 'users-edit',
		components: {
			default: UserEdit,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Profile',
			},
		},
		beforeEnter: loginRequiredGuard,
	},
	{
		path: '/invitations',
		name: 'invitations',
		components: {
			default: Invitation,
			appHeader: AppHeader,
		},
		props: {
			appHeader: {
				title: 'Invitation',
			},
		},
	},
];

export default function createRouter(store: Store<RootState>) {
	const loginRequiredGuard = createLoginRequiredGuard(store);
	const getResourcesGuard = createGetResourcesGuard(store);
	return new VueRouter({
		mode: 'history',
		base: process.env.BASE_URL,
		routes: createRoutes({ loginRequiredGuard, getResourcesGuard }),
	});
}
