// interface Area {
// 	dateCreated: string
// }

/**
 * Users:
 *  - Project Manager (0)
 *  - Provider / Farmer (1)
 *  - Stratifier (2)
 *  - Sampler (3)
 *  - Lab tech (4)
 */

// // https://schema.org/GeoShape
// const area0 = {
// 	id: 'https://esmcportal.com/area/0',
// 	polygon: ''
// 	name:
// }

// GeoJSON Feature
const area0 = {
	id: 'https://esmcportal.com/area/0',
	type: 'Feature',
	geometry: {
		type: 'Polygon',
		coordinates: [
			[
				[-84.55068956887425, 39.08187355127035],
				[-84.55264041478657, 39.07970296943185],
				[-84.55329069675669, 39.075109192314756],
				[-84.54795838459823, 39.07041414405805],
				[-84.54249601604481, 39.07319082324054],
				[-84.54360149539491, 39.07697702788229],
				[-84.55068956887425, 39.08187355127035],
			],
		],
	},
	properties: {
		creator: 'https://esmcportal.com/user/1',
		name: 'Provider Boundary',
		dateCreated: '2020-10-05T15:44:10.499Z',
		featureOfInterest: 'https://esmcportal.com/field/0',
	},
};

const area1 = {
	id: 'https://esmcportal.com/area/1',
	type: 'Feature',
	geometry: {
		type: 'Polygon',
		coordinates: [
			[
				[-84.55078956887425, 39.081773551270345],
				[-84.55274041478657, 39.079602969431846],
				[-84.5533906967567, 39.07500919231475],
				[-84.54805838459824, 39.070314144058045],
				[-84.54259601604481, 39.073090823240534],
				[-84.54370149539491, 39.07687702788229],
				[-84.55078956887425, 39.081773551270345],
			],
		],
	},
	properties: {
		name: 'Stratifier Boundary',
		creator: 'https://esmcportal.com/user/2', // Stratifier
		dateCreated: '2020-10-05T15:44:10.499Z',
		featureOfInterest: 'https://esmcportal.com/field/0',
	},
};

export const areas = [area0, area1];

// https://schema.org/Place
// https://schema.org/LocalBusiness
const field0 = {
	id: 'https://esmcportal.com/field/0',
	name: 'Field 0',
	meta: {
		dateCreated: '2020-10-05T15:44:10.499Z',
		creator: 'https://esmcportal.com/user/0',
	},
	address: {
		// '@type': 'PostalAddress',
		streetAddress: '1234 Winding Way',
		addressLocality: 'West Roxbury',
		addressRegion: 'MA',
		postalCode: '02132',
	},
	contactPoint: [
		{
			telephone: '+1234567890',
			email: 'dave@example.com',
			name: 'Dave Smith',
			contactType: 'farm owner',
		},
	],
	// Place would typically have a `geo` attribute but it doesn't feel like it makes sense here
	area: [
		// array of https://schema.org/GeoShape or GeoJSON FeatureCollection of
		'https://esmcportal.com/area/0',
		'https://esmcportal.com/area/1',
	],
};

export const fields = [field0];

const stratificationAction0 = {
	id: 'https://esmcportal.com/stratification/0',
	name: 'OSPATS+ for Field 0',
	agent: 'https://esmcportal.com/user/2',
	dateCreated: '2020-09-23T17:47:52.276Z',
	provider: {
		url: 'https://stratification-service.oursci.com/v1/',
		version: '1.0.1',
		name: 'Our Sci ESMC Stratification Service',
	},
	// Should this contain a reference to the field?
	object: 'https://esmcportal.com/area/1',
	algorithm: {
		// object or array
		// '@type': 'https://schema.org/SoftwareSourceCode'
		name: 'Constrained Latin Hypercube Sampling',
		alternateName: 'CLHS',
		codeRepository: 'https://github.com/pierreroudier/clhs/',
		version: '1.0.2',
		doi: '',
	},
	input: [
		{
			name: 'Area',
			value: 'https://esmcportal.com/area/1',
		},
		{
			name: 'PseudoRandomSeed',
			value: 2345,
		},
		{
			name: 'Layer',
			value: [
				{
					//'@type': 'DataCatalog',
					//'@type': 'Dataset',
					name: 'GSSURGO',
					id: 'https://doi.org/10.15482/USDA.ADC/1255234',
				},
				{
					name: 'LayerLandSat5',
					id: 'https://doi.org/10.5066/F7N015TQ',
				},
			],
		},
	],

	// Should we have a separate `outputs` key, for general outputs, like number of samples
	// and use the `result` key for just sampling locations and cost estimate?
	result: [
		{
			name: 'Strata',
			value: 6,
		},
		{
			name: 'SampleSize',
			value: '58',
		},
		{
			name: 'SampleSizeInStrata',
			value: [15, 3, 9, 19, 4, 8],
		},
		{
			name: 'GridStrataSize',
			value: [1085, 220, 701, 1219, 454, 702],
		},
		{
			name: 'SamplingLocations',
			value: 'https://esmcportal.com/location-collection/0',
		},
		{
			name: 'CostEstimate',
			value: 'https://esmcportal.com/estimate/0',
		},
	],
};

const stratificationAction1 = {
	id: 'https://esmcportal.com/stratification/1',
	name: 'cLHS for Field 0',
	agent: 'https://esmcportal.com/user/2',
	dateCreated: '2020-09-23T17:47:52.276Z',
	provider: {
		url: 'https://stratification-service.oursci.com/v1/',
		version: '1.0.1',
		name: 'Our Sci ESMC Stratification Service',
	},
	// Should this contain a reference to the field?
	object: 'https://esmcportal.com/area/1',
	algorithm: {
		// object or array
		// '@type': 'https://schema.org/SoftwareSourceCode'
		name: 'Constrained Latin Hypercube Sampling',
		alternateName: 'CLHS',
		codeRepository: 'https://github.com/pierreroudier/clhs/',
		version: '1.0.2',
		doi: '',
	},
	input: [
		{
			name: 'Area',
			value: 'https://esmcportal.com/area/1',
		},
		{
			name: 'PseudoRandomSeed',
			value: 2345,
		},
		{
			name: 'Layer',
			value: [
				{
					//'@type': 'DataCatalog',
					//'@type': 'Dataset',
					name: 'GSSURGO',
					id: 'https://doi.org/10.15482/USDA.ADC/1255234',
				},
				{
					name: 'LayerLandSat5',
					id: 'https://doi.org/10.5066/F7N015TQ',
				},
			],
		},
	],

	// Should we have a separate `outputs` key, for general outputs, like number of samples
	// and use the `result` key for just sampling locations and cost estimate?
	result: [
		{
			name: 'Strata',
			value: 6,
		},
		{
			name: 'SampleSize',
			value: '58',
		},
		{
			name: 'SampleSizeInStrata',
			value: [15, 3, 9, 19, 4, 8],
		},
		{
			name: 'GridStrataSize',
			value: [1085, 220, 701, 1219, 454, 702],
		},
		{
			name: 'SamplingLocations',
			value: 'https://esmcportal.com/location-collection/1',
		},
		{
			name: 'CostEstimate',
			value: 'https://esmcportal.com/estimate/1',
		},
	],
};

const stratifications = [stratificationAction0, stratificationAction1];

const locations = [
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54925262935322, 39.080062092491204],
		},
		id: 'https://esmcportal.com/location/0',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54780445316865, 39.074852480848776],
		},
		id: 'https://esmcportal.com/location/1',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.5447402433226, 39.07335664029616],
		},
		id: 'https://esmcportal.com/location/2',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54705658466001, 39.07430741338497],
		},
		id: 'https://esmcportal.com/location/3',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54710212818028, 39.071980660486744],
		},
		id: 'https://esmcportal.com/location/4',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.55041677248897, 39.07766625340349],
		},
		id: 'https://esmcportal.com/location/5',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54499101362738, 39.07681119760934],
		},
		id: 'https://esmcportal.com/location/6',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.5518122272932, 39.07791986865453],
		},
		id: 'https://esmcportal.com/location/7',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54934545769957, 39.07258900813801],
		},
		id: 'https://esmcportal.com/location/8',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54978102724984, 39.0740760233925],
		},
		id: 'https://esmcportal.com/location/9',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54643369960691, 39.07484952242388],
		},
		id: 'https://esmcportal.com/location/10',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54879282133076, 39.07224256586949],
		},
		id: 'https://esmcportal.com/location/11',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.55103623581746, 39.07894477382995],
		},
		id: 'https://esmcportal.com/location/12',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54433013778934, 39.07256257509835],
		},
		id: 'https://esmcportal.com/location/13',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54540936625273, 39.07788658040647],
		},
		id: 'https://esmcportal.com/location/14',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54901371430896, 39.07815397603323],
		},
		id: 'https://esmcportal.com/location/15',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54913961970567, 39.077886875590735],
		},
		id: 'https://esmcportal.com/location/16',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.55199580618817, 39.074590580627934],
		},
		id: 'https://esmcportal.com/location/17',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54504659761572, 39.07772826455198],
		},
		id: 'https://esmcportal.com/location/18',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54966156134117, 39.077690982747306],
		},
		id: 'https://esmcportal.com/location/19',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.5447180403234, 39.076868356712374],
		},
		id: 'https://esmcportal.com/location/20',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54981341659354, 39.079643981918395],
		},
		id: 'https://esmcportal.com/location/21',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.54461135693192, 39.07576239639783],
		},
		id: 'https://esmcportal.com/location/22',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.55148688480524, 39.07823930269823],
		},
		id: 'https://esmcportal.com/location/23',
	},
	{
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [-84.55005539687477, 39.07654571956119],
		},
		id: 'https://esmcportal.com/location/24',
	},
];

const locationCollections = [
	{
		id: 'https://esmcportal.com/location-collection/0',
		object: 'https://esmcportal.com/area/1',
		resultOf: 'https://esmcportal.com/stratification/0',
		featureOfInterest: 'https://esmcportal.com/field/0',
		features: [
			'https://esmcportal.com/location/0',
			'https://esmcportal.com/location/1',
			'https://esmcportal.com/location/2',
			'https://esmcportal.com/location/3',
			'https://esmcportal.com/location/4',
			'https://esmcportal.com/location/5',
			'https://esmcportal.com/location/6',
			'https://esmcportal.com/location/7',
			'https://esmcportal.com/location/8',
			'https://esmcportal.com/location/9',
			'https://esmcportal.com/location/10',
			'https://esmcportal.com/location/11',
			'https://esmcportal.com/location/12',
			'https://esmcportal.com/location/13',
			'https://esmcportal.com/location/14',
			'https://esmcportal.com/location/15',
			'https://esmcportal.com/location/16',
			'https://esmcportal.com/location/17',
			'https://esmcportal.com/location/18',
			'https://esmcportal.com/location/19',
			'https://esmcportal.com/location/20',
			'https://esmcportal.com/location/21',
			'https://esmcportal.com/location/22',
			'https://esmcportal.com/location/23',
			'https://esmcportal.com/location/24',
		],
	},
	{
		id: 'https://esmcportal.com/location-collection/1',
		object: 'https://esmcportal.com/area/1',
		resultOf: 'https://esmcportal.com/stratification/1',
		featureOfInterest: 'https://esmcportal.com/field/0',
		features: [
			'https://esmcportal.com/location/12',
			'https://esmcportal.com/location/13',
			'https://esmcportal.com/location/14',
			'https://esmcportal.com/location/15',
			'https://esmcportal.com/location/16',
			'https://esmcportal.com/location/17',
			'https://esmcportal.com/location/18',
			'https://esmcportal.com/location/19',
			'https://esmcportal.com/location/20',
			'https://esmcportal.com/location/21',
			'https://esmcportal.com/location/22',
			'https://esmcportal.com/location/23',
			'https://esmcportal.com/location/24',
		],
	},
];

/**
 * SamplingCollection
 * 	Sampling
 * 		Sample
 */

// TODO: Stratification Estimate
// TODO: ~~Lab Results~~
// TODO: ~~SamplingCollection~~
// TODO: ~~~~Sampling~~~~ should we skip this?
// TODO: ~~Sample~~

const samplings = [
	{
		name: 'Sampling at Location 0',
		id: 'https://esmcportal.com/sampling/0',
		resultTime: '1987-06-29T00:00:00.000',
		geometry: {
			type: 'Point',
			coordinates: [-84.5492527, 39.0800621],
		},
		soDepth: {
			// seems like this should go on the sampling, maybe this should be inside a propery
			minValue: {
				value: 0,
				unit: 'unit:CM',
			},
			maxValue: {
				value: 30,
				unit: 'unit:CM',
			},
		},
		comment: '',
		featureOfInterest: 'https://esmcportal.com/location/0',
		result: ['https://esmcportal.com/sample/0', 'https://esmcportal.com/sample/1', 'https://esmcportal.com/sample/2'],
		procedure: 'https://example.com/procedure/0',
	},
];

const samplingCollection0 = {
	name: 'December 2021 Sampling Collection',
	id: 'https://esmcportal.com/sampling-collection/0',
	comment: '',
	creator: 'https://esmcportal.com/user/0',
	participant: ['https://esmcportal.com/user/0', 'https://esmcportal.com/user/1'],
	featureOfInterest: 'https://esmcportal.com/field/0',
	object: 'https://esmcportal.com/location-collection/0',
	member: [
		// Should these be the samplings instead of samples? => Probably the samplings
		'https://esmcportal.com/sampling/0',
		// 'https://esmcportal.com/sample/0',
		// 'https://esmcportal.com/sample/1',
		// 'https://esmcportal.com/sample/2',
		// 'https://esmcportal.com/sample/3',
		// 'https://esmcportal.com/sample/4',
		// 'https://esmcportal.com/sample/5',
	],
	// object: [
	// 	// is the object the field, the area, the location collection, or the stratification?
	// 	{
	// 		type: 'LocationCollection',
	// 		id: 'https://esmcportal.com/location-collection/0',
	// 	},
	// 	{
	// 		type: 'Area',
	// 		id: 'https://esmcportal.com/area/1',
	// 	},
	// 	{
	// 		type: 'Field',
	// 		id: 'https://esmcportal.com/field/0',
	// 	},
	// ],
};

const samplingCollections = [samplingCollection0];

const samples = [
	{
		name: 'SB10000A',
		id: 'https://esmcportal.com/sample/0',
		sampleOf: 'https://esmcportal.com/field/0',
		result: 'https://esmcportal.com/lab-result/0',
		resultOf: 'https://esmcportal.com/sampling/0',
		// resultTime: '1987-06-29T00:00:00.000', // This should go on Sampling
		// geometry: {
		// 	// This should go on Sampling
		// 	type: 'Point',
		// 	coordinates: [-84.5492527, 39.0800621],
		// },
		// featureOfInterest: 'https://esmcportal.com/location/0', // This should go on Sampling
		soDepth: {
			minValue: {
				value: 0,
				unit: 'unit:CM',
			},
			maxValue: {
				value: 10,
				unit: 'unit:CM',
			},
		},
	},
];

const labResults = [
	// Observations
	{
		id: 'https://esmcportal.com/lab-result/0',
		name: 'Combustion of SB1000A',
		procedure: 'https://example.com/lab-procedure/0',
		observedProperty: 'https://example.com/property/carbon-mass',
		featureOfInterest: 'https://esmcportal.com/sample/0',
		result: {
			value: 0.125,
			unit: 'unit:g',
		},
		resultTime: '1987-06-29T00:00:00.000',
		phenomenonTime: '1987-06-29T00:00:00.000',
	},
];

const labResultCollections = [
	{
		id: 'https://esmcportal.com/lab-result-collection/0',
		name: 'Soil Analysis for Field 0',
		member: ['https://esmcportal.com/lab-result/0'],
	},
];

const projects = [
	{
		name: 'Project 0',
		id: 'https://esmcportal.com/project/0',
		description: '',
		contactPoint: [
			{
				telephone: '+1234567890',
				email: 'john@example.com',
				name: 'John Smith',
				contactType: 'project manager',
			},
		],
		participant: ['https://esmcportal.com/user/0', 'https://esmcportal.com/user/1'],
		object: ['https://esmcportal.com/field/0'],
	},
];

const producers = [
	{
		id: 'https://esmcportal.com/user/0',
		fields: ['https://esmcportal.com/field/0'],
	},
];

export default {
	// '@context': {
	// 	id: '@id',
	// },
	// dateCreated: '',
	// creator: '',
	projects,
	producers,
	fields,
	areas,
	stratifications,
	locations,
	locationCollections,
	samples,
	samplings,
	samplingCollections,
	labResults,
	labResultCollections,
};
