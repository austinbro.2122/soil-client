const locationCollection1 = {
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ05',
			properties: {
				// label: '0',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ05',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.65746161537712, 38.13002492488401],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ06',
			properties: {
				// label: '1',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ06',
			},
			geometry: {
				type: 'Point',
				coordinates: [-121.05829801379134, 37.81629606577075],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ07',
			properties: {
				// label: '2',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ07',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.61703676782135, 37.7357241350753],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ08',
			properties: {
				// label: '3',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ08',
			},
			geometry: {
				type: 'Point',
				coordinates: [-119.91390875993886, 37.57821840591237],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ09',
			properties: {
				// label: '4',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ09',
			},
			geometry: {
				type: 'Point',
				coordinates: [-119.64601421491118, 37.86621546028642],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ0+',
			properties: {
				// label: '5',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ0+',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.412752989259, 37.638605926703015],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ0/',
			properties: {
				// label: '6',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ0/',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.75271485380483, 37.62906918844378],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1A',
			properties: {
				// label: '7',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1A',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.89854342482975, 37.54958728925132],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1B',
			properties: {
				// label: '8',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1B',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.66896472487349, 37.629812277755526],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1C',
			properties: {
				// label: '9',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1C',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.19235197205546, 38.06621784974456],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1D',
			properties: {
				// label: '10',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1D',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.46401636216451, 37.29281687369119],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1E',
			properties: {
				// label: '11',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1E',
			},
			geometry: {
				type: 'Point',
				coordinates: [-121.01837627663734, 37.857639258716546],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1F',
			properties: {
				// label: '12',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1F',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.3054892460227, 38.05045297973783],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1G',
			properties: {
				// label: '13',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1G',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.76137517412737, 37.670148763116856],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1H',
			properties: {
				// label: '14',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1H',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.71948429170561, 38.05901871268068],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1I',
			properties: {
				// label: '15',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1I',
			},
			geometry: {
				type: 'Point',
				coordinates: [-119.65800420421292, 37.98016802520298],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1J',
			properties: {
				// label: '16',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1J',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.4219125924259, 38.07498500937788],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1K',
			properties: {
				// label: '17',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1K',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.82704826409358, 37.78700860206555],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1L',
			properties: {
				// label: '18',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1L',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.89542720528142, 37.513798284422485],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1M',
			properties: {
				// label: '19',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1M',
			},
			geometry: {
				type: 'Point',
				coordinates: [-119.937438958976, 37.98783195044137],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1N',
			properties: {
				// label: '20',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1N',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.44000821851898, 37.59186240003619],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1O',
			properties: {
				// label: '21',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1O',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.44551304712536, 37.38836521219901],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1P',
			properties: {
				// label: '22',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1P',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.83638452069232, 37.98892570026524],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1Q',
			properties: {
				// label: '23',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1Q',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.30980509415168, 37.446505704952095],
			},
		},
		{
			type: 'Feature',
			id: 'XYbToXGH9W/tKZ1R',
			properties: {
				// label: '24',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbToXGH9W/tKZ1R',
			},
			geometry: {
				type: 'Point',
				coordinates: [-120.37253014478962, 37.992725348423285],
			},
		},
	],
};

const locationCollection2 = {
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltp',
			properties: {
				// label: '0',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltp',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.28350067666945, 39.46212723452533],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltq',
			properties: {
				// label: '1',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltq',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.26171655600642, 39.03062408613256],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltr',
			properties: {
				// label: '2',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltr',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.49768536790249, 39.757814167244064],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlts',
			properties: {
				// label: '3',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlts',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.52177334334893, 39.03163319285841],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltt',
			properties: {
				// label: '4',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltt',
			},
			geometry: {
				type: 'Point',
				coordinates: [-123.00848383132139, 39.59634409047942],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltu',
			properties: {
				// label: '5',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltu',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.66811292006783, 39.430786823817314],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltv',
			properties: {
				// label: '6',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltv',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.55729568457066, 39.62888137976534],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltw',
			properties: {
				// label: '7',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltw',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.55175403849829, 39.83643754333679],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltx',
			properties: {
				// label: '8',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltx',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.55328539175444, 39.57647424788751],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlty',
			properties: {
				// label: '9',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlty',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.88558997288922, 39.34370656660985],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nltz',
			properties: {
				// label: '10',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nltz',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.35122775859152, 39.122819755056284],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt0',
			properties: {
				// label: '11',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt0',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.38197682091943, 39.7086004701415],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt1',
			properties: {
				// label: '12',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt1',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.35548881771628, 39.21634416068161],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt2',
			properties: {
				// label: '13',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt2',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.49671175082433, 39.44134563048453],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt3',
			properties: {
				// label: '14',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt3',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.50592885641743, 39.53023784128188],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt4',
			properties: {
				// label: '15',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt4',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.19205350451381, 39.02007587334336],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt5',
			properties: {
				// label: '16',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt5',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.56874059123697, 39.56809246434962],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt6',
			properties: {
				// label: '17',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt6',
			},
			geometry: {
				type: 'Point',
				coordinates: [-121.99888187881423, 39.15748418861727],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt7',
			properties: {
				// label: '18',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt7',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.00111544673088, 39.13837902783829],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt8',
			properties: {
				// label: '19',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt8',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.68604484095903, 39.18141566256271],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt9',
			properties: {
				// label: '20',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt9',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.41015237432656, 39.52065605535456],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt+',
			properties: {
				// label: '21',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt+',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.21350653359609, 39.641355602131235],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0Nlt/',
			properties: {
				// label: '22',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0Nlt/',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.35276646644485, 39.422143962337685],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0NluA',
			properties: {
				// label: '23',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0NluA',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.55949949798926, 39.241366359087934],
			},
		},
		{
			type: 'Feature',
			id: 'XYbTOaEalZh0NluB',
			properties: {
				// label: '24',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbTOaEalZh0NluB',
			},
			geometry: {
				type: 'Point',
				coordinates: [-122.66067710048291, 39.83286298036715],
			},
		},
	],
};

const locationCollection3 = {
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR1w',
			properties: {
				// label: '0',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR1w',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5134040109584, 39.13937683755256],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR1x',
			properties: {
				// label: '1',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR1x',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5119994548847, 39.138108456278815],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR1y',
			properties: {
				// label: '2',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR1y',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51298420260268, 39.1393676702951],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR1z',
			properties: {
				// label: '3',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR1z',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51149168483644, 39.13889086430062],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR10',
			properties: {
				// label: '4',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR10',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51216467990066, 39.138805321078536],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR11',
			properties: {
				// label: '5',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR11',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51244748117047, 39.13863440552873],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR12',
			properties: {
				// label: '6',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR12',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51116771985691, 39.13893214667022],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR13',
			properties: {
				// label: '7',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR13',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5118783599342, 39.13789365630685],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR14',
			properties: {
				// label: '8',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR14',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51363370848219, 39.13881774196902],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR15',
			properties: {
				// label: '9',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR15',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51330377256228, 39.139410041544224],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR16',
			properties: {
				// label: '10',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR16',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51110199380385, 39.13896756089734],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR17',
			properties: {
				// label: '11',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR17',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51381774924745, 39.1387697782856],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR18',
			properties: {
				// label: '12',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR18',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51426169685489, 39.138196112039836],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR19',
			properties: {
				// label: '13',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR19',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5143374495571, 39.1384632112133],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR1+',
			properties: {
				// label: '14',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR1+',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51180641060412, 39.13926236180337],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR1/',
			properties: {
				// label: '15',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR1/',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51314825785252, 39.13935710807519],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2A',
			properties: {
				// label: '16',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2A',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51283903396993, 39.13792328942438],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2B',
			properties: {
				// label: '17',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2B',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51344790271207, 39.13861046079495],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2C',
			properties: {
				// label: '18',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2C',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5135193303347, 39.13792066794858],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2D',
			properties: {
				// label: '19',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2D',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51416240830818, 39.13788835451959],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2E',
			properties: {
				// label: '20',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2E',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5114211314232, 39.138436345409126],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2F',
			properties: {
				// label: '21',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2F',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51379556430174, 39.13781192243478],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2G',
			properties: {
				// label: '22',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2G',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51254892295411, 39.13854527924339],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2H',
			properties: {
				// label: '23',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2H',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51245659802062, 39.13928670589144],
			},
		},
		{
			type: 'Feature',
			id: 'XYbSjY5bi0QOMR2I',
			properties: {
				// label: '24',
				// type: 'SURVEY_LOCATION_MARKER',
				// id: 'XYbSjY5bi0QOMR2I',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.51079244145033, 39.13807597811326],
			},
		},
	],
};

export const locationCollection4 = {
	id: '5f72011d498b619a6497337f',
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973366',
				// label: '0',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54925262935322, 39.080062092491204],
			},
			id: '5f720090498b619a64973366',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973367',
				// label: '1',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54780445316865, 39.074852480848776],
			},
			id: '5f720090498b619a64973367',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973368',
				// label: '2',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5447402433226, 39.07335664029616],
			},
			id: '5f720090498b619a64973368',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973369',
				// label: '3',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54705658466001, 39.07430741338497],
			},
			id: '5f720090498b619a64973369',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497336a',
				// label: '4',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54710212818028, 39.071980660486744],
			},
			id: '5f720090498b619a6497336a',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497336b',
				// label: '5',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.55041677248897, 39.07766625340349],
			},
			id: '5f720090498b619a6497336b',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497336c',
				// label: '6',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54499101362738, 39.07681119760934],
			},
			id: '5f720090498b619a6497336c',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497336d',
				// label: '7',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5518122272932, 39.07791986865453],
			},
			id: '5f720090498b619a6497336d',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497336e',
				// label: '8',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54934545769957, 39.07258900813801],
			},
			id: '5f720090498b619a6497336e',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497336f',
				// label: '9',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54978102724984, 39.0740760233925],
			},
			id: '5f720090498b619a6497336f',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973370',
				// label: '10',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54643369960691, 39.07484952242388],
			},
			id: '5f720090498b619a64973370',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973371',
				// label: '11',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54879282133076, 39.07224256586949],
			},
			id: '5f720090498b619a64973371',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973372',
				// label: '12',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.55103623581746, 39.07894477382995],
			},
			id: '5f720090498b619a64973372',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973373',
				// label: '13',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54433013778934, 39.07256257509835],
			},
			id: '5f720090498b619a64973373',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973374',
				// label: '14',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54540936625273, 39.07788658040647],
			},
			id: '5f720090498b619a64973374',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973375',
				// label: '15',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54901371430896, 39.07815397603323],
			},
			id: '5f720090498b619a64973375',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973376',
				// label: '16',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54913961970567, 39.077886875590735],
			},
			id: '5f720090498b619a64973376',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973377',
				// label: '17',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.55199580618817, 39.074590580627934],
			},
			id: '5f720090498b619a64973377',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973378',
				// label: '18',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54504659761572, 39.07772826455198],
			},
			id: '5f720090498b619a64973378',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a64973379',
				// label: '19',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54966156134117, 39.077690982747306],
			},
			id: '5f720090498b619a64973379',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497337a',
				// label: '20',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.5447180403234, 39.076868356712374],
			},
			id: '5f720090498b619a6497337a',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497337b',
				// label: '21',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54981341659354, 39.079643981918395],
			},
			id: '5f720090498b619a6497337b',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497337c',
				// label: '22',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.54461135693192, 39.07576239639783],
			},
			id: '5f720090498b619a6497337c',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497337d',
				// label: '23',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.55148688480524, 39.07823930269823],
			},
			id: '5f720090498b619a6497337d',
		},
		{
			type: 'Feature',
			properties: {
				// id: '5f720090498b619a6497337e',
				// label: '24',
				// type: 'SURVEY_LOCATION_MARKER',
			},
			geometry: {
				type: 'Point',
				coordinates: [-84.55005539687477, 39.07654571956119],
			},
			id: '5f720090498b619a6497337e',
		},
	],
};

export const locationCollection5 = {
	id: '5f72011d498b619a6497337f',
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54925262935322, 39.080062092491204],
			},
			id: '5f720090498b619a64973366',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54780445316865, 39.074852480848776],
			},
			id: '5f720090498b619a64973367',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.5447402433226, 39.07335664029616],
			},
			id: '5f720090498b619a64973368',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54705658466001, 39.07430741338497],
			},
			id: '5f720090498b619a64973369',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54710212818028, 39.071980660486744],
			},
			id: '5f720090498b619a6497336a',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.55041677248897, 39.07766625340349],
			},
			id: '5f720090498b619a6497336b',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54499101362738, 39.07681119760934],
			},
			id: '5f720090498b619a6497336c',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.5518122272932, 39.07791986865453],
			},
			id: '5f720090498b619a6497336d',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54934545769957, 39.07258900813801],
			},
			id: '5f720090498b619a6497336e',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54978102724984, 39.0740760233925],
			},
			id: '5f720090498b619a6497336f',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54643369960691, 39.07484952242388],
			},
			id: '5f720090498b619a64973370',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54879282133076, 39.07224256586949],
			},
			id: '5f720090498b619a64973371',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.55103623581746, 39.07894477382995],
			},
			id: '5f720090498b619a64973372',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54433013778934, 39.07256257509835],
			},
			id: '5f720090498b619a64973373',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54540936625273, 39.07788658040647],
			},
			id: '5f720090498b619a64973374',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54901371430896, 39.07815397603323],
			},
			id: '5f720090498b619a64973375',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54913961970567, 39.077886875590735],
			},
			id: '5f720090498b619a64973376',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.55199580618817, 39.074590580627934],
			},
			id: '5f720090498b619a64973377',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54504659761572, 39.07772826455198],
			},
			id: '5f720090498b619a64973378',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54966156134117, 39.077690982747306],
			},
			id: '5f720090498b619a64973379',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.5447180403234, 39.076868356712374],
			},
			id: '5f720090498b619a6497337a',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54981341659354, 39.079643981918395],
			},
			id: '5f720090498b619a6497337b',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.54461135693192, 39.07576239639783],
			},
			id: '5f720090498b619a6497337c',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.55148688480524, 39.07823930269823],
			},
			id: '5f720090498b619a6497337d',
		},
		{
			type: 'Feature',
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-84.55005539687477, 39.07654571956119],
			},
			id: '5f720090498b619a6497337e',
		},
	],
};

// Should this be a Feature or FeatureCollection?
// Feature can support Polygon or MultiPolygon which covers a single polygon (optionally with holes) and multiple polygons (optionally holes)
// FeatureCollection only adds support for adding properties for each polygon, which could be potentially useful
//  for example if we want to add labels to each polygon
const field0 = {
	type: 'Feature',
	id: '381c45d617841091661078bfc5e3d541',
	properties: {
		handle: 'asdf',
		organization: 1,
		name: 'asdf',
		depths: ['0_TO_10_CM', '10_TO_20_CM', '20_TO_30_CM'],
		hasAdditionalQuestions: true,
		email: 'asdf@asfd.com',
		createdAt: 1601306752888,
	},
	geometry: {
		coordinates: [
			[
				[-84.55068956887425, 39.08187355127035],
				[-84.55264041478657, 39.07970296943185],
				[-84.55329069675669, 39.075109192314756],
				[-84.54795838459823, 39.07041414405805],
				[-84.54249601604481, 39.07319082324054],
				[-84.54360149539491, 39.07697702788229],
				[-84.55068956887425, 39.08187355127035],
			],
			// [
			//   [-84.551876, 39.077692],
			//   [-84.554665, 39.07481],
			//   [-84.554107, 39.072028],
			//   [-84.546576, 39.072711],
			//   [-84.54664, 39.075627],
			//   [-84.551876, 39.077692]
			// ]
		],
		type: 'Polygon',
	},
};

export const fields = [field0];
export const locationCollections = [locationCollection5, locationCollection1, locationCollection2, locationCollection3];
