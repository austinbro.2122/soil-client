/* eslint-disable @typescript-eslint/no-var-requires */
const ObjectID = require('bson-objectid');
const boundary = require('./field_features_2');
const fs = require('fs');
const { bbox, pointsWithinPolygon, randomPoint, Polygon } = require('@turf/turf');

// TODO: add density option, check polygon area
function randomPointsInPolygon(polygon: typeof Polygon, count: number) {
	const polygonBbox = bbox(polygon);
	const pointsInPolygon = [];
	let remainder = Number(String(count));
	while (pointsInPolygon.length < count) {
		const points = randomPoint(remainder, { bbox: polygonBbox });
		const ptsInPoly = pointsWithinPolygon(points, polygon);
		pointsInPolygon.push(...ptsInPoly.features.slice(0, remainder));
		remainder -= ptsInPoly.features.length;
	}

	return {
		type: 'FeatureCollection',
		features: pointsInPolygon.map((feature) => {
			const id = new ObjectID().toHexString();
			return {
				...feature,
				id,
				properties: {},
			};
		}),
	};
}

function main() {
	const fieldId = new ObjectID().toHexString();
	const areaId = new ObjectID().toHexString();
	const stratificationId = new ObjectID().toHexString();
	const area = {
		...boundary.features[0],
		id: areaId,
		properties: {
			dateCreated: new Date().toISOString(),
			featureOfInterest: fieldId,
			name: 'Area',
		},
	};
	const field = {
		id: fieldId,
		name: 'Field ',
		address: {},
		contactPoint: [],
		area: [areaId],
	};

	const stratification = {
		id: stratificationId,
		name: 'Stratification',
		dateCreated: new Date().toISOString(),
		object: areaId,
	};

	const locations = randomPointsInPolygon(area.geometry, 25);

	const locationCollection = {
		id: new ObjectID().toHexString(),
		object: areaId,
		resultOf: stratificationId,
		featureOfInterest: fieldId,
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		features: locations.features.map((f: { [key: string]: any }) => f.id),
	};

	const data = {
		fields: [field],
		areas: [area],
		stratifications: [stratification],
		locations: locations.features,
		locationCollections: [locationCollection],
	};

	fs.writeFileSync('./out.json', JSON.stringify(data), 'utf8');
}

main();
