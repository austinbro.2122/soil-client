module.exports = {
	type: 'FeatureCollection',
	features: [
		{
			id: '2ae5524540ab1249f3eeb5aec04125e8',
			type: 'Feature',
			properties: {
				handle: 'asdf',
				organization: 1,
				name: 'asdf',
				depths: ['0_TO_10_CM', '10_TO_20_CM', '20_TO_30_CM'],
				hasAdditionalQuestions: false,
				email: 'asdf@asdf.com',
				createdAt: 1603806553996,
			},
			geometry: {
				coordinates: [
					[
						[-84.49588990912653, 39.118452235872326],
						[-84.49349937089559, 39.118285240315316],
						[-84.49402801153047, 39.117919839114336],
						[-84.49460308781886, 39.1180900069989],
						[-84.49513804250495, 39.118019449632925],
						[-84.49521026138757, 39.11816886514734],
						[-84.49586558087906, 39.11832865638874],
						[-84.49588990912653, 39.118452235872326],
					],
				],
				type: 'Polygon',
			},
		},
	],
};
