import { Ref, ref, computed, ComputedRef } from '@vue/composition-api';
import { Store } from 'vuex';
import { distance, point, booleanPointInPolygon, bboxPolygon, bbox } from '@turf/turf';
import stringify from 'csv-stringify/lib/sync';
import { padBounds } from '../../utils/mapFetching';
import { Location as SLocation, SamplingCollection, Sampling, Sample, SoDepth, Field, Area } from '@/store/types';

type UpdateSamplingCollectionEditRefsStrings =
	| 'hasError'
	| 'isReady'
	| 'field'
	| 'samplingCollection'
	| 'samplings'
	| 'locationCollection'
	| 'locations'
	| 'samplingsByLocationId'
	| 'locationsById';
export const updateSamplingCollectionEditRefs = (
	refs: {
		[key in UpdateSamplingCollectionEditRefsStrings]: Ref;
	}
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
) => (results: { [key: string]: any }) => {
	if (results.isReady && !results.hasError) {
		refs.hasError.value = results.hasError;
		refs.isReady.value = results.isReady;
		refs.field.value = results.field;
		refs.samplingCollection.value = results.samplingCollection;
		refs.samplings.value = results.samplings;
		refs.locationCollection.value = results.locationCollection;
		refs.locations.value = results.locations;
		refs.locationsById.value = (results.locations as SLocation[]).reduce(
			(r: { [key: string]: SLocation }, location: SLocation) => ({
				...r,
				[location.id]: location,
			}),
			{}
		);
		refs.samplingsByLocationId.value = (results.samplings as Sampling[]).reduce(
			(r: { [key: string]: Sampling }, sampling: Sampling) => ({
				...r,
				...(sampling?.featureOfInterest ? { [sampling.featureOfInterest]: sampling } : {}),
			}),
			{}
		);
		// mergedLocationCollections.value = result.mergedLocationCollections;
	}
};

export interface GeoPosition {
	accuracy: number;
	altitude?: number | null;
	altitudeAccuracy?: number | null;
	heading?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
	latitude: number;
	longitude: number;
	speed?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface UserLocation {
	coords: GeoPosition;
	timestamp: number;
}

export const useUserLocation = (
	activeLocationId: Ref<string | null>,
	activeLocation: Ref<SLocation | null>,
	area: ComputedRef<Area | undefined>
) => {
	const geolocationWatchId = ref();
	const userLocation = ref();
	const isOutOfBoundsAcknowledged = ref(false);
	const isOutOfBounds = ref(false);

	const distanceToActiveLocation = computed(
		() =>
			userLocation.value &&
			activeLocationId.value &&
			activeLocation.value &&
			distance(
				activeLocation.value.geometry.coordinates,
				[userLocation.value.coords.longitude, userLocation.value.coords.latitude],
				{ units: 'meters' }
			)
	);

	const checkIfOutOfBounds = (position: Position, area?: Area) => {
		if (!area) {
			return;
		}

		const bounds = bbox(area);
		const paddedBoundsPolygon = bboxPolygon(padBounds(bounds));
		const userLocationPoint = point([position.coords.latitude, position.coords.longitude]);
		const isUserLocationInMapBounds = booleanPointInPolygon(userLocationPoint, paddedBoundsPolygon);

		if (!isUserLocationInMapBounds) {
			isOutOfBounds.value = true;
		} else {
			isOutOfBounds.value = false;
		}
	};

	const acknowledgeOutOfBounds = () => {
		isOutOfBoundsAcknowledged.value = true;
	};

	const resetOutOfBoundsCheck = () => {
		isOutOfBoundsAcknowledged.value = false;
		isOutOfBounds.value = false;
	};

	function onSuccess(position: Position) {
		checkIfOutOfBounds(position, area.value);
		userLocation.value = {
			coords: {
				accuracy: position.coords.accuracy,
				latitude: position.coords.latitude,
				longitude: position.coords.longitude,
			},
			timestamp: position.timestamp,
		};
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	function onError(error: any) {
		console.warn(new Error(String(error)));
	}

	function startWatchPosition() {
		return navigator.geolocation.watchPosition(onSuccess, onError, {
			enableHighAccuracy: true,
		});
	}

	function restartWatchPosition() {
		if (document.visibilityState === 'visible') {
			if (geolocationWatchId.value) {
				navigator.geolocation.clearWatch(geolocationWatchId.value);
				geolocationWatchId.value = startWatchPosition();
			}
		}
	}

	function onTrackUserLocationStart() {
		resetOutOfBoundsCheck();
		geolocationWatchId.value = startWatchPosition();
		// restart watchPosition when resuming the app to prevent a bug on iOS Safari where high accuracy is silently lost when returning to the app
		document.addEventListener('visibilitychange', restartWatchPosition);
	}

	function onTrackUserLocationEnd() {
		navigator.geolocation.clearWatch(geolocationWatchId.value);
		document.removeEventListener('visibilitychange', restartWatchPosition);
		userLocation.value = null;
	}

	return {
		onTrackUserLocationStart,
		onTrackUserLocationEnd,
		distanceToActiveLocation,
		userLocation,
		isOutOfBounds,
		isOutOfBoundsAcknowledged,
		acknowledgeOutOfBounds,
	};
};

export const useReactiveSamplings = (
	store: Store<any>, // eslint-disable-line @typescript-eslint/no-explicit-any
	samplingCollectionIds: string | string[]
) => {
	const ids = Array.isArray(samplingCollectionIds) ? samplingCollectionIds : [samplingCollectionIds];
	return {
		samplingsR: computed(() => store.getters['samplings/getBySamplingCollectionIds'](ids)),
		samplingsRByLocationId: computed(() => store.getters['samplings/getBySamplingCollectionIdsKeyedByLocationId'](ids)),
	};
};

export const useReactiveSamples = (
	store: Store<any>, // eslint-disable-line @typescript-eslint/no-explicit-any
	samplingsRef: ComputedRef<Sampling[]>
) => {
	const samplesReactive: ComputedRef<Sample[]> = computed(() => {
		const samplingIds = samplingsRef.value.map((sampling: Sampling) => sampling.id);
		return store.getters['samples/getBySamplingIds'](samplingIds);
	});

	const samplesReactiveByLocation: ComputedRef<{
		[key: string]: Sample[];
	}> = computed(() =>
		samplesReactive.value.reduce((r: { [key: string]: Sample[] }, x: Sample) => {
			const locationId = samplingsRef.value?.find((s: Sampling) => x.resultOf === s.id)?.featureOfInterest;
			if (typeof locationId === 'string') {
				return {
					...r,
					[locationId]: [...(r[locationId] || []), x] as Sample[],
				};
			}

			return r;
		}, {})
	);

	return {
		samplesReactive,
		samplesReactiveByLocation,
	};
};

function flattenSoDepth(soDepth: SoDepth) {
	return {
		soDepthMin: soDepth.minValue.value,
		soDepthMax: soDepth.maxValue.value,
		soDepthUnit: soDepth.minValue.unit,
	};
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function prefixObjectKeys(obj: { [key: string]: any }, prefix: string) {
	return Object.entries(obj).reduce(
		(r, [k, v]) => ({
			...r,
			[`${prefix}${k.charAt(0).toUpperCase()}${k.slice(1)}`]: v,
		}),
		{}
	);
}

interface FlatSampleExport {
	sampleId: string;
	sampleName: string;
	sampleResultOf: string;
	sampleSoDepthMin: number;
	sampleSoDepthMax: number;
	sampleSoDepthUnit: string;
}

function flattenSample(sample: Sample): FlatSampleExport {
	const { soDepth, ...rest } = sample;
	const flatSample = {
		...rest,
		...flattenSoDepth(soDepth),
	};

	return prefixObjectKeys(flatSample, 'sample') as FlatSampleExport;
}

function flattenSampling(sampling: Sampling) {
	const { featureOfInterest, geometry, properties, soDepth, ...rest } = sampling;
	const flatSampling = {
		...rest,
		locationId: featureOfInterest,
		longitude: geometry.coordinates[0],
		latitude: geometry.coordinates[1],
		geoAccuracy: properties.geoAccuracy,
		...flattenSoDepth(soDepth),
	};
	return prefixObjectKeys(flatSampling, 'sampling');
}

function flattenLocation(location: SLocation) {
	const { type: _, properties, geometry, id, ...rest } = location;
	const flatLocation = {
		...rest,
		lId: id,
		stratum: properties?.stratum ?? '',
		composite: properties?.composite ?? '',
		label: properties?.label ?? '',
		longitude: geometry.coordinates[0],
		latitude: geometry.coordinates[1],
	};
	return prefixObjectKeys(flatLocation, 'location');
}

function flattenSamplingCollection(samplingCollection: SamplingCollection) {
	const { object, featureOfInterest, members, ...rest } = samplingCollection;
	const flatSamplingCollection = {
		locationCollectionId: object,
		fieldId: featureOfInterest,
		samplingIds: members,
		...rest,
	};
	return prefixObjectKeys(flatSamplingCollection, 'samplingCollection');
}

export function transformDataForRawSamplingCollectionCSV({
	locations,
	samplings,
	samplingCollections,
	samples,
	field,
}: {
	locations: SLocation[];
	samplings: Sampling[];
	samplingCollections: SamplingCollection[];
	samples: Sample[];
	field?: Field;
}) {
	return samples.map(flattenSample).map((sampleExport: FlatSampleExport) => {
		const sampling = samplings.find((s: Sampling) => s.id === sampleExport.sampleResultOf);
		const location = locations.find((l: SLocation) => l.id === sampling?.featureOfInterest);
		const samplingCollection = samplingCollections.find((collection) =>
			collection.members.some((samplingId) => samplingId === sampling?.id)
		);
		return {
			...sampleExport,
			...(sampling ? flattenSampling(sampling) : {}),
			...(location ? flattenLocation(location) : {}),
			...(samplingCollection ? flattenSamplingCollection(samplingCollection) : {}),
			fieldName: field?.name,
		};
	});
}

interface LabInventoryCSVRow {
	'Label (Bag ID)': string;
	Stratum: string;
	Composite: string;
	'Sample ID': string;
	'Sampling ID': string;
	'Sampling Collection ID': string;
	'Min Depth': string;
	'Max Depth': string;
	'Test pH and P'?: string;
}
export const orderedLabInventoryHeaders: Array<keyof LabInventoryCSVRow> = [
	'Label (Bag ID)',
	'Stratum',
	'Composite',
	'Sample ID',
	'Sampling ID',
	'Sampling Collection ID',
	'Min Depth',
	'Max Depth',
	'Test pH and P',
];

export const flagPercentageOfSamplings = (samplings: Sampling[], percentage: number) => {
	const numberToFlag = Math.ceil(samplings.length * percentage);
	const flagEvery = Math.ceil(samplings.length / numberToFlag);
	const flaggedSamplingIds = samplings.filter((_, i) => i % flagEvery === 0).map((s) => s.id);
	return flaggedSamplingIds;
};

export function transformDataForLabInventoryCSV(
	samplingCollectionData: {
		locations: SLocation[];
		samplings: Sampling[];
		samplingCollection: SamplingCollection;
		samples: Sample[];
	},
	includePHandP = false
) {
	const samplingsIdsFlaggedForPHandP = flagPercentageOfSamplings(
		// sort to ensure we flag the same samplings every time
		samplingCollectionData.samplings.sort((a, b) => a.id.localeCompare(b.id)),
		0.2
	);
	return transformDataForRawSamplingCollectionCSV({
		...samplingCollectionData,
		samplingCollections: [samplingCollectionData.samplingCollection],
	})
		.sort(
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(a: any, b: any) =>
				a.samplingCollectionId.localeCompare(b.samplingCollectionId) || a.samplingId.localeCompare(b.samplingId)
		)
		.map(
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(row: any): LabInventoryCSVRow => ({
				'Label (Bag ID)': row.sampleName,
				Stratum: row.locationStratum,
				Composite: row.locationComposite,
				'Sample ID': row.sampleId,
				'Sampling ID': row.samplingId,
				'Sampling Collection ID': row.samplingCollectionId,
				'Min Depth': row.sampleSoDepthMin,
				'Max Depth': row.sampleSoDepthMax,
				...(includePHandP
					? {
							'Test pH and P': samplingsIdsFlaggedForPHandP.includes(row.samplingId) ? 'Yes' : 'No',
					  }
					: {}),
			})
		);
}

export function dataToCSV(data: Array<object>, useLabExportHeaders = false) {
	const columnHeadersPresentInData = [...new Set(data.flatMap(Object.keys))];
	const sortedColumnHeaders = orderedLabInventoryHeaders.filter((header) =>
		columnHeadersPresentInData.includes(header)
	);
	const headers = useLabExportHeaders ? sortedColumnHeaders : columnHeadersPresentInData;
	return stringify(data, {
		columns: headers,
		header: true,
	});
}

export function generateRawSamplingCollectionCSV(samplingCollectionData: {
	locations: SLocation[];
	samplings: Sampling[];
	samplingCollections: SamplingCollection[];
	samples: Sample[];
	field: Field;
}) {
	const transformedData = transformDataForRawSamplingCollectionCSV(samplingCollectionData);
	return dataToCSV(transformedData);
}

export function generateDataURI(data: string, type: string) {
	const uriEncodedData = encodeURIComponent(data);
	switch (type) {
		case 'csv':
			return `data:text/csv;charset=utf-8,${uriEncodedData}`;
		case 'json':
			return `data:text/json;,${uriEncodedData}`;
		default:
			throw new TypeError(`generateDataURI: unknown type ${type}`);
	}
}

export function generateLabInventoryFilename() {
	return `lab-inventory-${new Date().toISOString().replaceAll(/:|\./g, '-')}.csv`;
}

export function download(filename: string, data: string, type: string) {
	const anchor = document.createElement('a');
	anchor.target = '_blank';
	anchor.download = filename;
	anchor.href = generateDataURI(data, type);
	anchor.click();
}

export function downloadLabInventoryCSV(
	samplingCollectionsData: {
		samplingCollectionData: {
			locations: SLocation[];
			samplings: Sampling[];
			samplingCollection: SamplingCollection;
			samples: Sample[];
		};
		includePHandP: boolean;
	}[]
) {
	const filename = generateLabInventoryFilename();
	const rows = samplingCollectionsData.flatMap(({ samplingCollectionData, includePHandP }) =>
		transformDataForLabInventoryCSV(samplingCollectionData, includePHandP)
	);
	download(filename, dataToCSV(rows, true), 'csv');
}

const omitKey = (key: string) => (object: object): object => {
	const objectCopy = JSON.parse(JSON.stringify(object));
	delete objectCopy[key];
	return objectCopy;
};

export function prepareSamplingCollectionForSubmission({
	samplingCollection,
	samplings,
	samples,
}: {
	samplingCollection: SamplingCollection;
	samplings: Sampling[];
	samples: Sample[];
}) {
	return {
		...omitKey('id')(samplingCollection),
		members: samplings
			.map(omitKey('id'))
			.map(omitKey('memberOf'))
			.map((sampling: Partial<Sampling>) => ({
				...sampling,
				results: samples
					.filter((sample: Sample) => sampling.results?.includes(sample.id))
					.map(omitKey('id'))
					.map(omitKey('resultOf')),
			})),
		meta: {
			referenceIds: [{ owner: 'soilstack-draft', id: samplingCollection.id }],
		},
	};
}
