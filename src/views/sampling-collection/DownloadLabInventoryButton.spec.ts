import DownloadLabInventoryButton from './DownloadLabInventoryButton.vue';
import { renderWithVuetify } from '../../../tests/renderWithVuetify';
import { createMockResourcesState, createMockGroupsState } from '../../store/modules/test-utils';
import { createStoreConfig } from '../../store';

describe('DownloadLabInventoryButton', () => {
	it('is disabled if no sampling collection ids are passed', () => {
		const { getByText } = renderWithVuetify(DownloadLabInventoryButton, {
			props: {
				samplingCollectionIds: [],
			},
			store: {} as any,
		});

		const button = getByText('Lab Inventory', { exact: false }).closest('button');

		expect(button).toBeDisabled();
	});

	it('is disabled if resources are not loaded', () => {
		const { getByText } = renderWithVuetify(
			DownloadLabInventoryButton,
			{
				props: {
					samplingCollectionIds: ['1'],
				},
				store: createStoreConfig() as any,
			},
			(_, store) => {
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({ isLoaded: false }),
					groups: createMockGroupsState({ isLoaded: true }),
				});
			}
		);

		const button = getByText('Lab Inventory', { exact: false }).closest('button');

		expect(button).toBeDisabled();
	});

	it('is disabled if groups are not loaded', () => {
		const { getByText } = renderWithVuetify(
			DownloadLabInventoryButton,
			{
				props: {
					samplingCollectionIds: ['1'],
				},
				store: createStoreConfig() as any,
			},
			(_, store) => {
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({ isLoaded: true }),
					groups: createMockGroupsState({ isLoaded: false }),
				});
			}
		);

		const button = getByText('Lab Inventory', { exact: false }).closest('button');

		expect(button).toBeDisabled();
	});

	it('is not disabled if sampling collection ids are passed and resources and groups are loaded', () => {
		const wrapper = renderWithVuetify(
			DownloadLabInventoryButton,
			{
				props: {
					samplingCollectionIds: ['1'],
				},
				store: createStoreConfig() as any,
			},
			(_, store) => {
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({ isLoaded: true }),
					groups: createMockGroupsState({ isLoaded: true }),
				});
			}
		);

		const button = wrapper.getByText('Lab Inventory', { exact: false }).closest('button');

		expect(button).not.toBeDisabled();
	});
});
