import { Sampling, Sample, SamplingCollection, Location, Field, Area } from '@/store/types';
import { computed, ComputedRef, defineComponent, Ref, ref } from '@vue/composition-api';
import CollectMap from '@/components/map/CollectMap.vue';
import CollectSamplingButton from '@/components/collect/CollectSamplingButton.vue';
import SamplingList from '@/components/collect/SamplingList.vue';
import SamplingCollectionSubmit from './SamplingCollectionSubmit.vue';
import {
	useUserLocation,
	useReactiveSamplings,
	useReactiveSamples,
	generateRawSamplingCollectionCSV,
	downloadLabInventoryCSV,
	download,
} from '@/views/sampling-collection/helpers';
import { useAllResources } from '@/store/modules/resources';
import { useAllSamplingCollections } from '@/store/modules/samplingCollections';
import { useAllSamplings } from '@/store/modules/samplings';
import { useAllSamples } from '@/store/modules/samples';
import { useGroups } from '@/store/modules/group';

export default defineComponent({
	props: {
		samplingCollectionId: {
			required: true,
			type: String,
		},
	},
	components: {
		CollectMap,
		CollectSamplingButton,
		SamplingList,
		SamplingCollectionSubmit,
	},
	setup(props, { root }) {
		const {
			isLoaded: samplingCollectionsAreLoaded,
			isLoading: samplingCollectionsAreLoading,
			hasError: samplingCollectionsHaveError,
		} = useAllSamplingCollections(root.$store);
		const {
			isLoaded: samplingsAreLoaded,
			isLoading: samplingsAreLoading,
			hasError: samplingsHaveError,
		} = useAllSamplings(root.$store);
		const { isLoaded: samplesAreLoaded, isLoading: samplesAreLoading, hasError: samplesHaveError } = useAllSamples(
			root.$store
		);
		const {
			isLoaded: resourcesAreLoaded,
			isLoading: resourcesAreLoading,
			hasError: resourcesHaveError,
		} = useAllResources(root.$store);
		const { isLoaded: groupsAreLoaded, isLoading: groupsAreLoading, hasError: groupsHaveError } = useGroups(
			root.$store
		);
		const isLoaded = computed(
			() =>
				samplingCollectionsAreLoaded.value &&
				samplingsAreLoaded.value &&
				samplesAreLoaded.value &&
				resourcesAreLoaded.value &&
				groupsAreLoaded.value
		);
		const isLoading = computed(() =>
			[
				samplingCollectionsAreLoading.value,
				samplingsAreLoading.value,
				samplesAreLoading.value,
				resourcesAreLoading.value,
				groupsAreLoading.value,
			].some(Boolean)
		);
		const hasError = computed(() =>
			[
				samplingCollectionsHaveError.value,
				samplingsHaveError.value,
				samplesHaveError.value,
				resourcesHaveError.value,
				groupsHaveError.value,
			].some(Boolean)
		);

		const progressDialogIsVisible = ref(false);
		const showSubmitDialog = ref(false);

		const samplingCollection: ComputedRef<SamplingCollection> = computed(() =>
			root.$store.getters['samplingCollections/getById'](props.samplingCollectionId)
		);

		const field: ComputedRef<Field> = computed(() =>
			root.$store.getters['resources/getFieldById'](samplingCollection.value?.featureOfInterest)
		);

		const area: ComputedRef<Area | undefined> = computed(
			() => root.$store.getters['resources/getAreasByFieldId'](samplingCollection.value?.featureOfInterest)?.[0]
		);

		const locations = computed(() =>
			root.$store.getters['resources/getLocationsByFieldId'](samplingCollection.value?.featureOfInterest)
		);

		const mapLocations = computed(() =>
			locations.value?.map((location: Location, i: number) => ({
				...location,
				properties: {
					...location.properties,
					id: location.id,
					/*
						Use location.properties.label if present, otherwise use 1-based indexes.
						Stratifications prior to 2021-10-25 do not include location.properties.label.
						Defaulting to 1-based indexes if label is not present ensures we maintain
						consistent numbering for stratifications created before the above date.
					*/
					label: location.properties?.label ?? `${i + 1}`,
					markerType: 'LOCATION_INCOMPLETE',
				},
			}))
		);

		const locationsById = computed(() =>
			locations.value.reduce(
				(r: { [key: string]: Location }, location: Location) => ({
					...r,
					[location.id]: location,
				}),
				{}
			)
		);

		const activeLocationId: Ref<null | string> = ref(null);
		const activeLocation: ComputedRef<null | Location> = computed(
			() => (activeLocationId.value && locationsById.value[activeLocationId.value]) || null
		);

		const reactiveSamplings = useReactiveSamplings(root.$store, props.samplingCollectionId);

		const reactiveSamples = useReactiveSamples(root.$store, reactiveSamplings.samplingsR);

		const activeSampling: ComputedRef<null | Sampling> = computed(
			() => (activeLocationId.value && reactiveSamplings.samplingsRByLocationId.value[activeLocationId.value]) || null
		);

		const activeSamples: ComputedRef<Sample[]> = computed(
			() => (activeLocationId.value && reactiveSamples.samplesReactiveByLocation.value[activeLocationId.value]) || []
		);

		function onLocationClick(id: string) {
			activeLocationId.value = activeLocationId.value === id ? null : id;
		}

		const userLocation = useUserLocation(activeLocationId, activeLocation, area);

		function onSamplingCreate({ sampling: newSampling }: { sampling: Sampling; samples: Sample[] }) {
			samplingCollection.value?.members &&
				root.$store.dispatch('samplingCollections/setRecord', {
					...samplingCollection.value,
					members: [...samplingCollection.value.members, newSampling.id],
					dateModified: new Date().toISOString(),
				});
		}

		async function onSamplingDelete(s: Sampling) {
			if (samplingCollection.value?.members.length) {
				root.$store.dispatch('samplingCollections/setRecord', {
					...samplingCollection.value,
					members: samplingCollection.value.members.filter((m) => m !== s.id),
					dateModified: new Date().toISOString(),
				});
			}
		}

		const locationsWithStatus: ComputedRef<Location[] | null> = computed(() =>
			(mapLocations.value as Location[]).map((l: Location) => ({
				...l,
				properties: {
					...l.properties,
					markerType: reactiveSamplings.samplingsRByLocationId.value[l.id]
						? 'LOCATION_COMPLETE'
						: 'LOCATION_INCOMPLETE',
				},
			}))
		);

		const progress: ComputedRef<string | null> = computed(() => {
			return `${reactiveSamplings.samplingsR.value?.length || 0} / ${locations.value?.length}`;
		});

		function exportRawSamplingCollection(type = 'json') {
			const data = {
				samplingCollections: samplingCollection.value ? [samplingCollection.value] : [],
				samplings: reactiveSamplings.samplingsR.value,
				samples: reactiveSamples.samplesReactive.value,
				locations: locations.value,
			};

			const filename = `sampling-collection-${new Date().toISOString().replaceAll(/:|\./g, '-')}.${type.toLowerCase()}`;
			let formattedData: string;
			switch (type) {
				case 'csv':
					formattedData = generateRawSamplingCollectionCSV({
						...data,
						field: field.value,
					});
					break;
				case 'json':
					formattedData = JSON.stringify(data);
					break;
				default:
					throw new TypeError(`exportRawSamplingCollection: unknown type ${type}`);
			}

			download(filename, formattedData, type);
		}

		function exportLabInventory() {
			const samplingCollectionData = {
				samplingCollection: samplingCollection.value,
				samplings: reactiveSamplings.samplingsR.value,
				samples: reactiveSamples.samplesReactive.value,
				locations: locations.value,
			};

			const includePHandP = root.$store.getters['getIsFieldWithinGroupPath'](field.value.id, '/soilstack/esmc/');

			downloadLabInventoryCSV([{ samplingCollectionData, includePHandP }]);
		}

		return {
			exportLabInventory,
			exportRawSamplingCollection,
			progressDialogIsVisible,
			showSubmitDialog,
			isLoaded,
			isLoading,
			hasError,
			activeLocationId,
			activeLocation,
			onLocationClick,
			...userLocation,
			mapLocations,
			locationsById,
			activeSampling,
			samplingsR: reactiveSamplings.samplingsR,
			samplingsRByLocationId: reactiveSamplings.samplingsRByLocationId,
			samplingCollection,
			onSamplingCreate,
			onSamplingDelete,
			locationsWithStatus,
			progress,
			...reactiveSamples,
			activeSamples,
			field,
			area,
		};
	},
});
