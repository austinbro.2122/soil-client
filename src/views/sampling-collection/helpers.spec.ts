import {
	generateDataURI,
	transformDataForRawSamplingCollectionCSV,
	transformDataForLabInventoryCSV,
	prepareSamplingCollectionForSubmission,
	flagPercentageOfSamplings,
	dataToCSV,
	orderedLabInventoryHeaders,
} from './helpers';
import {
	createMockLocation,
	createMockSampling,
	createMockSamplingCollection,
	createMockSample,
	createMockSoDepth,
	createMockSoDepthValue,
	createMockField,
} from '../../../tests/mockGenerators';

describe('dataToCSV', () => {
	describe('when useLabExportHeader is false', () => {
		it('contains all the keys that are present in any row as headers', () => {
			const data = [
				{
					a: 'a',
					b: 'b',
					c: 'c',
				},
				{
					a: 'a',
					b: 'b',
					c: 'c',
					d: 'd',
				},
			];
			const csv = dataToCSV(data, false);
			expect(csv).toContain('a,b,c,d');
		});
	});

	describe('when useLabExportHeaders is true', () => {
		it('only includes headers from LabInventoryCSVRow that are present in the passed in data', () => {
			const data = [
				{
					'Label (Bag ID)': '1',
					Stratum: '1',
					Composite: '1',
					'Sample ID': '1',
					'Sampling ID': '1',
					'Sampling Collection ID': '1',
					'Min Depth': '1',
					'Max Depth': '1',
				},
				{
					'Label (Bag ID)': '2',
					Stratum: '2',
					Composite: '2',
					'Sample ID': '2',
					'Sampling ID': '2',
					'Sampling Collection ID': '2',
					'Min Depth': '2',
					'Max Depth': '2',
				},
			];

			const result = dataToCSV(data, true);

			expect(result).not.toContain('Test pH and P');
		});

		it('includes headers if they are present in any row', () => {
			const data = [
				{
					'Label (Bag ID)': '1',
					Stratum: '1',
					Composite: '1',
					'Sample ID': '1',
					'Sampling ID': '1',
					'Sampling Collection ID': '1',
					'Min Depth': '1',
					'Max Depth': '1',
				},
				{
					'Label (Bag ID)': '2',
					Stratum: '2',
					Composite: '2',
					'Sample ID': '2',
					'Sampling ID': '2',
					'Sampling Collection ID': '2',
					'Min Depth': '2',
					'Max Depth': '2',
					'Test pH and P': '2',
				},
			];

			const result = dataToCSV(data, true);

			expect(result).toContain('Test pH and P');
		});

		it('orders headers according to orderedLabInventoryHeaders, not the order of keys in the passed in data', () => {
			const data = [
				{
					'Min Depth': '1',
					Stratum: '1',
					'Label (Bag ID)': '1',
					Composite: '1',
					'Test pH and P': '1',
					'Sampling ID': '1',
					'Sample ID': '1',
					'Sampling Collection ID': '1',
					'Max Depth': '1',
				},
			];

			const result = dataToCSV(data, true);

			expect(result).toContain(orderedLabInventoryHeaders.join(','));
		});
	});
});

describe('flagPercentageOfSamplings', () => {
	it('returns percentage of samplings ids according to given percentage', () => {
		const samplings = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }] as any;

		const flaggedIds = flagPercentageOfSamplings(samplings, 0.2);

		expect(flaggedIds.length).toBe(1);
	});

	it('rounds up so that the percentage is always met, and may be exceeded', () => {
		const samplings = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }] as any;

		const flaggedIds = flagPercentageOfSamplings(samplings, 0.2);

		expect(flaggedIds.length).toBe(2);
	});

	it('rounds up so that the percentage is always met, even when the number of samples is not evenly divisible by the number to flag', () => {
		const samplings = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }] as any;

		const flaggedIds = flagPercentageOfSamplings(samplings, 0.2);

		expect(flaggedIds.length).toBe(2);

		const samplings2 = [
			{ id: 1 },
			{ id: 2 },
			{ id: 3 },
			{ id: 4 },
			{ id: 5 },
			{ id: 6 },
			{ id: 7 },
			{ id: 8 },
			{ id: 9 },
		] as any;

		const flaggedIds2 = flagPercentageOfSamplings(samplings2, 0.2);

		expect(flaggedIds2.length).toBe(2);
	});
});

describe('prepareSamplingCollectionForSubmission', () => {
	const mockSamplingCollection = createMockSamplingCollection({ id: '123', members: ['1', '2'] });
	const mockSampling1 = createMockSampling({ id: '1', results: ['s1', 's2'] });
	const mockSampling2 = createMockSampling({ id: '2', results: ['s3', 's4'] });
	const mockSample1 = createMockSample({ id: 's1' });
	const mockSample2 = createMockSample({ id: 's2' });
	const mockSample3 = createMockSample({ id: 's3' });
	const mockSample4 = createMockSample({ id: 's4' });

	const actual = prepareSamplingCollectionForSubmission({
		samplingCollection: mockSamplingCollection,
		samplings: [mockSampling1, mockSampling2],
		samples: [mockSample1, mockSample2, mockSample3, mockSample4],
	});

	it('stores id of the draft as a reference id', () => {
		expect(actual.meta.referenceIds[0]).toHaveProperty('owner', 'soilstack-draft');
		expect(actual.meta.referenceIds[0]).toHaveProperty('id', '123');
	});

	describe('omitting certain properties', () => {
		it('omits id from samplingCollection', () => {
			expect(actual).not.toHaveProperty('id');
		});

		it('omits id and memberOf fields from samplings', () => {
			expect(actual.members).toEqual([
				expect.not.objectContaining({
					id: expect.anything(),
					memberOf: expect.anything(),
				}),
				expect.not.objectContaining({
					id: expect.anything(),
					memberOf: expect.anything(),
				}),
			]);
		});

		it('omits id and resultOf fields from samples', () => {
			const samples = actual.members.flatMap((sampling) => sampling.results);
			samples.forEach((sample) => {
				expect(sample).not.toHaveProperty('id');
				expect(sample).not.toHaveProperty('resultOf');
			});
		});
	});

	describe('populating the sampling collection (replacing reference ids with actual objects)', () => {
		it('replaces samplingCollection.members reference ids with sampling objects', () => {
			expect(actual.members).not.toEqual(['1', '2']);
			expect(actual.members).toEqual([expect.objectContaining({}), expect.objectContaining({})]);
		});

		it('replaces sampling.results reference ids with sample objects', () => {
			actual.members.forEach((sampling) => {
				expect(sampling.results).not.toEqual([expect.any(String), expect.any(String)]);
				expect(sampling.results).toEqual([expect.objectContaining({}), expect.objectContaining({})]);
			});
		});
	});
});

describe('generateDataURI', () => {
	it('uri encodes data', () => {
		const inputWithIllegalURIcharacters = ` ?\n`;
		const encodedURI = generateDataURI(inputWithIllegalURIcharacters, 'csv');

		expect(encodedURI).toContain('%20');
		expect(encodedURI).toContain('%3F');
		expect(encodedURI).toContain('%0A');
	});

	it('throws an error if type is unknown', () => {
		expect(() => generateDataURI('', 'exe')).toThrow(TypeError);
	});
});

describe('preparing data for csv export', () => {
	const mockData = {
		locations: [
			createMockLocation({
				id: 'loc1',
				properties: {
					stratum: 1,
					composite: '2',
				},
			}),
			createMockLocation({
				id: 'loc2',
				properties: {
					stratum: 3,
					composite: '4',
				},
			}),
		],
		samplings: [
			createMockSampling({ id: 'sampling1', featureOfInterest: 'loc1' }),
			createMockSampling({ id: 'sampling2', featureOfInterest: 'loc2' }),
		],
		samplingCollection: createMockSamplingCollection({ id: 'coll0', members: ['sampling1', 'sampling2'] }),
		samples: [
			createMockSample({ name: 'Sample 1', id: 'sample1', resultOf: 'sampling1' }),
			createMockSample({
				name: 'Sample 2',
				id: 'sample2',
				resultOf: 'sampling2',
				soDepth: createMockSoDepth({
					minValue: createMockSoDepthValue({ value: 22 }),
					maxValue: createMockSoDepthValue({ value: 33 }),
				}),
			}),
			createMockSample({
				name: 'Sample 3',
				id: 'sample3',
				resultOf: 'sampling2',
				soDepth: createMockSoDepth({
					minValue: createMockSoDepthValue({ value: 44 }),
					maxValue: createMockSoDepthValue({ value: 55 }),
				}),
			}),
		],
	};

	describe('transformDataForRawSamplingCollectionCSV', () => {
		it('transforms data into rows', () => {
			expect(
				transformDataForRawSamplingCollectionCSV({
					...mockData,
					samplingCollections: [mockData.samplingCollection],
					field: createMockField(),
				})
			).toMatchSnapshot();
		});
	});

	describe('transformDataForLabInventoryCSV', () => {
		it('transforms data into rows', () => {
			expect(transformDataForLabInventoryCSV(mockData)).toMatchSnapshot();
		});

		it('includes pH and P column when includePHandP is true', () => {
			const csvRows = transformDataForLabInventoryCSV(mockData, true);
			expect(csvRows[0]).toHaveProperty('Test pH and P');
		});

		it('does not include pH and P column when includePHandP is false', () => {
			const csvRows = transformDataForLabInventoryCSV(mockData, false);
			expect(csvRows[0]).not.toHaveProperty('Test pH and P');
		});
	});
});
