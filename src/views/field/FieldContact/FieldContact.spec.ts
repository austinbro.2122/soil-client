import FieldContact from './FieldContact.vue';
import { ContactPoint } from '@/store/types';
import { renderWithVuetify } from '../../../../tests/renderWithVuetify';

describe('FieldContact', () => {
	const contact: ContactPoint = {
		name: 'Jerry Allen',
		contactType: 'Producer',
		email: 'jerryallen@aol.com',
		telephone: '123-456-7890',
		organization: 'Company Name',
	};
	let getByText: any;
	beforeEach(() => {
		({ getByText } = renderWithVuetify(FieldContact, { props: { contact } }));
	});

	it('renders contact name', () => {
		getByText(contact.name);
	});

	it('renders contact type when present', () => {
		getByText(contact.contactType, { exact: false });
	});

	it('renders contact email when present', () => {
		getByText(contact.email);
	});

	it('renders contact telephone when present', () => {
		getByText(contact.telephone);
	});

	it('renders contact organization when present', () => {
		getByText(contact.organization);
	});
});
