import { centroid } from '@turf/turf';
import { Area, Address } from '@/store/types';

const createGoogleMapsUrl = (query: string): string => `https://www.google.com/maps/search/?api=1&query=${query}`;

const createContactAddressHref = (address: Address): string => {
	const { streetAddress, addressLocality, addressRegion, postalCode } = address;
	if (!streetAddress || !addressLocality) {
		return '';
	}

	const query = `${streetAddress},+${addressLocality}${addressRegion ? `,+${addressRegion}` : ''}${
		postalCode ? `+${postalCode}` : ''
	}`;

	return createGoogleMapsUrl(query);
};

const createFieldLocationHref = (area: Area): string => {
	const fieldCentroid = centroid(area);
	const [lng, lat] = fieldCentroid?.geometry?.coordinates || [];
	const query = encodeURIComponent(`${lat},${lng}`);
	return createGoogleMapsUrl(query);
};

export { createContactAddressHref, createFieldLocationHref };
