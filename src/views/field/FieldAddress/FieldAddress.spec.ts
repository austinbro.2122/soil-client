/* eslint-disable @typescript-eslint/no-explicit-any */
import FieldAddress from './FieldAddress.vue';
import { createContactAddressHref, createFieldLocationHref } from './FieldAddress';
import { Area, Address } from '@/store/types';
import { renderWithVuetify } from '../../../../tests/renderWithVuetify';

// TODO: I think we can use createMockArea here to avoid duplication
const createArea = (): Area => ({
	id: '',
	meta: {
		submissionId: '1',
		groupId: '2',
		submittedAt: new Date(0).toISOString(),
	},
	type: 'Feature',
	geometry: {
		type: 'Polygon',
		coordinates: [
			[
				[-94.0, 42.0],
				[-94.2, 42.0],
				[-94.2, 42.2],
				[-94.0, 42.2],
			],
		],
	},
	properties: {},
});
// TODO: I think we can use createMockAddress here to avoid duplication
const createAddress = ({
	streetAddress = '',
	addressLocality = '',
	addressRegion = '',
	addressCountry = '',
	postalCode = '',
} = {}): Address => {
	return {
		streetAddress,
		addressLocality,
		addressRegion,
		addressCountry,
		postalCode,
	};
};

describe('createContactAddressHref', () => {
	it('returns an empty string if address or locality are absent', () => {
		const href = createContactAddressHref(createAddress());

		expect(href).toBe('');
	});

	it('returns a url to google maps with the address properties as a query', () => {
		const address = createAddress({
			streetAddress: '166 W Mehring Way',
			addressLocality: 'Cincinnati',
			addressRegion: 'Ohio',
			addressCountry: 'USA',
			postalCode: '45202',
		});
		const href = createContactAddressHref(address);

		expect(href).toContain('google.com/maps');
		expect(href).toContain(address.streetAddress);
		expect(href).toContain(address.addressLocality);
		expect(href).toContain(address.addressRegion);
		expect(href).toContain(address.postalCode);
	});
});

describe('createFieldLocationHref', () => {
	it("returns a url to google maps with the area centroid's latitude and longitude as a query", () => {
		const area = createArea();
		const href = createFieldLocationHref(area);

		expect(href).toContain('google.com/maps');
		expect(href).toContain('-94');
		expect(href).toContain('42');
	});
});

describe('FieldAddress', () => {
	describe('rendering address properties', () => {
		describe('when street address is not available', () => {
			let getByText: any;
			beforeEach(() => {
				({ getByText } = renderWithVuetify(FieldAddress, {
					props: { address: createAddress(), areas: [createArea()] },
				}));
			});

			it("renders 'unavailable' message", () => {
				getByText('Street address unavailable.');
			});

			it("renders 'Navigate to Field Location' link", () => {
				getByText('Navigate to Field Location');
			});
		});

		describe('when street address is available', () => {
			let getByText: any;
			beforeEach(() => {
				({ getByText } = renderWithVuetify(FieldAddress, {
					props: { address: createAddress({ streetAddress: '123 Rose St.' }), areas: [createArea()] },
				}));
			});

			it('renders address', () => {
				getByText('123 Rose St.', { exact: false });
			});

			it("renders 'Navigate to Field Location' link", () => {
				getByText('Navigate to Field Location');
			});

			describe('and locality is available', () => {
				let getByText: any;
				beforeEach(() => {
					({ getByText } = renderWithVuetify(FieldAddress, {
						props: {
							address: createAddress({ streetAddress: '123 Rose St.', addressLocality: 'City' }),
							areas: [createArea()],
						},
					}));
				});

				it('renders locality', () => {
					getByText('City', { exact: false });
				});

				it("renders 'Navigate to Field Location' link", () => {
					getByText('Navigate to Field Location');
				});

				it("renders 'Navigate to Contact Address' link", () => {
					getByText('Navigate to Contact Address');
				});
			});

			describe('and region is available', () => {
				it('renders region', () => {
					const { getByText } = renderWithVuetify(FieldAddress, {
						props: {
							address: createAddress({ streetAddress: '123 Rose St.', addressRegion: 'State' }),
							areas: [createArea()],
						},
					});

					getByText('State', { exact: false });
				});
			});

			describe('and locality and region are both available', () => {
				it('renders locality and region together', () => {
					const { getByText } = renderWithVuetify(FieldAddress, {
						props: {
							address: createAddress({
								streetAddress: '123 Rose St.',
								addressLocality: 'City',
								addressRegion: 'State',
							}),
							areas: [createArea()],
						},
					});

					getByText('City, State', { exact: false });
				});
			});
		});
	});
});
