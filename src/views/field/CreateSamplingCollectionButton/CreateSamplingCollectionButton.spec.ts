import { fireEvent } from '@testing-library/vue';
import CreateSamplingCollectionButton from './CreateSamplingCollectionButton.vue';
import { renderWithVuetify } from '../../../../tests/renderWithVuetify';

describe('CreateSamplingCollectionButton', () => {
	it('navigates to the create-sampling-collection route on click', () => {
		const routerPushMock = jest.fn();
		const { getByText } = renderWithVuetify(CreateSamplingCollectionButton, {
			props: {
				fieldId: 'field-id',
				locationCollectionId: 'location-collection-id',
			},
			mocks: {
				$router: {
					push: routerPushMock,
				},
			},
		});

		const button = getByText('Collect').closest('button');
		button && fireEvent.click(button);

		expect(routerPushMock).toHaveBeenCalledWith({
			path: '/create-sampling-collection',
			query: {
				field: 'field-id',
				object: 'location-collection-id',
			},
		});
	});

	describe('when no location collection id is passed (indicating no stratification is present)', () => {
		it('is disabled', () => {
			const { getByText } = renderWithVuetify(CreateSamplingCollectionButton, {
				props: { fieldId: 'field-id' },
			});
			const button = getByText('Collect').closest('button');

			expect(button).toBeDisabled();
		});

		it('displays disabled info text', () => {
			const { getByText } = renderWithVuetify(CreateSamplingCollectionButton, {
				props: { fieldId: 'field-id' },
			});

			getByText('cannot be started', { exact: false });
		});
	});

	describe('when sampling collections are already in progress for the given field and user clicks the button', () => {
		let getByText: any;
		beforeEach(async () => {
			({ getByText } = renderWithVuetify(CreateSamplingCollectionButton, {
				props: {
					fieldId: 'field-id',
					locationCollectionId: 'location-collection-id',
					draftSamplingCollections: [{ id: 'sampling-collection-id-1', dateCreated: '2020-01-01' }],
				},
			}));
			const button = getByText('Collect').closest('button');
			button && fireEvent.click(button);

			await new Promise((res) => setTimeout(res, 0));
		});

		it('prompts user to resume existing sampling collections', () => {
			getByText('Choose a sampling collection to resume', { exact: false });
		});
	});
});
