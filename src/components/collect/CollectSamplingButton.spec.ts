import { shallowMount, createLocalVue } from '@vue/test-utils';
import CollectSamplingButton from '@/components/collect/CollectSamplingButton.vue';
import {
	locations as mockLocations,
	userLocation as mockUserLocation,
	samplingCollection as mockSamplingCollection,
	userLocation,
} from '@/../tests/mockData';
import Vuex from 'vuex';

describe('CollectSamplingButton.vue', () => {
	test('creates sampling', () => {
		const wrapper = shallowMount(CollectSamplingButton, {
			propsData: {
				location: mockLocations[0],
				userLocation: mockUserLocation,
				samplingCollectionId: mockSamplingCollection.id,
				samplingCollection: { ...mockSamplingCollection, members: [] },
			},
		});

		const samplingCreateStub = jest.fn();
		const samplingDeleteStub = jest.fn();
		wrapper.vm.$on('samplingCreate', samplingCreateStub);
		wrapper.vm.$on('samplingDelete', samplingDeleteStub);
		const collectButton = wrapper.find('v-btn-stub');
		// Doesn't do anything
		collectButton.trigger('click.stop');
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		const vm = wrapper.vm as any;
		vm.onSamplingCreate();
		expect(vm.dialogIsVisible).toBe(true);
		expect(vm.currentSampling.geometry.coordinates[0]).toBe(mockUserLocation.coords.longitude);
		expect(vm.currentSampling.geometry.coordinates[1]).toBe(mockUserLocation.coords.latitude);
		expect(vm.currentSampling.properties.geoAccuracy).toBe(mockUserLocation.coords.accuracy);
		expect(vm.currentSampling.soDepth).toBe(mockSamplingCollection.soDepth);
	});

	test('cancels sampling', () => {
		const wrapper = shallowMount(CollectSamplingButton, {
			propsData: {
				location: mockLocations[0],
				userLocation: mockUserLocation,
				samplingCollectionId: mockSamplingCollection.id,
				samplingCollection: { ...mockSamplingCollection, members: [] },
			},
		});

		const samplingCreateStub = jest.fn();
		const samplingDeleteStub = jest.fn();
		wrapper.vm.$on('samplingCreate', samplingCreateStub);
		wrapper.vm.$on('samplingDelete', samplingDeleteStub);
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		const vm = wrapper.vm as any;
		vm.onSamplingCreate();
		expect(vm.dialogIsVisible).toBe(true);
		expect(vm.currentSampling).toBeTruthy();
		vm.onSamplingCancel();

		expect(vm.dialogIsVisible).toBe(false);
		expect(vm.currentSampling).toBeFalsy();
		expect(vm.currentSamples.length).toBe(0);
	});

	test('saves sampling', () => {
		const localVue = createLocalVue();
		localVue.use(Vuex);

		const actions = {
			'samplings/setRecord': jest.fn(),
			'samples/setRecords': jest.fn(),
		};
		const store = new Vuex.Store({
			actions,
		});
		const wrapper = shallowMount(CollectSamplingButton, {
			localVue,
			store,
			propsData: {
				location: mockLocations[0],
				userLocation: mockUserLocation,
				samplingCollectionId: mockSamplingCollection.id,
				samplingCollection: { ...mockSamplingCollection, members: [] },
			},
		});

		const samplingCreateStub = jest.fn();
		const samplingDeleteStub = jest.fn();
		wrapper.vm.$on('samplingCreate', samplingCreateStub);
		wrapper.vm.$on('samplingDelete', samplingDeleteStub);
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		const vm = wrapper.vm as any;
		vm.onSamplingCreate();
		expect(vm.dialogIsVisible).toBe(true);
		expect(vm.currentSampling).toBeTruthy();

		vm.onSamplingSave();
		expect(actions['samplings/setRecord']).toHaveBeenCalled;
		expect(actions['samples/setRecords']).toHaveBeenCalled;
		expect(actions['samplings/setRecord'].mock.calls[0][1].geometry.coordinates[0]).toBe(userLocation.coords.longitude);
		expect(actions['samplings/setRecord'].mock.calls[0][1].geometry.coordinates[1]).toBe(userLocation.coords.latitude);
	});
});
