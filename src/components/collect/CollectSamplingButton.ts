import { defineComponent, ref, Ref } from '@vue/composition-api';
import { Sampling, Location as SLocation, SamplingCollection, SoDepth, Sample } from '@/store/types';
import ObjectId from 'bson-objectid';
import { UserLocation } from '@/views/sampling-collection/helpers';
import SamplingForm from '@/components/collect/SamplingForm.vue';
import { Point } from 'geojson';

interface Options {
	id: string;
	location: SLocation;
	userLocation: UserLocation | undefined | null;
	samplingCollectionId: string;
	soDepth: SoDepth;
}

function createSampling({ id, location, userLocation, samplingCollectionId, soDepth }: Options): Sampling {
	return {
		id,
		featureOfInterest: location.id,
		geometry: {
			type: 'Point',
			coordinates: [userLocation?.coords.longitude ?? -1, userLocation?.coords.latitude ?? -1],
		} as Point,
		properties: {
			geoAccuracy: userLocation?.coords.accuracy ?? -1,
		},
		comment: '',
		results: [],
		resultTime: new Date().toISOString(),
		procedures: ['unknown'],
		memberOf: samplingCollectionId,
		soDepth,
	};
}

export default defineComponent({
	props: {
		samples: {
			type: Array as () => Sample[],
		},
		sampling: {
			type: Object as () => null | Sampling,
		},
		samplingCollectionId: {
			type: String,
			required: true,
		},
		samplingCollection: {
			type: Object as () => SamplingCollection,
			required: true,
		},
		location: {
			type: Object as () => SLocation,
			required: true,
		},
		userLocation: {
			type: Object as () => UserLocation,
		},
	},
	components: {
		SamplingForm,
	},
	setup(props, { root, emit }) {
		const dialogIsVisible = ref(false);
		const currentSampling: Ref<Sampling | undefined | null> = ref(props.sampling);
		const currentSamples: Ref<Sample[]> = ref([]);

		function onSamplingCreate() {
			dialogIsVisible.value = true;
			const newSampling = createSampling({
				id: new ObjectId().toHexString(),
				userLocation: props.userLocation,
				location: props.location,
				samplingCollectionId: props.samplingCollectionId,
				soDepth: props.samplingCollection.soDepth || {
					minValue: {
						value: -1,
						unit: 'unit:CM',
					},
					maxValue: {
						value: -1,
						unit: 'unit:CM',
					},
				},
			});
			currentSampling.value = newSampling;
		}

		function onSamplingSave() {
			root.$store.dispatch('samplings/setRecord', currentSampling.value);
			root.$store.dispatch('samples/setRecords', currentSamples.value);

			if (props.sampling?.results && currentSampling.value?.results) {
				const prevSamples = props.sampling?.results ?? [];
				const samplesToDelete = prevSamples.filter(
					// filter: allow only samples not found in currentSampling
					(s: string) => currentSampling.value?.results?.indexOf(s) === -1
				);
				root.$store.dispatch('samples/removeRecords', samplesToDelete);
			}

			emit('samplingCreate', {
				sampling: currentSampling.value,
				samples: currentSamples.value,
			});

			dialogIsVisible.value = false;
			currentSamples.value = [];
			currentSampling.value = undefined;
		}

		function onSamplingCancel() {
			dialogIsVisible.value = false;
			currentSamples.value = [];
			currentSampling.value = undefined;
		}

		function onSamplingEdit() {
			currentSampling.value = props.sampling;
			currentSamples.value = props.samples || [];
			dialogIsVisible.value = true;
		}

		function onSamplingDelete() {
			if (props.sampling) {
				root.$store.dispatch('samples/removeRecords', props.sampling.results);
				root.$store.dispatch('samplings/removeRecord', props.sampling.id);
				emit('samplingDelete', {
					sampling: props.sampling.id,
					samples: props.sampling.results,
				});
			}
		}

		function onUpdateGeolocation() {
			if (currentSampling.value) {
				currentSampling.value = {
					...currentSampling.value,
					geometry: {
						...currentSampling.value.geometry,
						coordinates: [props.userLocation?.coords.longitude ?? -1, props.userLocation?.coords.latitude ?? -1],
					} as Point,
					properties: {
						...currentSampling.value.properties,
						geoAccuracy: props.userLocation?.coords.accuracy ?? -1,
					},
					resultTime: (props.userLocation?.timestamp
						? new Date(props.userLocation?.timestamp)
						: new Date()
					).toISOString(),
				};
			}
		}

		function onSetSamplingDepth(depth: SoDepth) {
			if (currentSampling.value) {
				currentSampling.value = {
					...currentSampling.value,
					soDepth: depth,
				};
			}
		}

		function onSetSamplingComment(comment: string) {
			if (currentSampling.value) {
				currentSampling.value = {
					...currentSampling.value,
					comment,
				};
			}
		}

		function onSetSamplingResult(sample: Sample) {
			// Update currentSamples
			const index = currentSamples.value.findIndex((s: Sample) => s.id === sample.id);
			if (index > -1) {
				currentSamples.value = [
					...currentSamples.value.slice(0, index),
					sample,
					...currentSamples.value.slice(index + 1),
				];
			} else {
				currentSamples.value = [...currentSamples.value, sample];
			}

			// Update `result` field on currentSampling
			if (currentSampling.value?.results?.indexOf(sample.id)) {
				currentSampling.value = {
					...currentSampling.value,
					results: [...currentSampling.value.results, sample.id],
				};
			}
		}

		function onDeleteSample(id: string) {
			// Update current samples, remove sample from list
			const index = currentSamples.value.findIndex((s: Sample) => s.id === id);
			if (index > -1) {
				currentSamples.value = [...currentSamples.value.slice(0, index), ...currentSamples.value.slice(index + 1)];
			} else {
				console.warn('sample not found', id);
			}

			// update currentSampling, remove sample id from result list
			const resultIndex = currentSampling.value?.results?.indexOf(id) ?? -1;
			if (currentSampling.value && resultIndex > -1) {
				currentSampling.value = {
					...currentSampling.value,
					results: [
						...(currentSampling.value.results?.slice(0, resultIndex) || []),
						...(currentSampling.value.results?.slice(resultIndex + 1) || []),
					],
				};
			}
		}

		return {
			onDeleteSample,
			onSamplingCreate,
			onSamplingDelete,
			onSamplingEdit,
			onSamplingSave,
			onSamplingCancel,
			dialogIsVisible,
			currentSampling,
			currentSamples,
			onUpdateGeolocation,
			onSetSamplingComment,
			onSetSamplingDepth,
			onSetSamplingResult,
		};
	},
});
