import { SamplingCollection, Sample, Sampling } from '@/store/types';

export interface ImportableResources {
	samples: Sample[];
	samplings: Sampling[];
	samplingCollections: SamplingCollection[];
}

const mergeFileResources = (parsedFiles: Partial<ImportableResources>[]): ImportableResources =>
	parsedFiles.reduce(
		(r, file) =>
			({
				samples: file.samples ? [...r.samples, ...file.samples] : r.samples,
				samplingCollections: file.samplingCollections
					? [...r.samplingCollections, ...file.samplingCollections]
					: r.samplingCollections,
				samplings: file.samplings ? [...r.samplings, ...file.samplings] : r.samplings,
			} as ImportableResources),
		{
			samples: [],
			samplingCollections: [],
			samplings: [],
		} as any
	);

export { mergeFileResources };
