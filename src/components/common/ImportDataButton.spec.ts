/* eslint-disable @typescript-eslint/no-explicit-any */
import { mergeFileResources, ImportableResources } from './ImportDataButton';
import { createMockSample, createMockSampling, createMockSamplingCollection } from '../../../tests/mockGenerators';

const createMockFileResource = (id: string): ImportableResources => ({
	samples: [createMockSample({ id })],
	samplingCollections: [createMockSamplingCollection({ id })],
	samplings: [createMockSampling({ id })],
});

describe('mergeFileResources', () => {
	const mockFileResources1 = createMockFileResource('1') as any;
	const mockFileResources2 = createMockFileResource('2') as any;
	const mockFileResources3 = createMockFileResource('3') as any;
	const result = mergeFileResources([mockFileResources1, mockFileResources2, mockFileResources3]) as any;

	Object.keys(mockFileResources1).forEach((key) => {
		it(`merges ${key} from each file into a single array`, () => {
			expect(result[key]).toEqual(expect.arrayContaining(mockFileResources1[key]));
			expect(result[key]).toEqual(expect.arrayContaining(mockFileResources2[key]));
			expect(result[key]).toEqual(expect.arrayContaining(mockFileResources3[key]));
		});
	});
});
