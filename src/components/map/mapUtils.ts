import { SetupContext } from '@vue/composition-api';
import { Feature, Polygon, FeatureCollection } from 'geojson';
import { EventData, Layer } from 'mapbox-gl';

export enum LayerTypes {
	Locations = 'LOCATIONS',
	ActiveLocation = 'ACTIVE_LOCATION',
	FieldBoundary = 'FIELD_BOUNDARY',
}

export const locationsClickHandler = (emit: SetupContext['emit']) => ({
	type: 'click',
	layer: LayerTypes.Locations,
	handler: (ev: EventData) => {
		const queryResults = ev.target.queryRenderedFeatures(ev.point);
		queryResults && queryResults[0].properties?.id && emit('locationClick', queryResults[0].properties?.id);
	},
});

export function createActiveLocationLayer(activeLocation: Feature): Layer {
	return {
		id: LayerTypes.ActiveLocation,
		type: 'symbol',
		source: {
			type: 'geojson',
			data: activeLocation || [],
			cluster: false,
		},
		layout: {
			'icon-image': 'circle',
			'icon-size': 1,
			'text-offset': [0, -0.5],
			'text-allow-overlap': true,
			'icon-allow-overlap': true,
		},
		paint: {
			'icon-color': '#ff00ff',
			'icon-opacity': 0.5,
		},
	};
}

export function createLocationCollectionLayer(locationCollection: FeatureCollection): Layer {
	return {
		id: LayerTypes.Locations,
		type: 'symbol',
		source: {
			type: 'geojson',
			data: locationCollection || [],
			cluster: false,
		},
		layout: {
			'text-size': {
				stops: [
					[16, 0],
					[18, 24],
				],
			},
			'icon-image': [
				'match',
				['get', 'markerType'],
				'LOCATION_INCOMPLETE',
				'circle-stroked-11',
				'LOCATION_COMPLETE',
				'triangle-15',
				'LOCATION_SKIPPED',
				'triangle-stroked-15',
				'LOCATION_COLOR_INTERPOLATE',
				'circle',
				'SURVEY_LOCATION_MARKER',
				'circle-11',
				'circle-15',
			],
			'icon-size': 1,
			'text-field': ['get', 'label'],
			'text-offset': [0, -0.75],
			'text-allow-overlap': true,
			'icon-allow-overlap': true,
		},
		paint: {
			'icon-color': '#ff00ff',
			'icon-halo-blur': 10,
			'icon-halo-color': 'rgba(0, 0, 0, 255)',
			'icon-halo-width': 10,
			'text-halo-blur': 1,
			'text-halo-color': 'rgba(0, 0, 0, 255)',
			'text-halo-width': 1,
			'text-color': '#ffffff',
		},
	};
}

export function createFieldBoundaryLayer(fieldBoundary: Feature<Polygon>): Layer {
	return {
		id: LayerTypes.FieldBoundary,
		type: 'line',
		source: {
			type: 'geojson',
			data: fieldBoundary,
		},
		paint: {
			'line-color': '#3bb2d0',
			'line-width': 2,
		},
		layout: {},
	};
}

export function createLayers(layers: {
	area?: Feature<Polygon> | null;
	locations?: FeatureCollection | null;
	activeLocation?: Feature | null;
}): Layer[] {
	const { area, locations, activeLocation } = layers;
	return [
		...(area ? [createFieldBoundaryLayer(area)] : []),
		...(locations ? [createLocationCollectionLayer(locations)] : []),
		...(activeLocation ? [createActiveLocationLayer(activeLocation)] : []),
	];
}
