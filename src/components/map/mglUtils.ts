import MapboxGL, { EventData, Map as MapboxMap, Layer, Control, GeolocateControl, MapLayerEventType } from 'mapbox-gl';
import { notEmpty } from '@/utils/typeHelpers';
import { SetupContext } from '@vue/composition-api';
import environmentIcon from '@/components/map/assets/environment-fill.png';
import circleIcon from '@/components/map/assets/circle-fill.png';

export enum ControlTypes {
	Geolocate = 'GEOLOCATE',
}

export function createMap({
	container = 'map',
	accessToken = '',
	center = [-84.55068956887425, 39.08187355127035],
	zoom = 12,
	maxBounds,
	tileset,
}: // eslint-disable-next-line @typescript-eslint/no-explicit-any
{ [key: string]: any } = {}) {
	MapboxGL.accessToken = accessToken;
	const map = new MapboxMap({
		container,
		style: `mapbox://styles/mapbox/${tileset}`, // stylesheet location
		center,
		zoom,
		maxBounds,
		refreshExpiredTiles: false,
	});
	return map;
}

export function createControls(controls: ControlTypes[]): { [key: string]: Control } {
	return controls
		.map((control) => {
			switch (control) {
				case ControlTypes.Geolocate:
					return {
						[ControlTypes.Geolocate]: new GeolocateControl({
							positionOptions: {
								enableHighAccuracy: true,
							},
							trackUserLocation: true,
							showAccuracyCircle: true,
							showUserLocation: true,
						}),
					};
				default:
					return null;
			}
		})
		.filter(notEmpty)
		.reduce((r, x) => ({ ...r, ...x }), {});
}

export function addControls(map: MapboxMap, controls: { [key: string]: Control }): void {
	Object.values(controls).forEach((control) => map.addControl(control));
}

export function addLayers(map: MapboxMap, layers: Layer[] = []) {
	layers.forEach((layer) => map.addLayer(layer));
}

export interface MapEventHandler {
	type: keyof MapLayerEventType;
	layer: string;
	handler: (ev: EventData) => void;
}

export function addEventHandlers(
	emit: SetupContext['emit'],
	map: MapboxMap,
	handlers: Array<(emit: SetupContext['emit']) => MapEventHandler>
) {
	handlers
		.map((handler) => handler(emit))
		.forEach(({ type, layer, handler }) => {
			map.on(type, layer, handler);
		});
}

export function loadIcons(map: MapboxMap) {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	map.loadImage(environmentIcon, (error: any, image: any) => {
		if (error) throw error;
		map.addImage('pin', image, { sdf: true });
	});
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	map.loadImage(circleIcon, (error: any, image: any) => {
		if (error) throw error;
		map.addImage('circle', image, { sdf: true });
	});
}
