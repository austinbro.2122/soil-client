import { Marker } from 'mapbox-gl';

export interface MarkerInstances {
	[key: string]: Marker;
}

// TODO: Reimplement this to support multiple instances of MglMap. Right now we have a single instance of markerInstances.
// This is currently okay because we only render one MglMap on a page at at time.
// - markerInstances should live in Vuex, and be keyed by MglMap instance
// - openPopup should be an action that takes a (1) map id and a (2) field id
// - we should handle that action in a Vuex plugin by looking up the right marker and clicking it
export let markerInstances: MarkerInstances = {};
export const clearMarkerInstances = () => {
	markerInstances = {};
};
export const openPopup = (fieldId: string) => {
	// close any open popups
	Object.values(markerInstances).forEach((marker: Marker) => {
		if (marker?.getPopup()?.isOpen()) {
			marker?.togglePopup();
		}
	});
	// open the popup for the field
	(markerInstances[fieldId] as any)?.togglePopup();
};
