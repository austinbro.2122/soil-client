import QRScanner from './QRScanner.vue';
import { renderWithVuetify } from '../../../tests/renderWithVuetify';
import { fireEvent, waitFor, RenderResult } from '@testing-library/vue';
import QrScanner from 'qr-scanner';

jest.mock('qr-scanner');

describe('QRScanner', () => {
	it('opens the QR code scanner when a user clicks the QR code icon button', async () => {
		const { getByRole, getByText } = renderWithVuetify(QRScanner);

		const openScannerButton = getByRole('button', { name: 'Open QR Scanner' });
		await fireEvent.click(openScannerButton);

		getByText('QR Code Scanner');
	});

	it('closes the QR code scanner when a user clicks the close icon in the scanner dialog', async () => {
		const { getByRole, getByText } = renderWithVuetify(QRScanner);
		const openScannerButton = getByRole('button', { name: 'Open QR Scanner' });
		await fireEvent.click(openScannerButton);
		const qrScannerDialog = getByText('QR Code Scanner');

		const closeScannerButton = getByRole('button', {
			name: 'Close QR Scanner',
		});
		await fireEvent.click(closeScannerButton);

		expect(qrScannerDialog).not.toBeVisible();
	});

	describe('on successful code detection', () => {
		let wrapper: RenderResult;

		beforeEach(async () => {
			(QrScanner.hasCamera as jest.Mock).mockResolvedValue(true);
			// stub the QrScanner library and just have it immediately call the onDecode callback as if a code were detected
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(QrScanner as any).mockImplementation((videoEl: Element, onDecode: Function) => {
				onDecode('qr code value');
				return {
					start: () => {
						/* noop */
					},
					stop: () => {
						/* noop */
					},
					destroy: () => {
						/* noop */
					},
				};
			});

			wrapper = renderWithVuetify(QRScanner);
			const { getByRole } = wrapper;
			const openScannerButton = getByRole('button', {
				name: 'Open QR Scanner',
			});
			await fireEvent.click(openScannerButton);

			// wait for nextTick so that vue propagates emitted events. Is there a better way to do this?
			await waitFor(() => new Promise((res) => setTimeout(res, 0)));
		});

		it("emits a 'codeDetected' event when a qr code is scanned successfully", async () => {
			expect(wrapper.emitted().codeDetected[0]).toEqual(['qr code value']);
		});

		it('closes the dialog', () => {
			const qrScannerDialog = wrapper.getByText('QR Code Scanner');
			expect(qrScannerDialog).not.toBeVisible();
		});
	});

	describe('error handling', () => {
		it('displays an error in the scanner dialog if the user has no camera', async () => {
			(QrScanner.hasCamera as jest.Mock).mockResolvedValue(false);

			const { getByRole, getByText } = renderWithVuetify(QRScanner);
			const openScannerButton = getByRole('button', {
				name: 'Open QR Scanner',
			});
			await fireEvent.click(openScannerButton);

			await waitFor(() => {
				expect(getByText('No camera detected.')).toBeInTheDocument();
			});
		});

		it('displays an error in the scanner dialog if the qr-scanner lib throws an error while starting (can happen if no camera permission)', async () => {
			(QrScanner.hasCamera as jest.Mock).mockResolvedValue(true);
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(QrScanner as any).mockImplementation(() => {
				return {
					start: () => {
						throw new Error();
					},
					stop: () => {
						/* noop */
					},
					destroy: () => {
						/* noop */
					},
				};
			});

			const { getByRole, getByText } = renderWithVuetify(QRScanner);
			const openScannerButton = getByRole('button', {
				name: 'Open QR Scanner',
			});
			await fireEvent.click(openScannerButton);

			await waitFor(() => {
				expect(getByText('No camera detected.')).toBeInTheDocument();
			});
		});
	});
});
