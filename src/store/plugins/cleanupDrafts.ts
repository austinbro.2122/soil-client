import { Store, MutationPayload } from 'vuex';
import { RootState, SamplingCollection } from '@/store/types';

const handleMutation = (store: Store<RootState>) => (mutation: MutationPayload) => {
	switch (mutation.type) {
		case 'resources/requestSubmitSamplingCollectionSuccess':
		case 'resources/requestGetAllResourcesSuccess': {
			const draftIds = mutation.payload.samplingCollections
				.map((samplingCollection: SamplingCollection) => {
					return samplingCollection.meta?.referenceIds?.find((refId) => refId.owner === 'soilstack-draft')?.id;
				})
				.filter(Boolean);

			store.dispatch('samplingCollections/removeRecords', draftIds);

			return;
		}

		default:
			return;
	}
};

function cleanupDrafts(store: Store<RootState>) {
	store.subscribe(handleMutation(store));
}

export { handleMutation };

export default cleanupDrafts;
