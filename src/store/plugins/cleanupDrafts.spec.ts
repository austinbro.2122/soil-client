import { handleMutation } from './cleanupDrafts';
import { MutationPayload } from 'vuex';
import { createMockSamplingCollection, createMockResourceMeta } from '../../../tests/mockGenerators';

describe('cleanupDrafts plugin', () => {
	const actionTypes = ['resources/requestGetAllResourcesSuccess', 'resources/requestSubmitSamplingCollectionSuccess'];

	actionTypes.forEach((actionType) => {
		describe(`on ${actionType}`, () => {
			it('removes sampling collection records from IDB that correspond to drafts that have already been submitted', () => {
				const mutation = {
					type: actionType,
					payload: {
						samplingCollections: [
							createMockSamplingCollection({
								meta: createMockResourceMeta({
									referenceIds: [{ owner: 'soilstack-draft', id: 'draft1' }],
								}),
							}),
							createMockSamplingCollection({
								meta: createMockResourceMeta({
									referenceIds: [{ owner: 'soilstack-draft', id: 'draft2' }],
								}),
							}),
						],
						samplings: [],
						samples: [],
					},
				} as MutationPayload;
				const store = { dispatch: jest.fn() };

				handleMutation(store as any)(mutation);

				expect(store.dispatch).toHaveBeenCalledWith('samplingCollections/removeRecords', ['draft1', 'draft2']);
			});
		});
	});
});
