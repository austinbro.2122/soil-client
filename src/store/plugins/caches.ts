import { Store, MutationPayload } from 'vuex';
import { RootState, Area } from '@/store/types';
import { isAreaAvailableOffline } from '@/utils/mapFetching';

const checkIfAvailableOffline = async (area: Area) => {
	const isAvailableOffline = await isAreaAvailableOffline(area);
	return { areaId: area.id, isAvailableOffline };
};

const handleMutation = (store: Store<RootState>) => (mutation: MutationPayload) => {
	switch (mutation.type) {
		case 'resources/requestGetAllResourcesSuccess': {
			const { areas }: { areas: Area[] } = mutation.payload;
			Promise.allSettled(areas.map(checkIfAvailableOffline)).then((results) => {
				results
					.filter((result) => result.status === 'fulfilled' && result.value.isAvailableOffline)
					.forEach((result) => {
						// satisfy typescript by checking result.status again
						if (result.status === 'fulfilled') {
							store.dispatch('offlineMaps/setAreaAvailableOffline', result.value.areaId);
						}
					});

				results.filter((result) => result.status === 'rejected').forEach(console.warn);
			});
			return;
		}
		default:
			return;
	}
};

function caches(store: Store<RootState>) {
	store.subscribe(handleMutation(store));
}

export { handleMutation };

export default caches;
