import { Store, MutationPayload } from 'vuex';
import VueRouter from 'vue-router';
import { RootState } from '@/store/types';

let _router: VueRouter;

const injectRouter = (router: VueRouter) => {
	_router = router;
};

const handleMutation = () => (mutation: MutationPayload) => {
	switch (mutation.type) {
		case 'resources/requestSubmitSamplingCollectionSuccess': {
			const samplingCollectionId = mutation.payload.samplingCollections[0].id;
			_router.push({ name: 'Sampling Collection Show', params: { id: samplingCollectionId } });
			return;
		}
		default:
			return;
	}
};

function redirects(store: Store<RootState>) {
	store.subscribe(handleMutation());
}

export { handleMutation, injectRouter };

export default redirects;
