import { isAreaAvailableOffline } from '@/utils/mapFetching';
import { handleMutation } from './caches';
import { MutationPayload } from 'vuex';

jest.mock('@/utils/mapFetching');

describe('caches plugin', () => {
	describe('on resources/requestGetAllResourcesSuccess', () => {
		it('sets areas as available offline if isAreaAvailableOffline is true for the area', async () => {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			const store = { dispatch: jest.fn() } as any;
			(isAreaAvailableOffline as jest.Mock).mockImplementation(async ({ id }) => {
				if (id === '0') {
					return false;
				}
				if (id === '1') {
					return true;
				}
			});
			const mutation = {
				type: 'resources/requestGetAllResourcesSuccess',
				payload: { areas: [{ id: '0' }, { id: '1' }] },
			} as MutationPayload;

			handleMutation(store)(mutation);

			await new Promise((res) => setTimeout(res, 0));

			expect(store.dispatch).toHaveBeenCalledWith('offlineMaps/setAreaAvailableOffline', '1');
			expect(store.dispatch).not.toHaveBeenCalledWith('offlineMaps/setAreaAvailableOffline', '0');
		});
	});
});
