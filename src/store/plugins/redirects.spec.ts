import { handleMutation, injectRouter } from './redirects';
import { MutationPayload } from 'vuex';

describe('redirects plugin', () => {
	describe('on resources/requestSubmitSamplingCollectionSuccess', () => {
		it('redirects to the sampling collection show route for the sampling collection that was just submitted', () => {
			const mockRouter = { push: jest.fn() } as any;

			injectRouter(mockRouter);

			const mutation = {
				type: 'resources/requestSubmitSamplingCollectionSuccess',
				payload: {
					samplingCollections: [{ id: '1' }],
					samplings: [],
					samples: [],
				},
			} as MutationPayload;

			handleMutation()(mutation);

			expect(mockRouter.push).toHaveBeenCalledWith({ name: 'Sampling Collection Show', params: { id: '1' } });
		});
	});
});
