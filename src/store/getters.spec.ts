import { Store } from 'vuex';
import { RootState } from '@/store/types';
import createTestStore from '../../tests/createTestStore';
import { createMockSamplingCollection } from '../../tests/mockGenerators';
import { createMockResourcesState } from './modules/test-utils';

describe('getters', () => {
	let store: Store<RootState>;
	beforeEach(() => {
		store = createTestStore();
	});

	describe('getSamplingCollectionStatusByFieldId', () => {
		it("returns 'SUBMITTED' if a sampling collection is submitted for the field with the given id", () => {
			store.replaceState({
				...store.state,
				resources: {
					...store.state.resources,
					samplingCollections: [createMockSamplingCollection({ featureOfInterest: 'field-1' })],
				},
			});

			expect(store.getters.getSamplingCollectionStatusByFieldId('field-1')).toEqual('SUBMITTED');
		});

		it("returns 'SUBMITTED' if submitted and draft sampling collections for the field with the given id exist", () => {
			store.replaceState({
				...store.state,
				resources: {
					...store.state.resources,
					samplingCollections: [createMockSamplingCollection({ featureOfInterest: 'field-1' })],
				},
				samplingCollections: {
					...store.state.samplingCollections,
					records: [createMockSamplingCollection({ featureOfInterest: 'field-1' })],
				},
			});

			expect(store.getters.getSamplingCollectionStatusByFieldId('field-1')).toEqual('SUBMITTED');
		});

		it("returns 'DRAFT' if a draft sampling collection exists for the field with the given id and no submitted sampling collection for the field exists.", () => {
			store.replaceState({
				...store.state,
				samplingCollections: {
					...store.state.samplingCollections,
					records: [createMockSamplingCollection({ featureOfInterest: 'field-1' })],
				},
			});

			expect(store.getters.getSamplingCollectionStatusByFieldId('field-1')).toEqual('DRAFT');
		});

		it('returns null if no sampling collections exist for the field with the given id', () => {
			expect(store.getters.getSamplingCollectionStatusByFieldId('field-1')).toEqual(null);
		});
	});

	describe('getSamplingCollectionStatusBySamplingCollectionId', () => {
		it("returns 'SUBMITTED' if a sampling collection with the given id is submitted", () => {
			store.replaceState({
				...store.state,
				resources: {
					...store.state.resources,
					samplingCollections: [createMockSamplingCollection({ featureOfInterest: 'field-1', id: 'sc1' })],
				},
			});

			expect(store.getters.getSamplingCollectionStatusBySamplingCollectionId('sc1')).toEqual('SUBMITTED');
		});

		it("returns 'DRAFT' if a draft sampling collection with the given id exists", () => {
			store.replaceState({
				...store.state,
				samplingCollections: {
					...store.state.samplingCollections,
					records: [createMockSamplingCollection({ featureOfInterest: 'field-1', id: 'sc1' })],
				},
			});

			expect(store.getters.getSamplingCollectionStatusBySamplingCollectionId('sc1')).toEqual('DRAFT');
		});

		it('returns null if no sampling collections with the given id exist', () => {
			expect(store.getters.getSamplingCollectionStatusBySamplingCollectionId('sc1')).toEqual(null);
		});
	});

	describe('getFieldGroupPath', () => {
		it("returns the group path for the group referenced in the field's meta property", () => {
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					fields: [
						{
							id: 'field1',
							meta: { groupId: 'group1' },
						} as any,
					],
				}),
				groups: {
					groups: [{ id: 'group1', name: 'Group 1', path: '/soilstack/group1/' }],
				},
			});

			expect(store.getters.getFieldGroupPath('field1')).toEqual('/soilstack/group1/');
		});
	});

	describe('getIsFieldWithinGroupPath', () => {
		it('returns true if the group the field is in matches the groupPath', () => {
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					fields: [
						{
							id: 'field1',
							meta: { groupId: 'group1' },
						} as any,
					],
				}),
				groups: {
					groups: [{ id: 'group1', name: 'Group 1', path: '/soilstack/group1/' }],
				},
			});

			expect(store.getters.getIsFieldWithinGroupPath('field1', '/soilstack/group1/')).toBe(true);
		});

		it('returns true if the group the field is in is a subgroup of the groupPath', () => {
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					fields: [
						{
							id: 'field1',
							meta: { groupId: 'group1' },
						} as any,
					],
				}),
				groups: {
					groups: [{ id: 'group1', name: 'Group 1', path: '/soilstack/group1/subgroup/' }],
				},
			});

			expect(store.getters.getIsFieldWithinGroupPath('field1', '/soilstack/group1/')).toBe(true);
		});

		it('returns false if the group the field is in is not within the groupPath', () => {
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					fields: [
						{
							id: 'field1',
							meta: { groupId: 'group2' },
						} as any,
					],
				}),
				groups: {
					groups: [{ id: 'group2', name: 'Group 2', path: '/soilstack/group2/' }],
				},
			});

			expect(store.getters.getIsFieldWithinGroupPath('field1', '/soilstack/group1/')).toBe(false);
		});
	});
});
