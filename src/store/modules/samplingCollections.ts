import { ActionTree, GetterTree } from 'vuex';
import { SamplingCollection, Record, ResourceState, RootState, Sampling, Sample } from '@/store/types';
import { createBaseModule } from '@/store/utils/createBaseModule';
import { createUseAllResourceHook } from '@/store/utils/createUseAllResourceHook';

interface SamplingCollectionsState extends ResourceState {
	records: SamplingCollection[] & Record[];
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
}

function createInitialState(): SamplingCollectionsState {
	return {
		records: [],
		isLoading: false,
		isLoaded: false,
		error: false,
	};
}

const actions: ActionTree<SamplingCollectionsState, RootState> = {
	async deleteSamplingCollection({ dispatch, rootGetters }, id): Promise<void> {
		const samplingIds = rootGetters['samplings/getBySamplingCollectionId'](id).map(({ id }: Sampling) => id);
		const sampleIds = rootGetters['samples/getBySamplingCollectionIds']([id]).map(({ id }: Sample) => id);
		await Promise.all([
			dispatch('removeRecord', id),
			dispatch('samplings/removeRecords', samplingIds, { root: true }),
			dispatch('samples/removeRecords', sampleIds, { root: true }),
		]);
	},
};

export const getters: GetterTree<SamplingCollectionsState, RootState> = {
	getByFieldId: (state) => (fieldId: string) =>
		state.records.filter(({ featureOfInterest }) => featureOfInterest === fieldId),
};

const baseModule = createBaseModule('samplingCollections', createInitialState);

export const useAllSamplingCollections = createUseAllResourceHook('samplingCollections');

export default {
	...baseModule,
	actions: {
		...baseModule.actions,
		...actions,
	},
	getters: {
		...baseModule.getters,
		...getters,
	},
};
