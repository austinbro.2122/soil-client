import { AsyncState } from '@/store/types';
import createReassignModule, { actions, getters, useReassign } from './reassign';
import soilApiService from '../../services/soilApiService';
import createTestStore from '../../../tests/createTestStore';

jest.mock('../../services/soilApiService');

const createMockReassignState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
}: Partial<AsyncState> = {}): AsyncState => ({
	isLoading,
	isLoaded,
	error,
});

describe('reassign module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('requestReassign', () => {
			it("commits 'requestReassign' mutation before making request", async () => {
				await (actions.requestReassign as any)(
					{ commit: commitSpy },
					{ groupId: 'mock group id', fieldId: 'mock field id' }
				);

				expect(commitSpy).toHaveBeenCalledWith('requestReassign');
			});

			it("commits 'requestReassignSuccess' mutation on success", async () => {
				(soilApiService.reassign as jest.Mock).mockResolvedValue({});

				await (actions.requestReassign as any)(
					{ commit: commitSpy },
					{ groupId: 'mock group id', fieldId: 'mock field id' }
				);

				expect(commitSpy).toHaveBeenCalledWith('requestReassignSuccess');
			});

			it("commits 'requestReassignError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.reassign as jest.Mock).mockRejectedValueOnce(error);

				await (actions.requestReassign as any)(
					{ commit: commitSpy },
					{ groupId: 'mock group id', fieldId: 'mock field id' }
				);

				expect(commitSpy).toHaveBeenCalledWith('requestReassignError', error);
			});

			it('passes groupId and fieldId along to soilApiServer', async () => {
				await (actions.requestReassign as any)(
					{ commit: commitSpy },
					{ groupId: 'mock group id', fieldId: 'mock field id' }
				);

				expect(soilApiService.reassign).toHaveBeenCalledWith({ groupId: 'mock group id', fieldId: 'mock field id' });
			});
		});

		describe('acknowledgeReassignError', () => {
			it("commits 'resetReassignRequest' mutation", async () => {
				await (actions.acknowledgeReassignError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetReassignRequest');
			});
		});
	});

	describe('mutations', () => {
		let reassignModule: any;

		beforeEach(() => {
			reassignModule = createReassignModule();
		});

		describe('requestReassign', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				reassignModule.state = createMockReassignState({
					isLoading: false,
					isLoaded: true,
					error: 'error',
				});

				reassignModule.mutations.requestReassign(reassignModule.state);

				expect(reassignModule.state).toHaveProperty('isLoading', true);
				expect(reassignModule.state).toHaveProperty('isLoaded', false);
				expect(reassignModule.state).toHaveProperty('error', null);
			});
		});

		describe('requestReassignSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null, and puts groups into state', () => {
				reassignModule.state = createMockReassignState({
					isLoading: true,
					isLoaded: false,
					error: 'error',
				});

				reassignModule.mutations.requestReassignSuccess(reassignModule.state);

				expect(reassignModule.state).toHaveProperty('isLoading', false);
				expect(reassignModule.state).toHaveProperty('isLoaded', true);
				expect(reassignModule.state).toHaveProperty('error', null);
			});
		});

		describe('requestReassignError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				reassignModule.state = createMockReassignState({
					isLoading: true,
					isLoaded: true,
					error: null,
				});

				reassignModule.mutations.requestReassignError(reassignModule.state, 'error');

				expect(reassignModule.state).toHaveProperty('isLoading', false);
				expect(reassignModule.state).toHaveProperty('isLoaded', false);
				expect(reassignModule.state).toHaveProperty('error', 'error');
			});
		});

		describe('resetReassignRequest', () => {
			it('sets submitIsLoading to false and submitError to null', () => {
				reassignModule.state = createMockReassignState({
					isLoading: true,
					error: 'error',
				});

				reassignModule.mutations.resetReassignRequest(reassignModule.state);

				expect(reassignModule.state).toHaveProperty('isLoading', false);
				expect(reassignModule.state).toHaveProperty('error', null);
			});
		});
	});

	describe('getters', () => {
		describe('getIsLoading', () => {
			it('returns isLoading', () => {
				const state = createMockReassignState({ isLoading: true });

				const actual = (getters as any).getIsLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockReassignState({ isLoaded: true });

				const actual = (getters as any).getIsLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasError', () => {
			it('returns true if error is not null', () => {
				const state = createMockReassignState({ error: 'error' });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockReassignState({ error: null });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(false);
			});
		});
	});

	describe('hooks', () => {
		describe('useReassign', () => {
			it('returns computedRefs of group state', () => {
				const store = createTestStore();
				const reassign = createMockReassignState({
					isLoaded: true,
				});
				store.replaceState({
					...store.state,
					reassign,
				});

				const actual = useReassign(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(true);
				expect(actual.hasError.value).toEqual(false);
			});
		});
	});
});
