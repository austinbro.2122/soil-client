import { computed } from '@vue/composition-api';
import { ActionTree, MutationTree, Module, Store, GetterTree } from 'vuex';
import { AsyncState, RootState } from '@/store/types';
import soilApiService from '@/services/soilApiService';

function createInitialState() {
	return {
		isLoading: false,
		isLoaded: false,
		error: null,
	};
}

const actions: ActionTree<AsyncState, RootState> = {
	async requestReassign({ commit }, { fieldId, groupId }): Promise<boolean> {
		commit('requestReassign');
		try {
			await soilApiService.reassign({ fieldId, groupId });
			commit('requestReassignSuccess');
			return true;
		} catch (error) {
			commit('requestReassignError', error);
			return false;
		}
	},
	acknowledgeReassignError({ commit }) {
		commit('resetReassignRequest');
	},
};

const mutations: MutationTree<AsyncState> = {
	requestReassign(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestReassignSuccess(state) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
	},
	requestReassignError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
	},
	resetReassignRequest(state) {
		state.isLoading = false;
		state.error = null;
	},
};

const getters: GetterTree<AsyncState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
};

const createReassignModule = (): Module<AsyncState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useReassign = (store: Store<RootState>) => {
	const isLoading = computed(() => store.getters['reassign/getIsLoading']);
	const isLoaded = computed(() => store.getters['reassign/getIsLoaded']);
	const hasError = computed(() => store.getters['reassign/getHasError']);

	return {
		isLoading,
		isLoaded,
		hasError,
	};
};

export { actions, mutations, getters, useReassign };

export default createReassignModule;
