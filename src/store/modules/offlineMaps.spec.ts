import { Store } from 'vuex';
import { RootState } from '@/store/types';
import { fetchOfflineMapResources } from '@/utils/mapFetching';
import offlineMapsModule, { mutations, actions, useOfflineMapAsyncState, OfflineMapsState } from './offlineMaps';
import { createMockField, createMockArea } from '../../../tests/mockGenerators';
import createTestStore from '../../../tests/createTestStore';
import { createMockResourcesState } from './test-utils';

jest.mock('@/utils/mapFetching');

describe('offlineMaps module', () => {
	describe('initial state', () => {
		it('is an empty object', () => {
			expect(offlineMapsModule.state).toEqual({});
		});
	});

	describe('mutations', () => {
		describe('fetchOfflineMapRequest', () => {
			it('sets isLoading to true, isLoaded to false, error to null for given area', () => {
				const state = {};

				mutations.fetchOfflineMapRequest(state, { areaId: '0' });

				expect(state).toEqual({
					0: { isLoading: true, isLoaded: false, error: null },
				});
			});
		});

		describe('fetchOfflineMapSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null for given area', () => {
				const state = {};

				mutations.fetchOfflineMapSuccess(state, { areaId: '0' });

				expect(state).toEqual({
					0: { isLoading: false, isLoaded: true, error: null },
				});
			});
		});

		describe('fetchOfflineMapError', () => {
			it('sets isLoading to false, isLoaded to false, error to given error for given area', () => {
				const state = {};

				mutations.fetchOfflineMapError(state, { areaId: '0', error: 'error' });

				expect(state).toEqual({
					0: { isLoading: false, isLoaded: false, error: 'error' },
				});
			});
		});
	});

	describe('actions', () => {
		describe('fetchOfflineMap', () => {
			let store: Store<RootState>;

			beforeEach(() => {
				store = createTestStore();
				const field1 = createMockField({ id: '0', areas: ['area1', 'area2'] });
				const area1 = createMockArea({ id: 'area1' });
				const area2 = createMockArea({ id: 'area2' });
				store.replaceState({
					...store.state,
					resources: {
						...store.state.resources,
						fields: [field1],
						areas: [area1, area2],
					},
				});
			});

			it('commits fetchOfflineMapRequest for each given area in given field', () => {
				store.commit = jest.fn();

				store.dispatch('offlineMaps/fetchOfflineMap', '0');

				expect(store.commit).toHaveBeenCalledWith('offlineMaps/fetchOfflineMapRequest', { areaId: 'area1' }, undefined);
				expect(store.commit).toHaveBeenCalledWith('offlineMaps/fetchOfflineMapRequest', { areaId: 'area2' }, undefined);
			});

			it('commits fetchOfflineMapSuccess for each area in given field upon fetchOfflineMapResources success', async () => {
				store.commit = jest.fn();
				(fetchOfflineMapResources as jest.Mock).mockResolvedValue(true);

				store.dispatch('offlineMaps/fetchOfflineMap', '0');

				await new Promise((res) => setTimeout(res, 0));

				expect(store.commit).toHaveBeenCalledWith('offlineMaps/fetchOfflineMapSuccess', { areaId: 'area1' }, undefined);
				expect(store.commit).toHaveBeenCalledWith('offlineMaps/fetchOfflineMapSuccess', { areaId: 'area2' }, undefined);
			});

			it('commits fetchOfflineMapError for each area in given field upon fetchOfflineMapResources failure', async () => {
				store.commit = jest.fn();
				(fetchOfflineMapResources as jest.Mock).mockRejectedValue('error');

				store.dispatch('offlineMaps/fetchOfflineMap', '0');

				await new Promise((res) => setTimeout(res, 0));

				expect(store.commit).toHaveBeenCalledWith(
					'offlineMaps/fetchOfflineMapError',
					{ areaId: 'area1', error: 'error' },
					undefined
				);
				expect(store.commit).toHaveBeenCalledWith(
					'offlineMaps/fetchOfflineMapError',
					{ areaId: 'area2', error: 'error' },
					undefined
				);
			});
		});

		describe('setAreaAvailableOffline', () => {
			it('commits fetchOfflineMapSuccess for given area', () => {
				const commit = jest.fn();

				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				(actions as any).setAreaAvailableOffline({ commit }, '0');

				expect(commit).toHaveBeenCalledWith('fetchOfflineMapSuccess', { areaId: '0' });
			});
		});
	});

	describe('hooks', () => {
		describe('useOfflineMapAsyncState', () => {
			const createStoreWithOfflineMapsState = (offlineMapsState: OfflineMapsState) => {
				const store = createTestStore();
				store.replaceState({
					...store.state,
					offlineMaps: offlineMapsState,
					resources: createMockResourcesState({
						fields: [
							createMockField({
								id: '0',
								areas: ['area1', 'area2'],
							}),
						],
						areas: [
							createMockArea({
								id: 'area1',
							}),
							createMockArea({
								id: 'area2',
							}),
						],
					}),
				});
				return store;
			};

			describe('isLoading', () => {
				it('is true if any areas belonging to given field id are loading', () => {
					const store = createStoreWithOfflineMapsState({
						area1: { isLoading: true, isLoaded: false, error: null },
						area2: { isLoading: false, isLoaded: true, error: null },
					});

					const { isLoading } = useOfflineMapAsyncState(store)('0');

					expect(isLoading).toBe(true);
				});

				it('is false if no areas belonging to given field id are loading', () => {
					const store = createStoreWithOfflineMapsState({
						area1: { isLoading: false, isLoaded: true, error: null },
						area2: { isLoading: false, isLoaded: true, error: null },
					});

					const { isLoading } = useOfflineMapAsyncState(store)('0');

					expect(isLoading).toBe(false);
				});
			});

			describe('isLoaded', () => {
				it('is true if all areas belonging to given field id are loaded', () => {
					const store = createStoreWithOfflineMapsState({
						area1: { isLoading: false, isLoaded: true, error: null },
						area2: { isLoading: false, isLoaded: true, error: null },
					});

					const { isLoaded } = useOfflineMapAsyncState(store)('0');

					expect(isLoaded).toBe(true);
				});

				it('is false if any areas belonging to given field id are not loaded', () => {
					const store = createStoreWithOfflineMapsState({
						area1: { isLoading: false, isLoaded: true, error: null },
						area2: { isLoading: true, isLoaded: false, error: null },
					});

					const { isLoaded } = useOfflineMapAsyncState(store)('0');

					expect(isLoaded).toBe(false);
				});
			});

			describe('hasError', () => {
				it('is true if any areas belonging to given field id have an error', () => {
					const store = createStoreWithOfflineMapsState({
						area1: { isLoading: false, isLoaded: true, error: null },
						area2: { isLoading: false, isLoaded: true, error: 'error' },
					});

					const { hasError } = useOfflineMapAsyncState(store)('0');

					expect(hasError).toBe(true);
				});

				it('is false if no areas belonging to given field id have an error', () => {
					const store = createStoreWithOfflineMapsState({
						area1: { isLoading: false, isLoaded: true, error: null },
						area2: { isLoading: false, isLoaded: true, error: null },
					});

					const { hasError } = useOfflineMapAsyncState(store)('0');

					expect(hasError).toBe(false);
				});
			});
		});
	});
});
