import { createMockSamplingCollection } from '../../../tests/mockGenerators';
import createTestStore from '../../../tests/createTestStore';

describe('samplingCollections module', () => {
	describe('getters', () => {
		describe('getByFieldId', () => {
			it('returns an array of the sampling collections that refer to the given field id', () => {
				const store = createTestStore();
				const samplingCollection1 = createMockSamplingCollection({
					id: 'sc1',
					featureOfInterest: 'field1',
				});
				const samplingCollection2 = createMockSamplingCollection({
					id: 'sc2',
					featureOfInterest: 'field1',
				});
				const samplingCollection3 = createMockSamplingCollection({
					id: 'sc3',
					featureOfInterest: 'field2',
				});
				store.replaceState({
					...store.state,
					samplingCollections: {
						records: [samplingCollection1, samplingCollection2, samplingCollection3],
					},
				});

				const actual = store.getters['samplingCollections/getByFieldId']('field1');

				expect(actual).toEqual([samplingCollection1, samplingCollection2]);
			});
		});
	});
});
