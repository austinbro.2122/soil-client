import { createMockSample, createMockSampling, createMockSamplingCollection } from '../../../tests/mockGenerators';
import createTestStore from '../../../tests/createTestStore';

describe('samples module', () => {
	describe('getters', () => {
		describe('getBySamplingsIds (utilizing samplings.results as the source of truth, NOT sample.resultOf)', () => {
			it('returns samples belonging to samplings with given ids', () => {
				const store = createTestStore();
				const sampling1 = createMockSampling({ id: 'sampling1', results: ['1', '2'] });
				const sampling2 = createMockSampling({ id: 'sampling2', results: ['4'] });
				const sample1 = createMockSample({ id: '1', resultOf: 'sampling1' });
				const sample2 = createMockSample({ id: '2', resultOf: 'sampling1' });
				// sample3 is not referenced by sampling1 and should NOT be returned
				const sample3 = createMockSample({ id: '3', resultOf: 'sampling1' });
				const sample4 = createMockSample({ id: '4', resultOf: 'sampling2' });
				store.replaceState({
					...store.state,
					samplings: {
						records: [sampling1, sampling2],
					},
					samples: {
						records: [sample1, sample2, sample3, sample4],
					},
				});

				const result = store.getters['samples/getBySamplingIds'](['sampling1', 'sampling2']);

				expect(result).toEqual([sample1, sample2, sample4]);
			});
		});

		describe('getBySamplingCollectionIds', () => {
			it('returns samples belonging to samplings belonging to sampling collections with given ids', () => {
				const store = createTestStore();
				const samplingCollection1 = createMockSamplingCollection({ id: 'sc1', members: ['sampling1'] });
				const sampling1 = createMockSampling({ id: 'sampling1', memberOf: 'sampling1', results: ['1', '2'] });
				// sampling2 is not referenced by samplingCollection1 and should NOT be returned
				const sampling2 = createMockSampling({ id: 'sampling2', memberOf: 'sampling1', results: ['4'] });
				const sample1 = createMockSample({ id: '1', resultOf: 'sampling1' });
				const sample2 = createMockSample({ id: '2', resultOf: 'sampling1' });
				// sample3 is not referenced by sampling1 and should NOT be returned
				const sample3 = createMockSample({ id: '3', resultOf: 'sampling1' });
				// sample4 belongs to sampling2 which is not referenced by samplingCollection1, and should NOT be returned
				const sample4 = createMockSample({ id: '4', resultOf: 'sampling2' });
				store.replaceState({
					...store.state,
					samplingCollections: {
						records: [samplingCollection1],
					},
					samplings: {
						records: [sampling1, sampling2],
					},
					samples: {
						records: [sample1, sample2, sample3, sample4],
					},
				});

				const result = store.getters['samples/getBySamplingCollectionIds'](['sc1']);

				expect(result).toEqual([sample1, sample2]);
			});
		});
	});
});
