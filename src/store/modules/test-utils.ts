import { ResourcesState } from './resources';
import { GroupsState } from './group';

export const createMockResourcesState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
	submitIsLoading = false,
	submitError = null,
	fields = [],
	areas = [],
	stratifications = [],
	locationCollections = [],
	locations = [],
	samples = [],
	samplings = [],
	samplingCollections = [],
}: Partial<ResourcesState> = {}): ResourcesState => ({
	isLoading,
	isLoaded,
	error,
	submitIsLoading,
	submitError,
	fields,
	areas,
	stratifications,
	locationCollections,
	locations,
	samples,
	samplings,
	samplingCollections,
});

export const createMockGroupsState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
	groups = [],
	groupTree = [],
}: Partial<GroupsState> = {}): GroupsState => ({
	isLoading,
	isLoaded,
	error,
	groups,
	groupTree,
});
