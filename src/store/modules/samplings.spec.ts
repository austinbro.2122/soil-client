import { createMockSampling, createMockSamplingCollection } from '../../../tests/mockGenerators';
import createTestStore from '../../../tests/createTestStore';

describe('samplings module', () => {
	describe('getters', () => {
		describe('getBySamplingCollectionIds (utilizing samplingCollection.members as the source of truth, NOT sampling.memberOf)', () => {
			it('returns samplings belonging to sampling collections with given ids', () => {
				const store = createTestStore();
				const samplingCollection1 = createMockSamplingCollection({ id: 'sc1', members: ['0', '1'] });
				const samplingCollection2 = createMockSamplingCollection({ id: 'sc2', members: ['3'] });
				const sampling1 = createMockSampling({ id: '0', memberOf: 'sc1' });
				const sampling2 = createMockSampling({ id: '1', memberOf: 'sc1' });
				// sampling3 is not referenced by samplingCollection1 and should NOT be returned
				const sampling3 = createMockSampling({ id: '2', memberOf: 'sc1' });
				const sampling4 = createMockSampling({ id: '3', memberOf: 'sc2' });
				store.replaceState({
					...store.state,
					samplings: {
						records: [sampling1, sampling2, sampling3, sampling4],
					},
					samplingCollections: {
						records: [samplingCollection1, samplingCollection2],
					},
				});

				const result = store.getters['samplings/getBySamplingCollectionIds'](['sc1', 'sc2']);

				expect(result).toEqual([sampling1, sampling2, sampling4]);
			});
		});

		describe('getbySamplingCollectionId', () => {
			it('returns samplings belonging to sampling collection with given id', () => {
				const store = createTestStore();
				const samplingCollection = createMockSamplingCollection({ id: 'sc1', members: ['0', '1'] });
				const sampling1 = createMockSampling({ id: '0', memberOf: 'sc1' });
				const sampling2 = createMockSampling({ id: '1', memberOf: 'sc1' });
				// sampling3 is not referenced by samplingCollection and should NOT be returned
				const sampling3 = createMockSampling({ id: '2', memberOf: 'sc1' });
				store.replaceState({
					...store.state,
					samplings: {
						records: [sampling1, sampling2, sampling3],
					},
					samplingCollections: {
						records: [samplingCollection],
					},
				});

				const result = store.getters['samplings/getBySamplingCollectionId']('sc1');

				expect(result).toEqual([sampling1, sampling2]);
			});
		});

		describe('getBySamplingCollectionIdsKeyedByLocationId', () => {
			it('returns samplings belonging to sampling collections with given ids, in an object keyed by location id', () => {
				const store = createTestStore();
				const samplingCollection = createMockSamplingCollection({ id: 'sc1', members: ['0', '1'] });
				const sampling1 = createMockSampling({ id: '0', memberOf: 'sc1', featureOfInterest: 'loc0' });
				const sampling2 = createMockSampling({ id: '1', memberOf: 'sc1', featureOfInterest: 'loc1' });
				// sampling3 is not referenced by samplingCollection and should NOT be returned
				const sampling3 = createMockSampling({ id: '2', memberOf: 'sc1', featureOfInterest: 'loc2' });
				store.replaceState({
					...store.state,
					samplings: {
						records: [sampling1, sampling2, sampling3],
					},
					samplingCollections: {
						records: [samplingCollection],
					},
				});

				const result = store.getters['samplings/getBySamplingCollectionIdsKeyedByLocationId'](['sc1']);

				expect(result).toEqual({
					loc0: sampling1,
					loc1: sampling2,
				});
			});
		});
	});
});
