import { Sampling, Record, ResourceState, RootState, SamplingCollection } from '@/store/types';
import { createBaseModule } from '@/store/utils/createBaseModule';
import { createUseAllResourceHook } from '@/store/utils/createUseAllResourceHook';
import { GetterTree } from 'vuex';

export interface SamplingsState extends ResourceState {
	records: Sampling[] & Record[];
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
}

function createInitialState(): SamplingsState {
	return {
		records: [],
		isLoading: false,
		isLoaded: false,
		error: false,
	};
}

export const getters: GetterTree<SamplingsState, RootState> = {
	getBySamplingCollectionIds: (_, getters, __, rootGetters) => (samplingCollectionIds: string[]) => {
		const samplingCollections = rootGetters['samplingCollections/getByIds'](samplingCollectionIds);
		const samplingIds = samplingCollections.flatMap(
			(samplingCollection: SamplingCollection) => samplingCollection.members
		);
		return getters.getByIds(samplingIds);
	},

	getBySamplingCollectionId: (_, getters) => (samplingCollectionId: string) =>
		getters.getBySamplingCollectionIds([samplingCollectionId]),

	getBySamplingCollectionIdsKeyedByLocationId: (_, getters) => (samplingCollectionIds: string[]) =>
		getters.getBySamplingCollectionIds(samplingCollectionIds).reduce(
			(r: { [key: string]: Sampling }, sampling: Sampling) => ({
				...r,
				[sampling.featureOfInterest]: sampling,
			}),
			{}
		),
};

const baseModule = createBaseModule('samplings', createInitialState);

export const useAllSamplings = createUseAllResourceHook('samplings');

export default {
	...baseModule,
	getters: {
		...baseModule.getters,
		...getters,
	},
};
