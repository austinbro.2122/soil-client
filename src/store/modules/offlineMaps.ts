import { Module, MutationTree, GetterTree, ActionTree, Store } from 'vuex';
import { AsyncState, RootState, Area, Field } from '@/store/types';
import { fetchOfflineMapResources } from '@/utils/mapFetching';
import Vue from 'vue';

export interface OfflineMapsState {
	[areaId: string]: AsyncState;
}

function createInitialState(): OfflineMapsState {
	return {};
}

const mutations: MutationTree<OfflineMapsState> = {
	fetchOfflineMapRequest(state, { areaId }: { areaId: string }) {
		Vue.set(state, areaId, {
			isLoading: true,
			isLoaded: false,
			error: null,
		});
	},
	fetchOfflineMapSuccess(state, { areaId }: { areaId: string }) {
		Vue.set(state, areaId, {
			isLoading: false,
			isLoaded: true,
			error: null,
		});
	},
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	fetchOfflineMapError(state, { areaId, error }: { areaId: string; error: any }) {
		Vue.set(state, areaId, {
			isLoading: false,
			isLoaded: false,
			error,
		});
	},
};

const actions: ActionTree<OfflineMapsState, RootState> = {
	async fetchOfflineMap({ commit, rootGetters }, fieldId: string) {
		const field: Field = rootGetters['resources/getFieldById'](fieldId);
		const areas: Area[] = rootGetters['resources/getAreasByIds'](field.areas);
		areas.forEach(async (area: Area) => {
			commit('fetchOfflineMapRequest', { areaId: area.id });
			try {
				await fetchOfflineMapResources(area);
				commit('fetchOfflineMapSuccess', { areaId: area.id });
			} catch (error) {
				commit('fetchOfflineMapError', { areaId: area.id, error });
			}
		});
	},
	setAreaAvailableOffline({ commit }, areaId: string) {
		commit('fetchOfflineMapSuccess', { areaId });
	},
};

const getters: GetterTree<OfflineMapsState, RootState> = {};

const offlineMapsModule: Module<OfflineMapsState, RootState> = {
	namespaced: true,
	state: createInitialState(),
	mutations,
	actions,
	getters,
};

const useOfflineMapAsyncState = (store: Store<RootState>) => (fieldId: string) => {
	const field = store.state.resources.fields.find((field: Field) => field.id === fieldId);
	const isLoading = field?.areas?.some((id: string) => store.state.offlineMaps[id]?.isLoading) ?? false;
	const isLoaded = field?.areas?.every((id: string) => store.state.offlineMaps[id]?.isLoaded) ?? false;
	const hasError = field?.areas?.some((id: string) => store.state.offlineMaps[id]?.error) ?? true;
	return {
		isLoading,
		isLoaded,
		hasError,
	};
};

export { mutations, actions, getters, useOfflineMapAsyncState };

export default offlineMapsModule;
