import { computed } from '@vue/composition-api';
import { ActionTree, MutationTree, Module, Store, GetterTree } from 'vuex';
import {
	Field,
	Area,
	Stratification,
	LocationCollection,
	Location,
	Sample,
	Sampling,
	SamplingCollection,
	AsyncState,
	RootState,
	FieldStatus,
} from '@/store/types';
import soilApiService from '@/services/soilApiService';

export interface ResourcesState extends AsyncState {
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
	submitIsLoading: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	submitError: any;
	fields: Field[];
	areas: Area[];
	stratifications: Stratification[];
	locationCollections: LocationCollection[];
	locations: Location[];
	samples: Sample[];
	samplings: Sampling[];
	samplingCollections: SamplingCollection[];
}

function createInitialState() {
	return {
		isLoading: false,
		isLoaded: false,
		error: null,
		submitIsLoading: false,
		submitError: null,
		fields: [],
		areas: [],
		stratifications: [],
		locationCollections: [],
		locations: [],
		samples: [],
		samplings: [],
		samplingCollections: [],
	};
}

const actions: ActionTree<ResourcesState, RootState> = {
	async getAllResources({ commit }): Promise<void> {
		commit('requestGetAllResources');
		try {
			const resources = await soilApiService.getAllResources();
			commit('requestGetAllResourcesSuccess', resources);
		} catch (error) {
			commit('requestGetAllResourcesError', error);
		}
	},
	async submitSamplingCollection(
		{ commit },
		{
			populatedSamplingCollection,
			groupId,
		}: {
			populatedSamplingCollection: object;
			groupId: string;
		}
	): Promise<void> {
		commit('requestSubmitSamplingCollection');
		try {
			const resources = await soilApiService.submitSamplingCollection(populatedSamplingCollection, groupId);
			commit('requestSubmitSamplingCollectionSuccess', resources);
		} catch (error) {
			commit('requestSubmitSamplingCollectionError', error);
		}
	},
	acknowledgeSubmitSamplingCollectionError({ commit }) {
		commit('resetSubmitSamplingCollectionRequest');
	},
};

const mutations: MutationTree<ResourcesState> = {
	requestGetAllResources(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestGetAllResourcesSuccess(state, resources) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
		state.fields = resources.fields;
		state.areas = resources.areas;
		state.stratifications = resources.stratifications;
		state.locationCollections = resources.locationCollections;
		state.locations = resources.locations;
		state.samples = resources.samples;
		state.samplings = resources.samplings;
		state.samplingCollections = resources.samplingCollections;
	},
	requestGetAllResourcesError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
	},
	requestSubmitSamplingCollection(state) {
		state.submitIsLoading = true;
		state.submitError = null;
	},
	requestSubmitSamplingCollectionSuccess(state, resources) {
		state.submitIsLoading = false;
		state.submitError = null;
		state.samples.push(...resources.samples);
		state.samplings.push(...resources.samplings);
		state.samplingCollections.push(...resources.samplingCollections);
	},
	requestSubmitSamplingCollectionError(state, error) {
		state.submitIsLoading = false;
		state.submitError = error;
	},
	resetSubmitSamplingCollectionRequest(state) {
		state.submitIsLoading = false;
		state.submitError = null;
	},
};

const getters: GetterTree<ResourcesState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
	getSubmitIsLoading: (state) => state.submitIsLoading,
	getSubmitHasError: (state) => Boolean(state.submitError),
	getFields: (state) => state.fields,
	getAreas: (state) => state.areas,
	getStratifications: (state) => state.stratifications,
	getLocationCollections: (state) => state.locationCollections,
	getLocations: (state) => state.locations,
	getSamples: (state) => state.samples,
	getSamplings: (state) => state.samplings,
	getSamplingCollections: (state) => state.samplingCollections,
	getFieldById: (state) => (fieldId: string) => state.fields.find(({ id }: Field) => fieldId === id),
	getAreaById: (state) => (areaId: string) => state.areas.find(({ id }: Area) => areaId === id),
	getLocationCollectionById: (state) => (locationCollectionId: string) =>
		state.locationCollections.find(({ id }: LocationCollection) => locationCollectionId === id),
	getSamplingCollectionById: (state) => (samplingCollectionId: string) =>
		state.samplingCollections.find(({ id }: SamplingCollection) => samplingCollectionId === id),
	getFieldsByIds: (state) => (fieldIds: string[]) => state.fields.filter(({ id }: Field) => fieldIds.includes(id)),
	getAreasByIds: (state) => (areaIds: string[]) => state.areas.filter(({ id }: Area) => areaIds.includes(id)),
	getLocationsByIds: (state) => (locationIds: string[]) =>
		state.locations.filter(({ id }: Location) => locationIds.includes(id)),
	getLocationCollectionsByIds: (state) => (locationCollectionIds: string[]) =>
		state.locationCollections.filter(({ id }: LocationCollection) => locationCollectionIds.includes(id)),
	getSamplingCollectionsByIds: (state) => (samplingCollectionIds: string[]) =>
		state.samplingCollections.filter(({ id }: SamplingCollection) => samplingCollectionIds.includes(id)),
	getSamplingsByIds: (state) => (samplingIds: string[]) =>
		state.samplings.filter(({ id }: Sampling) => samplingIds.includes(id)),
	getSamplesByIds: (state) => (sampleIds: string[]) => state.samples.filter(({ id }: Sample) => sampleIds.includes(id)),
	getAreasByFieldIds: (_, getters) => (fieldIds: string[]) => {
		const fields: Field[] = getters.getFieldsByIds(fieldIds);
		const areaIds = fields.map(({ areas }) => areas).flat();
		return getters.getAreasByIds(areaIds);
	},
	getAreasByFieldId: (_, getters) => (fieldId: string) => getters.getAreasByFieldIds([fieldId]),
	getStratificationsByFieldIds: (state, getters) => (fieldIds: string[]) => {
		const fields: Field[] = getters.getFieldsByIds(fieldIds);
		const areaIds = fields.map(({ areas }) => areas).flat();
		return state.stratifications.filter(({ object }: Stratification) => areaIds.includes(object));
	},
	getStratificationsByFieldId: (_, getters) => (fieldId: string) => getters.getStratificationsByFieldIds([fieldId]),
	getLocationCollectionsByFieldIds: (state) => (fieldIds: string[]) =>
		state.locationCollections.filter(({ featureOfInterest }: LocationCollection) =>
			fieldIds.includes(featureOfInterest)
		),
	getLocationCollectionsByFieldId: (_, getters) => (fieldId: string) =>
		getters.getLocationCollectionsByFieldIds([fieldId]),
	getLocationsByFieldIds: (_, getters) => (fieldIds: string[]) => {
		const locationCollections: LocationCollection[] = getters.getLocationCollectionsByFieldIds(fieldIds);
		const locationIds = locationCollections.map(({ features }) => features).flat();
		return getters.getLocationsByIds(locationIds);
	},
	getLocationsByFieldId: (_, getters) => (fieldId: string) => getters.getLocationsByFieldIds([fieldId]),
	getSamplingCollectionsByFieldId: (_, getters) => (fieldId: string) =>
		getters.getSamplingCollections.filter(({ featureOfInterest }: SamplingCollection) => fieldId === featureOfInterest),
	getSamplingsBySamplingCollectionIds: (_, getters) => (samplingCollectionIds: string[]) => {
		const samplingCollections: SamplingCollection[] = getters.getSamplingCollectionsByIds(samplingCollectionIds);
		const samplingIds = samplingCollections.map(({ members }) => members).flat();
		return getters.getSamplingsByIds(samplingIds);
	},
	getSamplingsBySamplingCollectionId: (_, getters) => (samplingCollectionId: string) =>
		getters.getSamplingsBySamplingCollectionIds([samplingCollectionId]),
	getSamplesBySamplingCollectionIds: (_, getters) => (samplingCollectionIds: string[]) => {
		const samplings: Sampling[] = getters.getSamplingsBySamplingCollectionIds(samplingCollectionIds);
		const sampleIds = samplings.map(({ results }) => results).flat();
		return getters.getSamplesByIds(sampleIds);
	},
	getSamplesBySamplingCollectionId: (_, getters) => (samplingCollectionId: string) =>
		getters.getSamplesBySamplingCollectionIds([samplingCollectionId]),
	getLocationsBySamplingCollectionId: (_, getters) => (samplingCollectionId: string) => {
		if (!getters.getIsLoaded) {
			return [];
		}
		const samplingCollection = getters.getSamplingCollectionById(samplingCollectionId);
		const locationCollectionId = samplingCollection.object;
		const locationCollection = getters.getLocationCollectionById(locationCollectionId);
		const locationIds = locationCollection.features;
		return getters.getLocationsByIds(locationIds);
	},
	getLocationsBySamplingCollectionIds: (_, getters) => (samplingCollectionIds: string[]) =>
		samplingCollectionIds.flatMap(getters.getLocationsBySamplingCollectionId),
	getFieldStatusById: (_, getters) => (fieldId: string): FieldStatus | null => {
		const field = getters.getFieldById(fieldId);
		if (!field) {
			return null;
		}
		const stratifications = getters.getStratificationsByFieldId(fieldId);
		const samplingCollections = getters.getSamplingCollectionsByFieldId(fieldId);
		const isStratified = stratifications.length > 0;
		const isSampled = samplingCollections.length > 0;
		const state = isSampled ? 'sampled' : isStratified ? 'stratified' : 'posted';
		return {
			state,
			timestamp:
				state === 'posted'
					? field.meta.submittedAt
					: state === 'stratified'
					? stratifications[0].meta.submittedAt
					: samplingCollections[0].meta.submittedAt,
		};
	},
};

const createResourcesModule = (): Module<ResourcesState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useAllResources = (store: Store<RootState>) => {
	const isLoading = computed(() => store.getters[`resources/getIsLoading`]);
	const isLoaded = computed(() => store.getters[`resources/getIsLoaded`]);
	const hasError = computed(() => store.getters[`resources/getHasError`]);
	const fields = computed(() => store.getters[`resources/getFields`]);
	const areas = computed(() => store.getters[`resources/getAreas`]);
	const stratifications = computed(() => store.getters[`resources/getStratifications`]);
	const locationCollections = computed(() => store.getters[`resources/getLocationCollections`]);
	const locations = computed(() => store.getters[`resources/getLocations`]);
	const samples = computed(() => store.getters[`resources/getSamples`]);
	const samplings = computed(() => store.getters[`resources/getSamplings`]);
	const samplingCollections = computed(() => store.getters[`resources/getSamplingCollections`]);

	if (!isLoading.value && !isLoaded.value) {
		store.dispatch(`resources/getAllResources`);
	}

	return {
		isLoading,
		isLoaded,
		hasError,
		fields,
		areas,
		stratifications,
		locationCollections,
		locations,
		samples,
		samplings,
		samplingCollections,
	};
};

export { actions, mutations, getters, useAllResources };

export default createResourcesModule;
