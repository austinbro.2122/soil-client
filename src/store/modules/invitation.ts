/* eslint-disable @typescript-eslint/no-explicit-any */

const createInitialState = () => ({
	invitation: null,
});

const initialState = createInitialState();

const getters = {
	hasInvitation: (state: any) => state.invitation !== null,
	code: (state: any) => state.invitation,
};

const actions = {
	reset({ commit }: any) {
		commit('RESET');
	},
	set({ commit }: any, invitation: any) {
		commit('set', invitation);
	},
	clear({ commit }: any) {
		commit('clear');
	},
};

const mutations = {
	RESET(state: any) {
		Object.assign(state, createInitialState());
	},
	set(state: any, invitation: any) {
		state.invitation = invitation;
	},
	clear(state: any) {
		state.invitation = null;
	},
};

export default {
	namespaced: true,
	state: initialState,
	getters,
	actions,
	mutations,
};
