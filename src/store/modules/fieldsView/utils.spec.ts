import {
	fieldToFieldRow,
	matchQuery,
	matchGroupFilter,
	matchRecentActivityFilter,
	matchStatusFilter,
	sortBys,
	FieldRow,
	addGroupFilter,
	removeGroupFilter,
} from './utils';
import { TableColumns } from './configuration';
import createTestStore from '../../../../tests/createTestStore';
import { createMockField, createMockArea, createMockResourceMeta } from '../../../../tests/mockGenerators';
import { createMockResourcesState } from '../test-utils';
import { RawGroup } from '@/store/types';
import { getGroupArray, getGroupTree } from '../group/utils';

const createMockRawGroup = (slug = '/soilstack/'): RawGroup[] => [
	{ id: 'group1', name: 'A', path: `${slug}a/` },
	{ id: 'group2', name: 'A-A', path: `${slug}a/a-a/` },
	{ id: 'group3', name: 'A-A-A', path: `${slug}a/a-a/a-a-a/` },
	{ id: 'group4', name: 'A-A-B', path: `${slug}a/a-a/a-a-b/` },
	{ id: 'group5', name: 'A-B', path: `${slug}a/a-b/` },
	{ id: 'group6', name: 'A-B-A', path: `${slug}a/a-b/a-b-a/` },
	{ id: 'group7', name: 'B', path: `${slug}b/` },
	{ id: 'group8', name: 'B-A', path: `${slug}b/b-a/` },
	{ id: 'group9', name: 'C', path: `${slug}c/` },
];

const createMockFieldRow = ({
	id = 'id',
	groupId = 'groupId',
	name = 'name',
	producerName = 'producerName',
	groupName = 'groupName',
	status = {
		state: 'posted',
		timestamp: new Date(0).toISOString(),
	},
	markerConfig = {
		id: 'id',
		lngLat: [0, 0],
		html: '',
	},
}: Partial<FieldRow> = {}): FieldRow => ({
	id,
	groupId,
	name,
	producerName,
	groupName,
	status,
	markerConfig,
});

describe('fieldsView utils', () => {
	describe('fieldToFieldRow', () => {
		const store = createTestStore();
		const mockField = createMockField({
			areas: ['area1'],
			meta: createMockResourceMeta({ groupId: 'group1' }),
		});
		const mockArea = createMockArea({ id: 'area1' });
		store.replaceState({
			...store.state,
			resources: createMockResourcesState({
				fields: [mockField],
				areas: [mockArea],
			}),
			groups: {
				groups: [{ id: 'group1', name: 'groupName', path: '/group-path/' }],
			},
		});

		const fieldRow = fieldToFieldRow(mockField, store.getters);

		it('maps id', () => {
			expect(fieldRow.id).toBe(mockField.id);
		});

		it('maps groupId', () => {
			expect(fieldRow.groupId).toBe(mockField.meta.groupId);
		});

		it('maps name', () => {
			expect(fieldRow.name).toBe(mockField.name);
		});

		it('maps producerName', () => {
			expect(fieldRow.producerName).toBe(mockField.producerName);
		});

		it('looks up groupName', () => {
			expect(fieldRow.groupName).toBe('groupName');
		});

		it('looks up status', () => {
			expect(fieldRow.status.state).toBe('posted');
			expect(fieldRow.status.timestamp).toBe(mockField.meta.submittedAt);
		});
	});

	describe('matchQuery', () => {
		const mockFieldRow = createMockFieldRow();

		it('returns true when the query is null', () => {
			expect(matchQuery(null)(mockFieldRow)).toBe(true);
		});

		it('returns true when the query is empty', () => {
			expect(matchQuery('')(mockFieldRow)).toBe(true);
		});

		it('matches on name property', () => {
			expect(matchQuery('name')(mockFieldRow)).toBe(true);
		});

		it('matches on producerName property', () => {
			expect(matchQuery('producerName')(mockFieldRow)).toBe(true);
		});

		it('matches on groupName property', () => {
			expect(matchQuery('groupName')(mockFieldRow)).toBe(true);
		});

		it('matches on status.state property', () => {
			expect(matchQuery('posted')(mockFieldRow)).toBe(true);
		});

		it('ignores case', () => {
			expect(matchQuery('PRODuCeR')(mockFieldRow)).toBe(true);
			expect(matchQuery('producername')(mockFieldRow)).toBe(true);
		});
	});

	describe('matchGroupFilter', () => {
		it('returns true if groupFilter is empty', () => {
			const mockFieldRow = createMockFieldRow();
			expect(matchGroupFilter([])(mockFieldRow)).toBe(true);
		});

		it('returns true if groupFilter contains groupId', () => {
			const mockFieldRow = createMockFieldRow({ groupId: 'group1' });
			expect(matchGroupFilter(['group1'])(mockFieldRow)).toBe(true);
		});

		it('returns false if groupFilter does not match groupId', () => {
			const mockFieldRow = createMockFieldRow({ groupId: 'group1' });

			expect(matchGroupFilter(['group2'])(mockFieldRow)).toBe(false);
		});
	});

	describe('matchRecentActivityFilter', () => {
		describe('timezone handling', () => {
			const originalGetTimezoneOffset = Date.prototype.getTimezoneOffset;
			beforeAll(() => {
				Date.prototype.getTimezoneOffset = () => 300; // UTC-5
			});

			afterAll(() => {
				Date.prototype.getTimezoneOffset = originalGetTimezoneOffset;
			});

			it("filters end date according to user's timezone", () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: '2022-01-02T05:00:00.000Z',
					},
				});

				const actual = matchRecentActivityFilter({
					start: null,
					end: new Date('2022-01-01'),
				})(mockFieldRow);

				expect(actual).toBe(true);
			});

			it("filters start date according to user's timezone", () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: '2022-01-02T05:00:00.000Z',
					},
				});

				const actual = matchRecentActivityFilter({
					start: new Date('2022-01-02'),
					end: null,
				})(mockFieldRow);

				expect(actual).toBe(true);
			});
		});

		describe('if start and end are both null', () => {
			it('returns true', () => {
				const mockFieldRow = createMockFieldRow({});

				const actual = matchRecentActivityFilter({ start: null, end: null })(mockFieldRow);

				expect(actual).toBe(true);
			});
		});

		describe('if start and end are both present', () => {
			it('returns true for fields whose most recent activity is between or on the start and end days', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2020-01-01').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: new Date('2020-01-01'),
					end: new Date('2020-01-01'),
				})(mockFieldRow);

				expect(actual).toBe(true);
			});

			it('returns false for fields whose most recent activity is before the start day', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2022-01-01').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: new Date('2022-01-02'),
					end: new Date('2022-01-03'),
				})(mockFieldRow);

				expect(actual).toBe(false);
			});

			it('returns false for fields whose most recent activity is after the end day', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2022-01-03T00:00:00.001Z').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: new Date('2022-01-01'),
					end: new Date('2022-01-02'),
				})(mockFieldRow);

				expect(actual).toBe(false);
			});
		});

		describe('if start is present and end is null', () => {
			it('returns true for fields whose most recent activity is on or after the day specified as start', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2022-01-01').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: new Date('2022-01-01'),
					end: null,
				})(mockFieldRow);

				expect(actual).toBe(true);
			});

			it('returns false for fields whose most recent activity is before the start day', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2022-01-01').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: new Date('2022-01-02'),
					end: null,
				})(mockFieldRow);

				expect(actual).toBe(false);
			});
		});

		describe('if start is null and end is present', () => {
			it('returns true for fields whose most recent activity is before or on the day specified as end', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2022-01-01').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: null,
					end: new Date('2022-01-01'),
				})(mockFieldRow);

				expect(actual).toBe(true);
			});

			it('returns false for fields whose most recent activity is after the day specified by end', () => {
				const mockFieldRow = createMockFieldRow({
					status: {
						state: 'posted',
						timestamp: new Date('2022-01-02T00:00:00.001Z').toISOString(),
					},
				});

				const actual = matchRecentActivityFilter({
					start: null,
					end: new Date('2022-01-01'),
				})(mockFieldRow);

				expect(actual).toBe(false);
			});
		});
	});

	describe('matchStatusFilter', () => {
		it('returns true when statusFilter is null', () => {
			const mockFieldRow = createMockFieldRow();

			const actual = matchStatusFilter(null)(mockFieldRow);

			expect(actual).toBe(true);
		});

		it('returns true when field status matches statusFilter', () => {
			const mockFieldRow = createMockFieldRow({
				status: {
					state: 'sampled',
					timestamp: new Date('2020-01-01').toISOString(),
				},
			});

			const actual = matchStatusFilter('sampled')(mockFieldRow);

			expect(actual).toBe(true);
		});

		it('returns false when field status does not match statusFilter', () => {
			const mockFieldRow = createMockFieldRow({
				status: {
					state: 'sampled',
					timestamp: new Date('2020-01-01').toISOString(),
				},
			});

			const actual = matchStatusFilter('posted')(mockFieldRow);

			expect(actual).toBe(false);
		});
	});

	describe('sortBys', () => {
		describe('name', () => {
			it('sorts by name ascending', () => {
				const mockFieldRow1 = createMockFieldRow({ name: 'a' });
				const mockFieldRow2 = createMockFieldRow({ name: 'b' });

				const sorted = [mockFieldRow1, mockFieldRow2].sort(sortBys[TableColumns.NAME]);

				expect(sorted).toEqual([mockFieldRow1, mockFieldRow2]);
			});
		});

		describe('producerName', () => {
			it('sorts by producerName ascending', () => {
				const mockFieldRow1 = createMockFieldRow({ producerName: 'a' });
				const mockFieldRow2 = createMockFieldRow({ producerName: 'b' });

				const sorted = [mockFieldRow1, mockFieldRow2].sort(sortBys[TableColumns.PRODUCER_NAME]);

				expect(sorted).toEqual([mockFieldRow1, mockFieldRow2]);
			});
		});

		describe('groupName', () => {
			it('sorts by groupName ascending', () => {
				const mockFieldRow1 = createMockFieldRow({ groupName: 'a' });
				const mockFieldRow2 = createMockFieldRow({ groupName: 'b' });

				const sorted = [mockFieldRow1, mockFieldRow2].sort(sortBys[TableColumns.GROUP_NAME]);

				expect(sorted).toEqual([mockFieldRow1, mockFieldRow2]);
			});
		});

		describe('status', () => {
			it('sorts by status.state ascending in the order that the states are progressed through in the application', () => {
				const mockFieldRow1 = createMockFieldRow({ status: { state: 'posted', timestamp: new Date(0).toISOString() } });
				const mockFieldRow2 = createMockFieldRow({
					status: { state: 'stratified', timestamp: new Date(0).toISOString() },
				});
				const mockFieldRow3 = createMockFieldRow({
					status: { state: 'sampled', timestamp: new Date(0).toISOString() },
				});

				const sorted = [mockFieldRow1, mockFieldRow2, mockFieldRow3].sort(sortBys[TableColumns.STATUS]);

				expect(sorted).toEqual([mockFieldRow1, mockFieldRow2, mockFieldRow3]);
			});

			it('secondarily, sorts by status.timestamp ascending when status.state are the same', () => {
				const mockFieldRow1 = createMockFieldRow({ status: { state: 'posted', timestamp: new Date(0).toISOString() } });
				const mockFieldRow2 = createMockFieldRow({ status: { state: 'posted', timestamp: new Date(1).toISOString() } });
				const mockFieldRow3 = createMockFieldRow({
					status: { state: 'stratified', timestamp: new Date(0).toISOString() },
				});
				const mockFieldRow4 = createMockFieldRow({
					status: { state: 'stratified', timestamp: new Date(1).toISOString() },
				});

				const sorted = [mockFieldRow1, mockFieldRow2, mockFieldRow3, mockFieldRow4].sort(sortBys[TableColumns.STATUS]);

				expect(sorted).toEqual([mockFieldRow1, mockFieldRow2, mockFieldRow3, mockFieldRow4]);
			});
		});
	});

	describe('addGroupFilter', () => {
		const source = createMockRawGroup();
		const groupTree = getGroupTree(source);
		const groups = getGroupArray(groupTree);

		it('group should be checked', () => {
			const groupC = groups[8];
			const groupFilter = addGroupFilter(groupC, [], groups);
			expect(groupFilter).toContain(groupC.id);
		});

		it('all children should be checked', () => {
			const groupA = groups[0];
			const groupFilter = addGroupFilter(groupA, [], groups);
			expect(groupFilter).toContain('group2');
			expect(groupFilter).toContain('group3');
			expect(groupFilter).toContain('group4');
		});

		it('parent should not be checked if all children are checked', () => {
			const groupABA = groups[5];
			const groupFilter = addGroupFilter(groupABA, [], groups);
			expect(groupFilter).not.toContain('group5');
		});
	});

	describe('removeGroupFilter', () => {
		const source = createMockRawGroup();
		const groupTree = getGroupTree(source);
		const groups = getGroupArray(groupTree);

		it('group should be unchecked', () => {
			const groupC = groups[8];
			const groupFilter = removeGroupFilter(groupC, ['group9']);
			expect(groupFilter).not.toContain(groupC.id);
		});

		it('children group should not be unchecked if we uncheck parent', () => {
			const groupAB = groups[4];
			const groupFilter = removeGroupFilter(groupAB, ['group5', 'group6']);
			expect(groupFilter).toContain('group6');
		});

		it('parent should not be unchecked if we uncheck children', () => {
			const groupAAB = groups[3];
			const groupFilter = removeGroupFilter(groupAAB, ['group2', 'group3', 'group4']);
			expect(groupFilter).toContain('group2');
		});
	});
});
