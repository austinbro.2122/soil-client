import { ActionTree, MutationTree, Module, GetterTree, Store } from 'vuex';
import { RootState, Field, StatusOptions, Group } from '@/store/types';
import {
	fieldToFieldRow,
	matchQuery,
	matchGroupFilter,
	matchRecentActivityFilter,
	matchStatusFilter,
	sortBys,
	addGroupFilter,
	removeGroupFilter,
} from './utils';
import Vue from 'vue';
import { computed } from '@vue/composition-api';

type SortByOptions = 'name' | 'producerName' | 'groupName' | 'status' | null;
export type SortDirOptions = 'ASC' | 'DESC' | null;

export interface FieldsViewState {
	query: string | null;
	sortBy: SortByOptions;
	sortDir: SortDirOptions;
	groupFilter: string[];
	recentActivityFilter: {
		start: Date | null;
		end: Date | null;
	};
	statusFilter: StatusOptions;
}

function createInitialState(): FieldsViewState {
	return {
		query: null,
		sortBy: null,
		sortDir: null,
		groupFilter: [],
		recentActivityFilter: {
			start: null,
			end: null,
		},
		statusFilter: null,
	};
}

const actions: ActionTree<FieldsViewState, RootState> = {
	setQuery({ commit }, query: string): void {
		commit('setQuery', query);
	},
	setSortBy({ commit }, sortBy: SortByOptions): void {
		commit('setSortBy', sortBy);
	},
	setSortDir({ commit }, sortDir: SortDirOptions): void {
		commit('setSortDir', sortDir);
	},
	changeSort({ commit }, { sortBy, sortDir }: { sortBy: SortByOptions; sortDir: SortDirOptions }): void {
		commit('setSortBy', sortBy);
		commit('setSortDir', sortDir);
	},
	setGroupFilter({ commit }, groupFilter: string[]): void {
		commit('setGroupFilter', groupFilter);
	},
	clearGroupFilter({ commit }): void {
		commit('clearGroupFilter');
	},
	setRecentActivityFilter({ commit }, { start, end }: { start: Date | null; end: Date | null }): void {
		commit('setRecentActivityFilter', { start, end });
	},
	setStatusFilter({ commit }, statusFilter: StatusOptions): void {
		commit('setStatusFilter', statusFilter);
	},
	clearAllFilters({ commit }): void {
		commit('clearAllFilters');
	},
};

const mutations: MutationTree<FieldsViewState> = {
	setQuery(state, query: string): void {
		state.query = query;
	},
	setSortBy(state, sortBy: SortByOptions): void {
		state.sortBy = sortBy;
	},
	setSortDir(state, sortDir: SortDirOptions): void {
		state.sortDir = sortDir;
	},
	setGroupFilter(state, groupFilter: string[]): void {
		Vue.set(state, 'groupFilter', groupFilter);
	},
	clearGroupFilter(state): void {
		Vue.set(state, 'groupFilter', []);
	},
	setRecentActivityFilter(state, { start, end }: { start: Date | null; end: Date | null }): void {
		Vue.set(state, 'recentActivityFilter', { start, end });
	},
	setStatusFilter(state, statusFilter: StatusOptions): void {
		state.statusFilter = statusFilter;
	},
	clearAllFilters(state): void {
		Object.assign(state, createInitialState());
	},
};

const getters: GetterTree<FieldsViewState, RootState> = {
	getQuery: (state) => state.query,
	getSortBy: (state) => state.sortBy,
	getSortDir: (state) => state.sortDir,
	getGroupFilter: (state) => state.groupFilter,
	getRecentActivityFilter: (state) => state.recentActivityFilter,
	getStatusFilter: (state) => state.statusFilter,
	getFieldRows: (state, getters, rootState, rootGetters) => {
		const fields: Field[] = rootGetters['resources/getFields'];
		const query = getters.getQuery;
		const sortBy = getters.getSortBy;
		const sortDir = getters.getSortDir;
		const groupFilter = getters.getGroupFilter;
		const recentActivityFilter = getters.getRecentActivityFilter;
		const statusFilter = getters.getStatusFilter;

		const filteredFieldRows = fields
			.map((field) => fieldToFieldRow(field, rootGetters))
			.filter(matchGroupFilter(groupFilter))
			.filter(matchStatusFilter(statusFilter))
			.filter(matchQuery(query))
			.filter(matchRecentActivityFilter(recentActivityFilter));

		if (sortBy && sortDir) {
			const sortedFieldRows = filteredFieldRows.sort(sortBys[sortBy]);
			return sortDir === 'ASC' ? sortedFieldRows : [...sortedFieldRows].reverse();
		} else {
			return filteredFieldRows;
		}
	},
	getFieldRowsLength: (state, getters) => getters.getFieldRows.length,
};

const createFieldsViewModule = (): Module<FieldsViewState, RootState> => ({
	namespaced: true,
	state: createInitialState(),
	actions,
	mutations,
	getters,
});

const useGroupFilter = (store: Store<RootState>) => {
	const groupFilter = computed<string[]>(() => store.getters['fieldsView/getGroupFilter']);
	const groups = computed<Group[]>(() => store.getters['groups/getGroups']);

	const setGroupFilter = (id: string) => {
		const group = store.getters['groups/getById'](id);
		if (group) {
			const isAdding = !groupFilter.value.includes(id);
			const newGroupFilter = isAdding
				? addGroupFilter(group, groupFilter.value, groups.value)
				: removeGroupFilter(group, groupFilter.value);
			store.dispatch('fieldsView/setGroupFilter', newGroupFilter);
		}
	};

	return { groupFilter, setGroupFilter };
};

export { actions, mutations, getters, useGroupFilter };

export default createFieldsViewModule;
