import { FieldRow } from './utils';
import { openPopup } from '@/components/map/MglMap';

export const TableColumns = {
	NAME: 'name',
	PRODUCER_NAME: 'producerName',
	GROUP_NAME: 'groupName',
	STATUS: 'status',
	OFFLINE: 'offline',
};

export const tableHeaders = {
	[TableColumns.NAME]: {
		text: 'Field',
		sortable: true,
		key: TableColumns.NAME,
	},
	[TableColumns.PRODUCER_NAME]: {
		text: 'Producer',
		sortable: true,
		key: TableColumns.PRODUCER_NAME,
	},
	[TableColumns.GROUP_NAME]: {
		text: 'Group',
		sortable: true,
		key: TableColumns.GROUP_NAME,
	},
	[TableColumns.STATUS]: {
		text: 'Status',
		sortable: true,
		key: TableColumns.STATUS,
	},
	[TableColumns.OFFLINE]: {
		text: 'Offline',
		key: TableColumns.OFFLINE,
	},
};

export const tableCellComponents = {
	[TableColumns.NAME]: {
		component: 'text-cell',
		getProps: (fieldRow: FieldRow) => ({ text: fieldRow.name, linkTo: `/field/${fieldRow.id}` }),
	},
	[TableColumns.PRODUCER_NAME]: {
		component: 'text-cell',
		getProps: (fieldRow: FieldRow) => ({ text: fieldRow.producerName, handleClick: () => openPopup(fieldRow.id) }),
	},
	[TableColumns.GROUP_NAME]: {
		component: 'text-cell',
		getProps: (fieldRow: FieldRow) => ({ text: fieldRow.groupName, handleClick: () => openPopup(fieldRow.id) }),
	},
	[TableColumns.STATUS]: {
		component: 'field-status-cell',
		getProps: (fieldRow: FieldRow) => ({ ...fieldRow.status, handleClick: () => openPopup(fieldRow.id) }),
	},
	[TableColumns.OFFLINE]: {
		component: 'offline-map-button',
		getProps: (fieldRow: FieldRow) => ({ fieldId: fieldRow.id, showCaption: false }),
	},
};
