import { Area, Field, FieldStatus, Group, StatusOptions } from '@/store/types';
import { centroid } from '@turf/turf';
import { TableColumns } from './configuration';
import sanitize from 'sanitize-html';
export interface MarkerConfig {
	id: string;
	lngLat: [number, number] | null;
	html: string;
}
export interface FieldRow {
	id: string;
	groupId: string;
	name: string;
	producerName: string;
	groupName: string;
	status: FieldStatus;
	markerConfig: MarkerConfig;
}

export function createMarkerConfig(field: Field, area: Area, status: FieldStatus, groupName: string): MarkerConfig {
	let coordinates;
	// guards against errors caused by nonsensical areas with coordinates like [0, 0]
	try {
		coordinates = centroid(area)?.geometry?.coordinates;
	} catch (err) {
		console.warn('Invalid Area Coordinates:', err);
	}

	const [fieldName, producerName, gName] = [field.name, field.producerName, groupName].map((item) =>
		sanitize(item, { allowedTags: [], allowedAttributes: {} })
	);

	const statusDate = new Date(status.timestamp).toLocaleString([], {
		year: 'numeric',
		month: 'numeric',
		day: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
	});

	return {
		id: field.id,
		lngLat: coordinates ? [coordinates[0], coordinates[1]] : null,
		html: `<div class="MapPopup-fieldInfo">
			<div class="text-overline">
				${producerName ? `${producerName}<br/>` : ''}
				(${gName})
			</div>
			<a href="/field/${field.id}">
				<div class="v-list-item__title text-h5 mb-1">${fieldName}</div>
			</a>
			<div class="v-list-item__subtitle">
				${status.state}<br>
				<div class="text-caption">${statusDate}</div>
			</div>
		</div>`,
	};
}

export function fieldToFieldRow(field: Field, rootGetters: any): FieldRow {
	const groupName = rootGetters['groups/getGroupNameById'](field.meta.groupId) || 'no group';
	const status = rootGetters['resources/getFieldStatusById'](field.id);
	const area = rootGetters['resources/getAreaById'](field.areas[0]);
	return {
		id: field.id,
		groupId: field.meta.groupId,
		name: field.name || '',
		producerName: field.producerName || '',
		groupName,
		status,
		markerConfig: createMarkerConfig(field, area, status, groupName),
	};
}

export const matchQuery = (query: string | null) => (fieldRow: FieldRow) => {
	if (query === null) {
		return true;
	}
	const { name, producerName, groupName, status } = fieldRow;
	const lowerCaseQuery = query.toLowerCase();
	return (
		name.toLowerCase().includes(lowerCaseQuery) ||
		producerName.toLowerCase().includes(lowerCaseQuery) ||
		groupName.toLowerCase().includes(lowerCaseQuery) ||
		status.state.toLowerCase().includes(lowerCaseQuery)
	);
};

export const matchGroupFilter = (groupFilter: string[]) => (fieldRow: FieldRow) =>
	groupFilter.length === 0 ? true : groupFilter.includes(fieldRow.groupId);

// start and end are expected to be dates, not datetimes. They should be initialized by passing a string of the format 'YYYY-MM-DD' into new Date().
export const matchRecentActivityFilter = (recentActivityFilter: { start: Date | null; end: Date | null }) => (
	fieldRow: FieldRow
) => {
	const {
		status: { timestamp },
	} = fieldRow;
	const { start, end } = recentActivityFilter;

	if (start === null && end === null) {
		return true;
	}

	const startOffset = start?.getTimezoneOffset() || 0;
	const startTime = start?.getTime() || 0;
	const startInUTC = new Date(startTime + startOffset * 60 * 1000);

	const endOffset = end?.getTimezoneOffset() || 0;
	const endCopy = end ? new Date(end) : new Date();
	const endTime = endCopy.setDate(endCopy.getDate() + 1) || 0;
	const endInUTC = new Date(endTime + endOffset * 60 * 1000);

	const lastActivity = new Date(timestamp);

	if (start !== null && end !== null) {
		return lastActivity >= startInUTC && lastActivity <= endInUTC;
	}
	if (start !== null) {
		return lastActivity >= startInUTC;
	}
	if (end !== null) {
		return lastActivity <= endInUTC;
	}
};

export const matchStatusFilter = (statusFilter: StatusOptions) => (fieldRow: FieldRow) =>
	statusFilter === null ? true : fieldRow.status.state === statusFilter;

/**
 * Check the group
 */
export const addGroupFilter = (group: Group, groupFilter: string[], groups: Group[]): string[] => {
	const newGroupFilter = [
		...groupFilter,
		...groups.filter((item) => item.path.startsWith(group.path)).map((item) => item.id),
	];

	return newGroupFilter;
};

/**
 * Uncheck the group
 */
export const removeGroupFilter = (group: Group, groupFilter: string[]): string[] =>
	groupFilter.filter((item) => item !== group.id);

const statusStateSortOrder = {
	posted: 0,
	stratified: 1,
	sampled: 2,
};

export const sortBys = {
	[TableColumns.NAME]: (a: FieldRow, b: FieldRow) => a.name.localeCompare(b.name),
	[TableColumns.PRODUCER_NAME]: (a: FieldRow, b: FieldRow) => a.producerName.localeCompare(b.producerName),
	[TableColumns.GROUP_NAME]: (a: FieldRow, b: FieldRow) => a.groupName.localeCompare(b.groupName),
	[TableColumns.STATUS]: (a: FieldRow, b: FieldRow) =>
		statusStateSortOrder[a.status.state] - statusStateSortOrder[b.status.state] ||
		a.status.timestamp.localeCompare(b.status.timestamp),
};
