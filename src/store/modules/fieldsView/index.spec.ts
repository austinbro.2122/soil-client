/* eslint-disable @typescript-eslint/no-explicit-any */
import createFieldsViewModule, { FieldsViewState, actions, useGroupFilter } from './index';
import { createMockResourcesState } from '../test-utils';
import createTestStore from '../../../../tests/createTestStore';
import {
	createMockField,
	createMockArea,
	createMockResourceMeta,
	createMockStratification,
	createMockSamplingCollection,
} from '../../../../tests/mockGenerators';

const createMockFieldsViewState = ({
	query = null,
	sortBy = null,
	sortDir = null,
	groupFilter = [],
	recentActivityFilter = {
		start: null,
		end: null,
	},
	statusFilter = null,
}: Partial<FieldsViewState> = {}): FieldsViewState => ({
	query,
	sortBy,
	sortDir,
	groupFilter,
	recentActivityFilter,
	statusFilter,
});

describe('fieldsView module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('setQuery', () => {
			it('commits "setQuery" mutation', async () => {
				await (actions.setQuery as any)({ commit: commitSpy }, 'search for this');

				expect(commitSpy).toHaveBeenCalledWith('setQuery', 'search for this');
			});
		});

		describe('setSortBy', () => {
			it('commits "setSortBy" mutation', async () => {
				await (actions.setSortBy as any)({ commit: commitSpy }, 'producerName');

				expect(commitSpy).toHaveBeenCalledWith('setSortBy', 'producerName');
			});
		});

		describe('setSortDir', () => {
			it('commits "setSortDir" mutation', async () => {
				await (actions.setSortDir as any)({ commit: commitSpy }, 'ASC');

				expect(commitSpy).toHaveBeenCalledWith('setSortDir', 'ASC');
			});
		});

		describe('changeSort', () => {
			it('commits "setSortBy" and "setSortDir" mutations', async () => {
				await (actions.changeSort as any)({ commit: commitSpy }, { sortBy: 'producerName', sortDir: 'ASC' });

				expect(commitSpy).toHaveBeenCalledWith('setSortBy', 'producerName');
				expect(commitSpy).toHaveBeenCalledWith('setSortDir', 'ASC');
			});
		});

		describe('setGroupFilter', () => {
			it('commits "setGroupFilter" mutation', async () => {
				await (actions.setGroupFilter as any)({ commit: commitSpy }, ['123', '456']);

				expect(commitSpy).toHaveBeenCalledWith('setGroupFilter', ['123', '456']);
			});
		});

		describe('clearGroupFilter', () => {
			it('commits "clearGroupFilter" mutation', async () => {
				await (actions.clearGroupFilter as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('clearGroupFilter');
			});
		});

		describe('setRecentActivityFilter', () => {
			it('commits "setRecentActivityFilter" mutation', async () => {
				const startDate = new Date('2022-01-01');
				const endDate = new Date('2022-01-02');
				await (actions.setRecentActivityFilter as any)({ commit: commitSpy }, { start: startDate, end: endDate });

				expect(commitSpy).toHaveBeenCalledWith('setRecentActivityFilter', { start: startDate, end: endDate });
			});
		});

		describe('setStatusFilter', () => {
			it('commits "setStatusFilter" mutation', async () => {
				await (actions.setStatusFilter as any)({ commit: commitSpy }, 'posted');

				expect(commitSpy).toHaveBeenCalledWith('setStatusFilter', 'posted');
			});
		});

		describe('clearAllFilters', () => {
			it('commits "clearAllFilters" mutation', async () => {
				await (actions.clearAllFilters as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('clearAllFilters');
			});
		});
	});

	describe('mutations', () => {
		let fieldsViewModule: any;

		beforeEach(() => {
			fieldsViewModule = createFieldsViewModule();
		});

		describe('setQuery', () => {
			it('sets query', () => {
				fieldsViewModule.state = createMockFieldsViewState({
					query: '',
				});

				fieldsViewModule.mutations.setQuery(fieldsViewModule.state, 'search for this');

				expect(fieldsViewModule.state.query).toBe('search for this');
			});
		});

		describe('setSortBy', () => {
			it('sets sortBy', () => {
				fieldsViewModule.state = createMockFieldsViewState({
					sortBy: null,
				});

				fieldsViewModule.mutations.setSortBy(fieldsViewModule.state, 'producerName');

				expect(fieldsViewModule.state.sortBy).toBe('producerName');
			});
		});

		describe('setSortDir', () => {
			it('sets sortDir', () => {
				fieldsViewModule.state = createMockFieldsViewState({
					sortDir: null,
				});

				fieldsViewModule.mutations.setSortDir(fieldsViewModule.state, 'ASC');

				expect(fieldsViewModule.state.sortDir).toBe('ASC');
			});
		});

		describe('setGroupFilter', () => {
			it('set IDs array to groupFilter', () => {
				fieldsViewModule.state = createMockFieldsViewState({ groupFilter: [] });

				fieldsViewModule.mutations.setGroupFilter(fieldsViewModule.state, ['123', '456']);

				expect(fieldsViewModule.state.groupFilter).toContain('123');
			});
		});

		describe('clearGroupFilter', () => {
			it('resets groupFilter', () => {
				fieldsViewModule.state = createMockFieldsViewState({ groupFilter: ['123', '456'] });

				fieldsViewModule.mutations.clearGroupFilter(fieldsViewModule.state);

				expect(fieldsViewModule.state.groupFilter).toEqual([]);
			});
		});

		describe('setRecentActivityFilter', () => {
			it('sets recentActivityFilter', () => {
				const startDate = new Date('2022-01-01');
				const endDate = new Date('2022-01-02');
				fieldsViewModule.state = createMockFieldsViewState({});

				fieldsViewModule.mutations.setRecentActivityFilter(fieldsViewModule.state, { start: startDate, end: endDate });

				expect(fieldsViewModule.state.recentActivityFilter).toEqual({ start: startDate, end: endDate });
			});
		});

		describe('setStatusFilter', () => {
			it('sets statusFilter', () => {
				fieldsViewModule.state = createMockFieldsViewState({});

				fieldsViewModule.mutations.setStatusFilter(fieldsViewModule.state, 'posted');

				expect(fieldsViewModule.state.statusFilter).toBe('posted');
			});
		});

		describe('clearAllFilters', () => {
			it('resets module to initial state', () => {
				fieldsViewModule.state = createMockFieldsViewState({
					query: 'search for this',
					sortBy: 'producerName',
					sortDir: 'ASC',
					groupFilter: ['123', '456'],
					recentActivityFilter: { start: new Date('2022-01-01'), end: new Date('2022-01-02') },
					statusFilter: 'posted',
				});

				fieldsViewModule.mutations.clearAllFilters(fieldsViewModule.state);

				expect(fieldsViewModule.state.query).toBe(null);
				expect(fieldsViewModule.state.sortBy).toBe(null);
				expect(fieldsViewModule.state.sortDir).toBe(null);
				expect(fieldsViewModule.state.groupFilter).toEqual([]);
				expect(fieldsViewModule.state.recentActivityFilter).toEqual({
					start: null,
					end: null,
				});
				expect(fieldsViewModule.state.statusFilter).toBe(null);
			});
		});
	});

	describe('getters', () => {
		describe('getQuery', () => {
			it('returns state.query', () => {
				const fieldsViewModule = createFieldsViewModule();
				fieldsViewModule.state = createMockFieldsViewState({
					query: 'search for this',
				});

				expect((fieldsViewModule.getters?.getQuery as any)(fieldsViewModule.state)).toBe('search for this');
			});
		});

		describe('getSortBy', () => {
			it('returns state.sortBy', () => {
				const fieldsViewModule = createFieldsViewModule();
				fieldsViewModule.state = createMockFieldsViewState({
					sortBy: 'producerName',
				});

				expect((fieldsViewModule.getters?.getSortBy as any)(fieldsViewModule.state)).toBe('producerName');
			});
		});

		describe('getSortDir', () => {
			it('returns state.sortDir', () => {
				const fieldsViewModule = createFieldsViewModule();
				fieldsViewModule.state = createMockFieldsViewState({
					sortDir: 'ASC',
				});

				expect((fieldsViewModule.getters?.getSortDir as any)(fieldsViewModule.state)).toBe('ASC');
			});
		});

		describe('getGroupFilter', () => {
			it('returns state.groupFilter', () => {
				const fieldsViewModule = createFieldsViewModule();
				fieldsViewModule.state = createMockFieldsViewState({
					groupFilter: ['123'],
				});

				const actual = (fieldsViewModule.getters?.getGroupFilter as any)(fieldsViewModule.state);

				expect(actual).toEqual(['123']);
			});
		});

		describe('getRecentActivityFilter', () => {
			it('returns state.recentActivityFilter', () => {
				const fieldsViewModule = createFieldsViewModule();
				fieldsViewModule.state = createMockFieldsViewState({
					recentActivityFilter: { start: new Date('2022-01-01'), end: new Date('2022-01-02') },
				});

				const actual = (fieldsViewModule.getters?.getRecentActivityFilter as any)(fieldsViewModule.state);

				expect(actual).toEqual({ start: new Date('2022-01-01'), end: new Date('2022-01-02') });
			});
		});

		describe('getStatusFilter', () => {
			it('returns state.statusFilter', () => {
				const fieldsViewModule = createFieldsViewModule();
				fieldsViewModule.state = createMockFieldsViewState({
					statusFilter: 'posted',
				});

				const actual = (fieldsViewModule.getters?.getStatusFilter as any)(fieldsViewModule.state);

				expect(actual).toBe('posted');
			});
		});

		describe('getFieldRows', () => {
			it('returns a field row for each field', () => {
				const store = createTestStore();
				const mockField1 = createMockField({ id: 'field1', areas: ['area1'] });
				const mockField2 = createMockField({ id: 'field2', areas: ['area2'] });
				const mockArea1 = createMockArea({ id: 'area1' });
				const mockArea2 = createMockArea({ id: 'area2' });
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField1, mockField2],
						areas: [mockArea1, mockArea2],
					}),
				});

				const actual = store.getters['fieldsView/getFieldRows'];

				expect(actual.length).toBe(2);
			});

			it('filters field rows according to state.query', () => {
				const store = createTestStore();
				const mockField1 = createMockField({ id: 'field1', name: 'Field Uno', areas: ['area1'] });
				const mockField2 = createMockField({ id: 'field2', name: 'Field Dos', areas: ['area2'] });
				const mockArea1 = createMockArea({ id: 'area1' });
				const mockArea2 = createMockArea({ id: 'area2' });
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField1, mockField2],
						areas: [mockArea1, mockArea2],
					}),
					fieldsView: createMockFieldsViewState({
						query: 'Uno',
					}),
					groups: {
						groups: [{ id: '2', name: 'group 2', path: '/group-2/' }],
					},
				});

				const actual = store.getters['fieldsView/getFieldRows'];

				expect(actual.length).toBe(1);
				expect(actual[0]).toHaveProperty('name', 'Field Uno');
			});

			it('filters field rows according to state.groupFilter', () => {
				const store = createTestStore();
				const mockField1 = createMockField({
					id: 'field1',
					areas: ['area1'],
					meta: createMockResourceMeta({ groupId: '1' }),
				});
				const mockField2 = createMockField({
					id: 'field2',
					areas: ['area2'],
					meta: createMockResourceMeta({ groupId: '2' }),
				});
				const mockField3 = createMockField({
					id: 'field3',
					areas: ['area3'],
					meta: createMockResourceMeta({ groupId: '3' }),
				});
				const mockArea1 = createMockArea({ id: 'area1' });
				const mockArea2 = createMockArea({ id: 'area2' });
				const mockArea3 = createMockArea({ id: 'area3' });
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField1, mockField2, mockField3],
						areas: [mockArea1, mockArea2, mockArea3],
					}),
					fieldsView: createMockFieldsViewState({
						groupFilter: ['2', '3'],
					}),
					groups: {
						groups: [
							{ id: '1', name: 'group 1', path: '/group-1/' },
							{ id: '2', name: 'group 2', path: '/group-2/' },
							{ id: '3', name: 'group 3', path: '/group-3/' },
						],
					},
				});

				const actual = store.getters['fieldsView/getFieldRows'];

				expect(actual.length).toBe(2);
				expect(actual).toContainEqual(expect.objectContaining({ id: 'field2' }));
				expect(actual).toContainEqual(expect.objectContaining({ id: 'field3' }));
			});

			it('filters fields according to state.statusFilter', () => {
				const store = createTestStore();
				// field1 has 'posted' status
				const mockField1 = createMockField({
					id: 'field1',
					areas: ['area1'],
				});
				const mockArea1 = createMockArea({ id: 'area1' });
				// field2 has 'stratified' status
				const mockField2 = createMockField({
					id: 'field2',
					areas: ['area2'],
				});
				const mockArea2 = createMockArea({ id: 'area2' });
				const mockStratification1 = createMockStratification({
					id: 'strat1',
					object: 'area2',
					meta: createMockResourceMeta({
						submittedAt: new Date(456).toISOString(),
					}),
				});
				// field3 has 'sampled' status
				const mockField3 = createMockField({
					id: 'field3',
					areas: ['area3'],
				});
				const mockArea3 = createMockArea({ id: 'area3' });
				const mockStratification2 = createMockStratification({
					id: 'strat1',
					object: 'area3',
					meta: createMockResourceMeta({
						submittedAt: new Date(456).toISOString(),
					}),
				});
				const mockSamplingCollection1 = createMockSamplingCollection({
					id: 'sc1',
					featureOfInterest: 'field3',
					meta: createMockResourceMeta({
						submittedAt: new Date(789).toISOString(),
					}),
				});
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField1, mockField2, mockField3],
						areas: [mockArea1, mockArea2, mockArea3],
						stratifications: [mockStratification1, mockStratification2],
						samplingCollections: [mockSamplingCollection1],
					}),
					fieldsView: createMockFieldsViewState({
						statusFilter: 'sampled',
					}),
					groups: {
						groups: [{ id: '1', name: 'group 1', path: '/group-1/' }],
					},
				});

				const actual = store.getters['fieldsView/getFieldRows'];

				expect(actual.length).toBe(1);
				expect(actual[0]).toHaveProperty('id', 'field3');
			});

			it('filters fields according to recentActivityFilter', () => {
				const store = createTestStore();
				const mockField1 = createMockField({
					id: 'field1',
					areas: ['area1'],
					meta: createMockResourceMeta({
						submittedAt: new Date('2022-01-01').toISOString(),
						groupId: '1',
					}),
				});
				const mockField2 = createMockField({
					id: 'field2',
					areas: ['area2'],
					meta: createMockResourceMeta({
						submittedAt: new Date('2022-02-01').toISOString(),
						groupId: '1',
					}),
				});
				const mockField3 = createMockField({
					id: 'field3',
					areas: ['area3'],
					meta: createMockResourceMeta({
						submittedAt: new Date('2022-03-01').toISOString(),
						groupId: '1',
					}),
				});
				const mockArea1 = createMockArea({ id: 'area1' });
				const mockArea2 = createMockArea({ id: 'area2' });
				const mockArea3 = createMockArea({ id: 'area3' });
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField1, mockField2, mockField3],
						areas: [mockArea1, mockArea2, mockArea3],
					}),
					fieldsView: createMockFieldsViewState({
						recentActivityFilter: {
							start: new Date('2022-02-01'),
							end: new Date('2022-03-01'),
						},
					}),
					groups: {
						groups: [{ id: '1', name: 'group 1', path: '/group-1/' }],
					},
				});

				const actual = store.getters['fieldsView/getFieldRows'];

				expect(actual.length).toBe(2);
				expect(actual).toContainEqual(expect.objectContaining({ id: 'field2' }));
				expect(actual).toContainEqual(expect.objectContaining({ id: 'field3' }));
			});

			describe('sorts field rows according to state.sortBy and state.sortDir', () => {
				it('sorts ASC when state.sortDir is ASC', () => {
					const store = createTestStore();
					const mockField1 = createMockField({ id: 'field1', name: 'Field Uno', areas: ['area1'] });
					const mockField2 = createMockField({ id: 'field2', name: 'Field Dos', areas: ['area2'] });
					const mockArea1 = createMockArea({ id: 'area1' });
					const mockArea2 = createMockArea({ id: 'area2' });
					store.replaceState({
						...store.state,
						resources: createMockResourcesState({
							fields: [mockField1, mockField2],
							areas: [mockArea1, mockArea2],
						}),
						fieldsView: createMockFieldsViewState({
							sortBy: 'name',
							sortDir: 'ASC',
						}),
						groups: {
							groups: [{ id: '2', name: 'group 2', path: '/group-2/' }],
						},
					});

					const actual = store.getters['fieldsView/getFieldRows'];

					expect(actual).toEqual([
						expect.objectContaining({ name: 'Field Dos' }),
						expect.objectContaining({ name: 'Field Uno' }),
					]);
				});

				it('sorts DESC when state.sortDir is DESC', () => {
					const store = createTestStore();
					const mockField1 = createMockField({ id: 'field1', name: 'Field Uno', areas: ['area1'] });
					const mockField2 = createMockField({ id: 'field2', name: 'Field Dos', areas: ['area2'] });
					const mockArea1 = createMockArea({ id: 'area1' });
					const mockArea2 = createMockArea({ id: 'area2' });
					store.replaceState({
						...store.state,
						resources: createMockResourcesState({
							fields: [mockField1, mockField2],
							areas: [mockArea1, mockArea2],
						}),
						fieldsView: createMockFieldsViewState({
							sortBy: 'name',
							sortDir: 'DESC',
						}),
						groups: {
							groups: [{ id: '2', name: 'group 2', path: '/group-2/' }],
						},
					});

					const actual = store.getters['fieldsView/getFieldRows'];

					expect(actual).toEqual([
						expect.objectContaining({ name: 'Field Uno' }),
						expect.objectContaining({ name: 'Field Dos' }),
					]);
				});
			});
		});
	});

	describe('useGroupFilter', () => {
		it('returns computedRefs of groupFilter', () => {
			const store = createTestStore();
			const fieldsView = createMockFieldsViewState({ groupFilter: ['group1'] });
			store.replaceState({ ...store.state, fieldsView });
			const actual = useGroupFilter(store);

			expect(actual.groupFilter.value).toEqual(['group1']);
			expect(actual.setGroupFilter).toBeDefined();
		});
	});
});
