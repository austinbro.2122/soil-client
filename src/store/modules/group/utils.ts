import { Group, RawGroup } from '@/store/types';

export const handleize = (str: string) => {
	const handle = str
		.toLowerCase()
		.replace(/[^a-z0-9]+/g, '-')
		.replace(/-$/, '')
		.replace(/^-/, '');
	return handle;
};

export const getGroupTreeMinLevel = (source: RawGroup[]): number =>
	Math.min(...source.map((item) => item.path.split('/').length));

/**
 * Get groups with tree structure from the RawGroup array
 */
export const getGroupTree = (source: RawGroup[], level = 1, parent?: RawGroup): Group[] => {
	const minLevel = getGroupTreeMinLevel(source);
	const childGroups = parent
		? source.filter((item) => item.path === `${parent.path}${handleize(item.name)}/`)
		: source.filter((item) => item.path.split('/').length === minLevel);

	return childGroups.map((rawGroup) => {
		const children = getGroupTree(source, level + 1, rawGroup);
		return { ...rawGroup, level, children };
	});
};

/**
 * Get groups array from the groups tree
 */
export const getGroupArray = (source: Group[]): Group[] => {
	const result: Group[] = [];
	source.forEach((item) => {
		result.push(item, ...getGroupArray(item.children));
	});
	return result;
};
