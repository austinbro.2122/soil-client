/* eslint-disable @typescript-eslint/no-explicit-any */
import createGroupsModule, { actions, getters, useGroups, GroupsState } from '.';
import soilApiService from '@/services/soilApiService';
import createTestStore from '../../../../tests/createTestStore';
import { RawGroup } from '@/store/types';
import { getGroupArray, getGroupTree } from './utils';
import { createMockGroupsState } from '@/store/modules/test-utils';

jest.mock('@/services/soilApiService');

const createMockRawGroup = (slug = '/soilstack/'): RawGroup[] => [
	{ id: 'group1', name: 'A', path: `${slug}a/` },
	{ id: 'group2', name: 'A-A', path: `${slug}a/a-a/` },
	{ id: 'group3', name: 'A-A-A', path: `${slug}a/a-a/a-a-a/` },
	{ id: 'group4', name: 'A-A-B', path: `${slug}a/a-a/a-a-b/` },
	{ id: 'group5', name: 'A-B', path: `${slug}a/a-b/` },
	{ id: 'group6', name: 'A-B-A', path: `${slug}a/a-b/a-b-a/` },
	{ id: 'group7', name: 'B', path: `${slug}b/` },
	{ id: 'group8', name: 'B-A', path: `${slug}b/b-a/` },
	{ id: 'group9', name: 'C', path: `${slug}c/` },
];

describe('groups module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('getGroups', () => {
			it("commits 'requestGetGroups' mutation before making request", async () => {
				await (actions.getGroups as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetGroups');
			});

			it("commits 'requestGetGroupsSuccess' mutation on success", async () => {
				(soilApiService.getGroups as jest.Mock).mockResolvedValue([]);

				await (actions.getGroups as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetGroupsSuccess', { groups: [], groupTree: [] });
			});

			it("commits 'requestGetGroupsError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.getGroups as jest.Mock).mockRejectedValueOnce(error);

				await (actions.getGroups as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetGroupsError', error);
			});
		});
	});

	describe('mutations', () => {
		let groupsModule: any;

		beforeEach(() => {
			groupsModule = createGroupsModule();
		});

		describe('requestGetGroups', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				groupsModule.state = createMockGroupsState({
					isLoading: false,
					isLoaded: true,
					error: 'error',
				});

				groupsModule.mutations.requestGetGroups(groupsModule.state);

				expect(groupsModule.state).toHaveProperty('isLoading', true);
				expect(groupsModule.state).toHaveProperty('isLoaded', false);
				expect(groupsModule.state).toHaveProperty('error', null);
			});
		});

		describe('requestGetGroupsSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null, and puts groupTree and groups into state', () => {
				groupsModule.state = createMockGroupsState({
					isLoading: true,
					isLoaded: false,
					error: 'error',
					groups: [],
					groupTree: [],
				});

				const source = createMockRawGroup();
				const groupTree = getGroupTree(source);
				const groups = getGroupArray(groupTree);

				groupsModule.mutations.requestGetGroupsSuccess(groupsModule.state, { groups, groupTree });

				expect(groupsModule.state).toHaveProperty('isLoading', false);
				expect(groupsModule.state).toHaveProperty('isLoaded', true);
				expect(groupsModule.state).toHaveProperty('error', null);
				expect(groupsModule.state).toHaveProperty('groups', groups);
				expect(groupsModule.state).toHaveProperty('groupTree', groupTree);
			});
		});

		describe('requestGetGroupsError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				groupsModule.state = createMockGroupsState({
					isLoading: true,
					isLoaded: true,
					error: null,
				});

				groupsModule.mutations.requestGetGroupsError(groupsModule.state, 'error');

				expect(groupsModule.state).toHaveProperty('isLoading', false);
				expect(groupsModule.state).toHaveProperty('isLoaded', false);
				expect(groupsModule.state).toHaveProperty('error', 'error');
			});
		});
	});

	describe('getters', () => {
		const source = createMockRawGroup();
		const groupTree = getGroupTree(source);
		const groups = getGroupArray(groupTree);

		describe('getIsLoading', () => {
			it('returns isLoading', () => {
				const state = createMockGroupsState({ isLoading: true });

				const actual = (getters as any).getIsLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockGroupsState({ isLoaded: true });

				const actual = (getters as any).getIsLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasError', () => {
			it('returns true if error is not null', () => {
				const state = createMockGroupsState({ error: 'error' });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockGroupsState({ error: null });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getGroups', () => {
			it('returns groups', () => {
				const state = createMockGroupsState({ groups });
				const actual = (getters as any).getGroups(state);
				expect(actual).toBe(groups);
			});
		});

		describe('getGroupTree', () => {
			it('returns groupTree', () => {
				const state = createMockGroupsState({ groupTree });
				const actual = (getters as any).getGroupTree(state);
				expect(actual).toBe(groupTree);
			});
		});

		describe('getById', () => {
			it('returns the group with the given id if found', () => {
				const store = createTestStore();
				const groupState = createMockGroupsState({ groups });
				store.replaceState({ ...store.state, groups: groupState });
				expect(store.getters['groups/getById']('group1')).toBe(groups[0]);
			});

			it('returns undefined if no group matches the given id', () => {
				const store = createTestStore();
				const groupState = createMockGroupsState({ groups });
				store.replaceState({ ...store.state, groups: groupState });
				expect(store.getters['groups/getById']('unknown-group-id')).toBeUndefined();
			});
		});

		describe('getGroupNameById', () => {
			it('returns the group name with the given id if found', () => {
				const store = createTestStore();
				const groupState = createMockGroupsState({ groups });
				store.replaceState({ ...store.state, groups: groupState });
				expect(store.getters['groups/getGroupNameById']('group1')).toBe('A');
			});

			it('returns undefined if no group matches the given id', () => {
				const store = createTestStore();
				const groupState = createMockGroupsState({ groups });
				store.replaceState({ ...store.state, groups: groupState });
				expect(store.getters['groups/getGroupNameById']('unknown-group-id')).toBeUndefined();
			});
		});
	});

	describe('hooks', () => {
		describe('useGroups', () => {
			const source = createMockRawGroup();
			const groupTree = getGroupTree(source);
			const groups = getGroupArray(groupTree);

			it('returns computedRefs of group state', () => {
				const store = createTestStore();
				const state = createMockGroupsState({ isLoaded: true, groups, groupTree });
				store.replaceState({ ...store.state, groups: state });
				const actual = useGroups(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(true);
				expect(actual.hasError.value).toEqual(false);
				expect(actual.groups.value).toEqual(groups);
				expect(actual.groupTree.value).toEqual(groupTree);
			});

			it('dispatches "groups/getGroups" if groups are not loaded or loading', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				const groups = createMockGroupsState({ isLoading: false, isLoaded: false });
				store.replaceState({ ...store.state, groups });

				useGroups(store);

				expect(store.dispatch).toHaveBeenCalledWith('groups/getGroups');
			});

			it('does NOT dispatch "groups/getGroups" if resources are loading', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				const groups = createMockGroupsState({ isLoading: true, isLoaded: false });
				store.replaceState({ ...store.state, groups });

				useGroups(store);

				expect(store.dispatch).not.toHaveBeenCalledWith('groups/getGroups');
			});

			it('does NOT dispatch "groups/getGroups" if resources are loaded', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				const groups = createMockGroupsState({
					isLoading: false,
					isLoaded: true,
				});
				store.replaceState({
					...store.state,
					groups,
				});

				useGroups(store);

				expect(store.dispatch).not.toHaveBeenCalledWith('groups/getGroups');
			});
		});
	});
});
