import { computed } from '@vue/composition-api';
import { ActionTree, MutationTree, Module, Store, GetterTree } from 'vuex';
import { AsyncState, Group, RootState } from '@/store/types';
import soilApiService from '@/services/soilApiService';
import { getGroupArray, getGroupTree } from './utils';

export interface GroupsState extends AsyncState {
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
	groups: Group[];
	groupTree: Group[];
}

function createInitialState(): GroupsState {
	return {
		isLoading: false,
		isLoaded: false,
		error: null,
		groups: [],
		groupTree: [],
	};
}

const actions: ActionTree<GroupsState, RootState> = {
	async getGroups({ commit }): Promise<void> {
		commit('requestGetGroups');
		try {
			const rawGroups = await soilApiService.getGroups();
			const groupTree = getGroupTree(rawGroups);
			const groups = getGroupArray(groupTree);
			commit('requestGetGroupsSuccess', { groups, groupTree });
		} catch (error) {
			commit('requestGetGroupsError', error);
		}
	},
};

const mutations: MutationTree<GroupsState> = {
	requestGetGroups(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestGetGroupsSuccess(state, { groups, groupTree }) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
		state.groups = groups;
		state.groupTree = groupTree;
	},
	requestGetGroupsError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
	},
};

const getters: GetterTree<GroupsState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
	getGroups: (state) => state.groups,
	getGroupTree: (state) => state.groupTree,
	getById: (_, getters) => (id: string) => getters.getGroups.find((group: Group) => group.id === id),
	getGroupNameById: (_, getters) => (id: string) => getters.getById(id)?.name,
};

const createGroupsModule = (): Module<GroupsState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useGroups = (store: Store<RootState>) => {
	const isLoading = computed<boolean>(() => store.getters['groups/getIsLoading']);
	const isLoaded = computed<boolean>(() => store.getters['groups/getIsLoaded']);
	const hasError = computed<boolean>(() => store.getters['groups/getHasError']);
	const groups = computed<Group[]>(() => store.getters['groups/getGroups']);
	const groupTree = computed<Group[]>(() => store.getters['groups/getGroupTree']);

	if (!isLoading.value && !isLoaded.value) {
		store.dispatch('groups/getGroups');
	}

	return {
		isLoading,
		isLoaded,
		hasError,
		groups,
		groupTree,
	};
};

export { actions, mutations, getters, useGroups };

export default createGroupsModule;
