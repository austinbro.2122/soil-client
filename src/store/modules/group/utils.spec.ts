import { RawGroup } from '@/store/types';
import { getGroupArray, getGroupTree, getGroupTreeMinLevel, handleize } from './utils';

const createMockRawGroup = (slug = '/soilstack/'): RawGroup[] => [
	{ id: 'group1', name: 'A', path: `${slug}a/` },
	{ id: 'group2', name: 'A-A', path: `${slug}a/a-a/` },
	{ id: 'group3', name: 'A-A-A', path: `${slug}a/a-a/a-a-a/` },
	{ id: 'group4', name: 'A-A-B', path: `${slug}a/a-a/a-a-b/` },
	{ id: 'group5', name: 'A-B', path: `${slug}a/a-b/` },
	{ id: 'group6', name: 'A-B-A', path: `${slug}a/a-b/a-b-a/` },
	{ id: 'group7', name: 'B', path: `${slug}b/` },
	{ id: 'group8', name: 'B-A', path: `${slug}b/b-a/` },
	{ id: 'group9', name: 'C', path: `${slug}c/` },
];

describe('group utils', () => {
	describe('handleize', () => {
		const slug = handleize('Group Name');

		it('handleized name', () => {
			expect(slug).toBe('group-name');
		});
	});

	describe('getGroupTreeMinLevel', () => {
		it('should be 4', () => {
			const source = createMockRawGroup();
			const minLevel = getGroupTreeMinLevel(source);
			expect(minLevel).toBe(4);
		});

		it('should be 6', () => {
			const source = createMockRawGroup('/soilstack/region/north/');
			const minLevel = getGroupTreeMinLevel(source);
			expect(minLevel).toBe(6);
		});
	});

	describe('getGroupTree', () => {
		const source = createMockRawGroup();

		const groupTree = getGroupTree(source);

		it('should returns three groups', () => {
			expect(groupTree.length).toBe(3);
		});

		it('The "group1" should have two children', () => {
			expect(groupTree[0].children.length).toBe(2);
		});

		it('The name of the first child of the first chlid of the first group should be "A-A-A"', () => {
			expect(groupTree[0].children[0].children[0].name).toBe('A-A-A');
		});

		it('The level of the first child of the second group should be 2', () => {
			expect(groupTree[1].children[0].level).toBe(2);
		});
	});

	describe('getGroupArray', () => {
		const source = createMockRawGroup();

		const groupTree = getGroupTree(source);
		const groups = getGroupArray(groupTree);

		it('should have same length of source', () => {
			expect(groups.length).toBe(source.length);
		});

		it('id of the fifth group should be "group5"', () => {
			expect(groups[4].id).toBe('group5');
		});
	});
});
