import { Sample, Record, ResourceState, RootState, Sampling } from '@/store/types';
import { createBaseModule } from '@/store/utils/createBaseModule';
import { createUseAllResourceHook } from '@/store/utils/createUseAllResourceHook';
import { GetterTree } from 'vuex';

export interface SamplesState extends ResourceState {
	records: Sample[] & Record[];
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
}

function createInitialState(): SamplesState {
	return {
		records: [],
		isLoading: false,
		isLoaded: false,
		error: false,
	};
}

export const getters: GetterTree<SamplesState, RootState> = {
	getBySamplingIds: (_, getters, __, rootGetters) => (samplingIds: string[]) => {
		const samplings = rootGetters['samplings/getByIds'](samplingIds);
		const sampleIds = samplings.flatMap((sampling: Sampling) => sampling.results);
		return getters.getByIds(sampleIds);
	},

	getBySamplingCollectionIds: (_, getters, __, rootGetters) => (samplingCollectionIds: string[]) => {
		const samplingIds = rootGetters['samplings/getBySamplingCollectionIds'](samplingCollectionIds).map(
			({ id }: Sampling) => id
		);
		return getters.getBySamplingIds(samplingIds);
	},
};

const baseModule = createBaseModule('samples', createInitialState);

export const useAllSamples = createUseAllResourceHook('samples');

export default {
	...baseModule,
	getters: {
		...baseModule.getters,
		...getters,
	},
};
