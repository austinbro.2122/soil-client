/* eslint-disable @typescript-eslint/no-explicit-any */

import { GetterTree } from 'vuex';
import { RootState } from '@/store/types';
import SurveystackService from '@/services/surveystackService';
import { AuthService, MembershipService, GroupService } from '@/services/storageService';
export interface User {
	authProviders: any[];
	email: string;
	memberships: any[];
	name: string;
	permissions: string[];
	roles: string[];
	token: string;
	_id: string;
}
export interface AuthState {
	status: string;
	user: User;
	header: string;
}

const deleteCookie = (name: any) => {
	document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
};

const deleteCookies = () => {
	deleteCookie('user');
	deleteCookie('token');
};

const createInitialState = (): AuthState => ({
	status: AuthService.getStatus(),
	user: AuthService.getUser(),
	header: AuthService.getHeader(),
});

const initialState = createInitialState();

const getters: GetterTree<AuthState, RootState> = {
	authStatus: (state: any) => state.status,
	isLoggedIn: (state: any) => state.status === 'success',
	isAdmin: (state: any) => state.user && state.user.permissions && state.user.permissions.includes('admin'),
	isSuperAdmin: (state: any) => state.user && state.user.permissions && state.user.permissions.includes('super-admin'),
	user: (state: any) => state.user,
	getAuthorizationHeaderValue: (state: any) => state.header,
	getRoles: (_, getters) => getters.user?.roles ?? [],
};

const clearLocalData = ({ dispatch }: any) => {
	// remove items from storage
	AuthService.clear();
	MembershipService.clear();
	GroupService.clear();

	dispatch('reset', null, { root: true });
};

const actions = {
	reset({ commit }: any) {
		commit('RESET');
	},
	login({ commit }: any, auth: any) {
		return new Promise((resolve, reject) => {
			commit('authRequest');
			SurveystackService.post(auth.url, auth.user)
				.then((resp: any) => {
					const user = resp.data;
					const { email, token } = user;
					const header = `${email} ${token}`;

					AuthService.saveStatus('success');
					AuthService.saveUser(user);
					AuthService.saveHeader(header);

					commit('authSuccess', { user, header });
					resolve(resp.data);
				})
				.catch((err: any) => {
					console.log(err);
					commit('authError');
					reject(err);
				});
		});
	},
	logout({ commit, dispatch }: any) {
		return new Promise<void>((resolve) => {
			clearLocalData({ dispatch });
			deleteCookies();
			commit('logout');
			resolve();
		});
	},
};

const mutations = {
	RESET(state: any) {
		Object.assign(state, createInitialState());
	},
	authRequest(state: any) {
		state.status = 'loading';
	},
	authSuccess(state: any, { user, header }: any) {
		state.status = 'success';
		state.user = user;
		state.header = header;
		SurveystackService.setHeader('Authorization', header);
	},
	authError(state: any) {
		state.status = 'error';
		state.user = {};
		state.header = '';
	},
	logout(state: any) {
		state.status = '';
		state.user = {};
		state.header = '';
		SurveystackService.removeHeaders();
	},
};

export default {
	namespaced: true,
	state: initialState,
	getters,
	actions,
	mutations,
};
