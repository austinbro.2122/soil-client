import createResourcesModule, { actions, getters, useAllResources } from './resources';
import soilApiService from '../../services/soilApiService';
import {
	createMockField,
	createMockArea,
	createMockStratification,
	createMockLocationCollection,
	createMockLocation,
	createMockSample,
	createMockSampling,
	createMockSamplingCollection,
	createMockResourceMeta,
} from '../../../tests/mockGenerators';
import createTestStore from '../../../tests/createTestStore';
import { createMockResourcesState } from './test-utils';

jest.mock('../../services/soilApiService');

describe('resources modules', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;
		let dispatchSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
			dispatchSpy = jest.fn();
		});

		describe('getAllResources', () => {
			it("commits 'requestGetAllResources' mutation before making request", async () => {
				await (actions.getAllResources as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetAllResources');
			});

			it('commits requestGetAllRecordsSuccess mutation on success', async () => {
				(soilApiService.getAllResources as jest.Mock).mockResolvedValue({ fields: [], areas: [] });

				await (actions.getAllResources as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetAllResourcesSuccess', { fields: [], areas: [] });
			});

			it('commits requestGetAllRecordsError mutation on error', async () => {
				const error = new Error('error');
				(soilApiService.getAllResources as jest.Mock).mockRejectedValueOnce(error);

				await (actions.getAllResources as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetAllResourcesError', error);
			});
		});

		describe('submitSamplingCollection', () => {
			it("commits 'requestSubmitSamplingCollection' mutation before making request", async () => {
				await (actions.submitSamplingCollection as any)(
					{ commit: commitSpy, dispatch: dispatchSpy },
					{
						populatedSamplingCollection: {},
						groupId: '123',
					}
				);

				expect(commitSpy).toHaveBeenCalledWith('requestSubmitSamplingCollection');
			});

			it("commits 'requestSubmitSamplingCollectionSuccess' mutation on success", async () => {
				(soilApiService.submitSamplingCollection as jest.Mock).mockResolvedValue({
					samplingCollections: [],
					samplings: [],
					samples: [],
				});

				await (actions.submitSamplingCollection as any)(
					{ commit: commitSpy, dispatch: dispatchSpy },
					{
						populatedSamplingCollection: {},
						groupId: '123',
					}
				);

				expect(commitSpy).toHaveBeenCalledWith('requestSubmitSamplingCollectionSuccess', {
					samplingCollections: [],
					samplings: [],
					samples: [],
				});
			});

			it("commits 'requestSubmitSamplingCollectionError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.submitSamplingCollection as jest.Mock).mockRejectedValueOnce(error);

				await (actions.submitSamplingCollection as any)(
					{ commit: commitSpy, dispatch: dispatchSpy },
					{
						populatedSamplingCollection: {},
						groupId: '123',
					}
				);

				expect(commitSpy).toHaveBeenCalledWith('requestSubmitSamplingCollectionError', error);
			});
		});

		describe('acknowledgeSubmitSamplingCollectionError', () => {
			it("commits 'resetSubmitSamplingCollectionRequest' mutation", async () => {
				await (actions.acknowledgeSubmitSamplingCollectionError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetSubmitSamplingCollectionRequest');
			});
		});
	});

	describe('mutations', () => {
		let resourcesModule: any;

		beforeEach(() => {
			resourcesModule = createResourcesModule();
		});

		describe('requestGetAllResources', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				resourcesModule.state = createMockResourcesState({
					isLoading: false,
					isLoaded: true,
					error: 'error',
				});

				resourcesModule.mutations.requestGetAllResources(resourcesModule.state);

				expect(resourcesModule.state).toHaveProperty('isLoading', true);
				expect(resourcesModule.state).toHaveProperty('isLoaded', false);
				expect(resourcesModule.state).toHaveProperty('error', null);
			});
		});

		describe('requestGetAllResourcesSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null, and puts resources into state by key', () => {
				resourcesModule.state = createMockResourcesState({
					isLoading: true,
					isLoaded: false,
					error: 'error',
					fields: [],
					areas: [],
					stratifications: [],
					locationCollections: [],
					locations: [],
				});

				resourcesModule.mutations.requestGetAllResourcesSuccess(resourcesModule.state, {
					fields: [{ id: '1' }],
					areas: [{ id: '2' }],
					stratifications: [{ id: '3' }],
					locationCollections: [{ id: '4' }],
					locations: [{ id: '5' }],
					samples: [{ id: '6' }],
					samplings: [{ id: '7' }],
					samplingCollections: [{ id: '8' }],
				});

				expect(resourcesModule.state).toHaveProperty('isLoading', false);
				expect(resourcesModule.state).toHaveProperty('isLoaded', true);
				expect(resourcesModule.state).toHaveProperty('error', null);
				expect(resourcesModule.state).toHaveProperty('fields', [{ id: '1' }]);
				expect(resourcesModule.state).toHaveProperty('areas', [{ id: '2' }]);
				expect(resourcesModule.state).toHaveProperty('stratifications', [{ id: '3' }]);
				expect(resourcesModule.state).toHaveProperty('locationCollections', [{ id: '4' }]);
				expect(resourcesModule.state).toHaveProperty('locations', [{ id: '5' }]);
				expect(resourcesModule.state).toHaveProperty('samples', [{ id: '6' }]);
				expect(resourcesModule.state).toHaveProperty('samplings', [{ id: '7' }]);
				expect(resourcesModule.state).toHaveProperty('samplingCollections', [{ id: '8' }]);
			});
		});

		describe('requestGetAllResourcesError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				resourcesModule.state = createMockResourcesState({
					isLoading: true,
					isLoaded: true,
					error: null,
				});

				resourcesModule.mutations.requestGetAllResourcesError(resourcesModule.state, 'error');

				expect(resourcesModule.state).toHaveProperty('isLoading', false);
				expect(resourcesModule.state).toHaveProperty('isLoaded', false);
				expect(resourcesModule.state).toHaveProperty('error', 'error');
			});
		});

		describe('requestSubmitSamplingCollection', () => {
			it('sets submitIsLoading to true and submitError to null', () => {
				resourcesModule.state = createMockResourcesState({
					submitIsLoading: false,
					submitError: 'error',
				});

				resourcesModule.mutations.requestSubmitSamplingCollection(resourcesModule.state);

				expect(resourcesModule.state).toHaveProperty('submitIsLoading', true);
				expect(resourcesModule.state).toHaveProperty('submitError', null);
			});
		});

		describe('requestSubmitSamplingCollectionSuccess', () => {
			beforeEach(() => {
				resourcesModule.state = createMockResourcesState({
					submitIsLoading: true,
					submitError: 'error',
					samples: [],
					samplings: [],
					samplingCollections: [],
				});

				const mockResources = {
					samples: [{ id: '1' }, { id: '2' }],
					samplings: [{ id: '3' }, { id: '4' }],
					samplingCollections: [{ id: '5' }, { id: '6' }],
				};

				resourcesModule.mutations.requestSubmitSamplingCollectionSuccess(resourcesModule.state, mockResources);
			});

			it('sets submitIsLoading to false and submitError to null', () => {
				expect(resourcesModule.state).toHaveProperty('submitIsLoading', false);
				expect(resourcesModule.state).toHaveProperty('submitError', null);
			});

			it('adds samples from passed in resources to samples array in state', () => {
				expect(resourcesModule.state).toHaveProperty('samples', [{ id: '1' }, { id: '2' }]);
			});

			it('adds samplings from passed in resources to samplings array in state', () => {
				expect(resourcesModule.state).toHaveProperty('samplings', [{ id: '3' }, { id: '4' }]);
			});

			it('adds samplingCollections from passed in resources to samplingCollections array in state', () => {
				expect(resourcesModule.state).toHaveProperty('samplingCollections', [{ id: '5' }, { id: '6' }]);
			});
		});

		describe('requestSubmitSamplingCollectionError', () => {
			it('sets submitIsLoading to false and submitError to the passed in error', () => {
				resourcesModule.state = createMockResourcesState({
					submitIsLoading: true,
					submitError: null,
				});

				resourcesModule.mutations.requestSubmitSamplingCollectionError(resourcesModule.state, 'error');

				expect(resourcesModule.state).toHaveProperty('submitIsLoading', false);
				expect(resourcesModule.state).toHaveProperty('submitError', 'error');
			});
		});

		describe('resetSubmitSamplingCollectionRequest', () => {
			it('sets submitIsLoading to false and submitError to null', () => {
				resourcesModule.state = createMockResourcesState({
					submitIsLoading: true,
					submitError: 'error',
				});

				resourcesModule.mutations.resetSubmitSamplingCollectionRequest(resourcesModule.state);

				expect(resourcesModule.state).toHaveProperty('submitIsLoading', false);
				expect(resourcesModule.state).toHaveProperty('submitError', null);
			});
		});
	});

	describe('getters', () => {
		describe('getIsLoading', () => {
			it('returns isLoading', () => {
				const state = createMockResourcesState({ isLoading: true });

				const actual = (getters as any).getIsLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockResourcesState({ isLoaded: true });

				const actual = (getters as any).getIsLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasError', () => {
			it('returns true if error is not null', () => {
				const state = createMockResourcesState({ error: 'error' });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockResourcesState({ error: null });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getSubmitIsLoading', () => {
			it('returns submitIsLoading', () => {
				const state = createMockResourcesState({ submitIsLoading: true });

				const actual = (getters as any).getSubmitIsLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getSubmitHasError', () => {
			it('returns true if submitError is not null', () => {
				const state = createMockResourcesState({ submitError: 'error' });

				const actual = (getters as any).getSubmitHasError(state);

				expect(actual).toBe(true);
			});

			it('returns false if submitError is null', () => {
				const state = createMockResourcesState({ submitError: null });

				const actual = (getters as any).getSubmitHasError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getFields', () => {
			it('returns fields', () => {
				const fields = [createMockField()];
				const state = createMockResourcesState({ fields });

				const actual = (getters as any).getFields(state);

				expect(actual).toBe(fields);
			});
		});

		describe('getAreas', () => {
			it('returns areas', () => {
				const areas = [createMockArea()];
				const state = createMockResourcesState({ areas });

				const actual = (getters as any).getAreas(state);

				expect(actual).toBe(areas);
			});
		});

		describe('getStratifications', () => {
			it('returns stratifications', () => {
				const stratifications = [createMockStratification()];
				const state = createMockResourcesState({ stratifications });

				const actual = (getters as any).getStratifications(state);

				expect(actual).toBe(stratifications);
			});
		});

		describe('getLocationCollections', () => {
			it('returns locationCollections', () => {
				const locationCollections = [createMockLocationCollection()];
				const state = createMockResourcesState({ locationCollections });

				const actual = (getters as any).getLocationCollections(state);

				expect(actual).toBe(locationCollections);
			});
		});

		describe('getLocations', () => {
			it('returns locations', () => {
				const locations = [createMockLocation()];
				const state = createMockResourcesState({ locations });

				const actual = (getters as any).getLocations(state);

				expect(actual).toBe(locations);
			});
		});

		describe('getSamples', () => {
			it('returns samples', () => {
				const samples = [createMockSample()];
				const state = createMockResourcesState({ samples });

				const actual = (getters as any).getSamples(state);

				expect(actual).toBe(samples);
			});
		});

		describe('getSamplings', () => {
			it('returns samplings', () => {
				const samplings = [createMockSampling()];
				const state = createMockResourcesState({ samplings });

				const actual = (getters as any).getSamplings(state);

				expect(actual).toBe(samplings);
			});
		});

		describe('getSamplingCollections', () => {
			it('returns samplingCollections', () => {
				const samplingCollections = [createMockSamplingCollection()];
				const state = createMockResourcesState({ samplingCollections });

				const actual = (getters as any).getSamplingCollections(state);

				expect(actual).toBe(samplingCollections);
			});
		});

		describe('getFieldById', () => {
			it('returns the field matching the given id when present', () => {
				const mockField = createMockField({ id: '123' });
				const state = createMockResourcesState({
					fields: [mockField],
				});

				const actual = (getters as any).getFieldById(state)('123');

				expect(actual).toBe(mockField);
			});

			it('returns undefined if no field with the given id is found', () => {
				const state = createMockResourcesState({
					fields: [],
				});

				const actual = (getters as any).getFieldById(state)('123');

				expect(actual).toBe(undefined);
			});
		});

		describe('getAreaById', () => {
			it('returns the area matching the given id when present', () => {
				const mockArea = createMockArea({ id: '123' });
				const state = createMockResourcesState({
					areas: [mockArea],
				});

				const actual = (getters as any).getAreaById(state)('123');

				expect(actual).toBe(mockArea);
			});

			it('returns undefined if no area with the given id is found', () => {
				const state = createMockResourcesState({
					areas: [],
				});

				const actual = (getters as any).getAreaById(state)('123');

				expect(actual).toBe(undefined);
			});
		});

		describe('getLocationCollectionById', () => {
			it('returns the location collection matching the given id when present', () => {
				const mockLocationCollection = createMockLocationCollection({ id: '123' });
				const state = createMockResourcesState({
					locationCollections: [mockLocationCollection],
				});

				const actual = (getters as any).getLocationCollectionById(state)('123');

				expect(actual).toBe(mockLocationCollection);
			});

			it('returns undefined if no location collection with the given id is found', () => {
				const state = createMockResourcesState({
					locationCollections: [],
				});

				const actual = (getters as any).getLocationCollectionById(state)('123');

				expect(actual).toBe(undefined);
			});
		});

		describe('getSamplingCollectionById', () => {
			it('returns the sampling collection matching the given id when present', () => {
				const mockSamplingCollection = createMockSamplingCollection({ id: '123' });
				const state = createMockResourcesState({
					samplingCollections: [mockSamplingCollection],
				});

				const actual = (getters as any).getSamplingCollectionById(state)('123');

				expect(actual).toBe(mockSamplingCollection);
			});

			it('returns undefined if no sampling collection with the given id is found', () => {
				const state = createMockResourcesState({
					samplingCollections: [],
				});

				const actual = (getters as any).getSamplingCollectionById(state)('123');

				expect(actual).toBe(undefined);
			});
		});

		describe('getFieldsByIds', () => {
			it('returns an array of fields matching the given ids if present', () => {
				const mockField1 = createMockField({ id: '1' });
				const mockField2 = createMockField({ id: '2' });
				const state = createMockResourcesState({
					fields: [mockField1, mockField2],
				});

				const actual = (getters as any).getFieldsByIds(state)(['1', '2']);

				expect(actual).toEqual([mockField1, mockField2]);
			});
		});

		describe('getAreasByIds', () => {
			it('returns an array of areas matching the given ids if present', () => {
				const mockArea1 = createMockArea({ id: '1' });
				const mockArea2 = createMockArea({ id: '2' });
				const state = createMockResourcesState({
					areas: [mockArea1, mockArea2],
				});

				const actual = (getters as any).getAreasByIds(state)(['1', '2']);

				expect(actual).toEqual([mockArea1, mockArea2]);
			});
		});

		describe('getLocationsByIds', () => {
			it('returns an array of locations matching the given ids if present', () => {
				const mockLocation1 = createMockLocation({ id: '1' });
				const mockLocation2 = createMockLocation({ id: '2' });
				const state = createMockResourcesState({
					locations: [mockLocation1, mockLocation2],
				});

				const actual = (getters as any).getLocationsByIds(state)(['1', '2']);

				expect(actual).toEqual([mockLocation1, mockLocation2]);
			});
		});

		describe('getLocationCollectionsByIds', () => {
			it('returns an array of location collections matching the given ids if present', () => {
				const mockLocationCollection1 = createMockLocationCollection({ id: '1' });
				const mockLocationCollection2 = createMockLocationCollection({ id: '2' });
				const state = createMockResourcesState({
					locationCollections: [mockLocationCollection1, mockLocationCollection2],
				});

				const actual = (getters as any).getLocationCollectionsByIds(state)(['1', '2']);

				expect(actual).toEqual([mockLocationCollection1, mockLocationCollection2]);
			});
		});

		describe('getSamplingCollectionsByIds', () => {
			it('returns an array of sampling collections matching the given ids if present', () => {
				const mockSamplingCollection1 = createMockSamplingCollection({ id: '1' });
				const mockSamplingCollection2 = createMockSamplingCollection({ id: '2' });
				const state = createMockResourcesState({
					samplingCollections: [mockSamplingCollection1, mockSamplingCollection2],
				});

				const actual = (getters as any).getSamplingCollectionsByIds(state)(['1', '2']);

				expect(actual).toEqual([mockSamplingCollection1, mockSamplingCollection2]);
			});
		});

		describe('getSamplingsByIds', () => {
			it('returns an array of sampling collections matching the given ids if present', () => {
				const mockSampling1 = createMockSampling({ id: '1' });
				const mockSampling2 = createMockSampling({ id: '2' });
				const state = createMockResourcesState({
					samplings: [mockSampling1, mockSampling2],
				});

				const actual = (getters as any).getSamplingsByIds(state)(['1', '2']);

				expect(actual).toEqual([mockSampling1, mockSampling2]);
			});
		});

		describe('getSamplesByIds', () => {
			it('returns an array of sample collections matching the given ids if present', () => {
				const mockSample1 = createMockSample({ id: '1' });
				const mockSample2 = createMockSample({ id: '2' });
				const state = createMockResourcesState({
					samples: [mockSample1, mockSample2],
				});

				const actual = (getters as any).getSamplesByIds(state)(['1', '2']);

				expect(actual).toEqual([mockSample1, mockSample2]);
			});
		});

		describe('getAreasByFieldsIds and getAreasByFieldId', () => {
			const store = createTestStore();
			const mockField1 = createMockField({ id: 'f1', areas: ['a1'] });
			const mockField2 = createMockField({ id: 'f2', areas: ['a2'] });
			const mockArea1 = createMockArea({ id: 'a1' });
			const mockArea2 = createMockArea({ id: 'a2' });
			const mockArea3 = createMockArea({ id: 'a3' });
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					fields: [mockField1, mockField2],
					areas: [mockArea1, mockArea2, mockArea3],
				}),
			});

			describe('getAreasByFieldIds', () => {
				it('returns an array of areas belonging to the fields with the given ids', () => {
					const actual = store.getters['resources/getAreasByFieldIds'](['f1', 'f2']);

					expect(actual).toEqual([mockArea1, mockArea2]);
				});
			});

			describe('getAreasByFieldId', () => {
				it('returns an array of areas belonging to the field with the given id', () => {
					const actual = store.getters['resources/getAreasByFieldId']('f1');

					expect(actual).toEqual([mockArea1]);
				});
			});
		});

		describe('getStratificationsByFieldIds and getStratificationsByFieldId', () => {
			const store = createTestStore();
			const mockField1 = createMockField({ id: 'field1', areas: ['area1'] });
			const mockField2 = createMockField({ id: 'field2', areas: ['area2'] });
			const mockStratification1 = createMockStratification({ object: 'area1' });
			const mockStratification2 = createMockStratification({ object: 'area2' });
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					fields: [mockField1, mockField2],
					stratifications: [mockStratification1, mockStratification2],
				}),
			});

			describe('getStratificationsByFieldIds', () => {
				it('returns an array of stratifications that apply to areas belonging to the fields with the given ids', () => {
					const actual = store.getters['resources/getStratificationsByFieldIds'](['field1', 'field2']);

					expect(actual).toEqual([mockStratification1, mockStratification2]);
				});
			});

			describe('getStratificationsByFieldId', () => {
				it('returns an array of stratificationst hat apply to the areas belonging to the field with the given id', () => {
					const actual = store.getters['resources/getStratificationsByFieldId']('field1');

					expect(actual).toEqual([mockStratification1]);
				});
			});
		});

		describe('getLocationCollectionsByFieldIds and getLocationCollectionsByFieldId', () => {
			const store = createTestStore();
			const mockLocationCollection1 = createMockLocationCollection({ featureOfInterest: 'field1' });
			const mockLocationCollection2 = createMockLocationCollection({ featureOfInterest: 'field2' });
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					locationCollections: [mockLocationCollection1, mockLocationCollection2],
				}),
			});

			describe('getLocationCollectionsByFieldIds', () => {
				it('returns an array of the location collections that reference fields with the given ids', () => {
					const actual = store.getters['resources/getLocationCollectionsByFieldIds'](['field1', 'field2']);

					expect(actual).toEqual([mockLocationCollection1, mockLocationCollection2]);
				});
			});

			describe('getLocationCollectionsByFieldId', () => {
				it('returns an array of the location collections that reference the field with the given id', () => {
					const actual = store.getters['resources/getLocationCollectionsByFieldId']('field1');

					expect(actual).toEqual([mockLocationCollection1]);
				});
			});
		});

		describe('getLocationsByFieldIds and getLocationsByFieldId', () => {
			const store = createTestStore();
			const mockLocationCollection1 = createMockLocationCollection({
				featureOfInterest: 'field1',
				features: ['loc1', 'loc2'],
			});
			const mockLocationCollection2 = createMockLocationCollection({
				featureOfInterest: 'field2',
				features: ['loc3'],
			});
			const mockLocation1 = createMockLocation({ id: 'loc1' });
			const mockLocation2 = createMockLocation({ id: 'loc2' });
			const mockLocation3 = createMockLocation({ id: 'loc3' });
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					locationCollections: [mockLocationCollection1, mockLocationCollection2],
					locations: [mockLocation1, mockLocation2, mockLocation3],
				}),
			});

			describe('getLocationsByFieldIds', () => {
				it('returns an array of the locations referenced by the location collections that reference the fields with the given ids', () => {
					const actual = store.getters['resources/getLocationsByFieldIds'](['field1', 'field2']);

					expect(actual).toEqual([mockLocation1, mockLocation2, mockLocation3]);
				});
			});

			describe('getLocationsByFieldId', () => {
				it('returns an array of the locations referenced by the location collections that reference the field with the given id', () => {
					const actual = store.getters['resources/getLocationsByFieldId']('field1');

					expect(actual).toEqual([mockLocation1, mockLocation2]);
				});
			});
		});

		describe('getSamplingCollectionsByFieldId', () => {
			it('returns an array of the sampling collections which reference the given field id', () => {
				const store = createTestStore();
				const mockSamplingCollection1 = createMockSamplingCollection({
					id: 'sc1',
					featureOfInterest: 'field1',
				});
				const mockSamplingCollection2 = createMockSamplingCollection({
					id: 'sc2',
					featureOfInterest: 'field1',
				});
				const mockSamplingCollection3 = createMockSamplingCollection({
					id: 'sc3',
					featureOfInterest: 'field2',
				});
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						samplingCollections: [mockSamplingCollection1, mockSamplingCollection2, mockSamplingCollection3],
					}),
				});

				const actual = store.getters['resources/getSamplingCollectionsByFieldId']('field1');

				expect(actual).toEqual([mockSamplingCollection1, mockSamplingCollection2]);
			});
		});

		describe('getSamplingsBySamplingCollectionIds and getSamplingsBySamplingCollectionId', () => {
			const store = createTestStore();
			const mockSamplingCollection1 = createMockSamplingCollection({
				id: 'sc1',
				members: ['sampling1', 'sampling2'],
			});
			const mockSamplingCollection2 = createMockSamplingCollection({
				id: 'sc2',
				members: ['sampling3', 'sampling4'],
			});
			const mockSampling1 = createMockSampling({ id: 'sampling1' });
			const mockSampling2 = createMockSampling({ id: 'sampling2' });
			const mockSampling3 = createMockSampling({ id: 'sampling3' });
			const mockSampling4 = createMockSampling({ id: 'sampling4' });
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					samplingCollections: [mockSamplingCollection1, mockSamplingCollection2],
					samplings: [mockSampling1, mockSampling2, mockSampling3, mockSampling4],
				}),
			});

			describe('getSamplingsBySamplingCollectionIds', () => {
				it('returns an array of the Samplings referenced by the sampling collections with the given ids', () => {
					const actual = store.getters['resources/getSamplingsBySamplingCollectionIds'](['sc1', 'sc2']);

					expect(actual).toEqual([mockSampling1, mockSampling2, mockSampling3, mockSampling4]);
				});
			});

			describe('getSamplingsBySamplingCollectionId', () => {
				it('returns an array of the Samplings referenced by the sampling collection with the given id', () => {
					const actual = store.getters['resources/getSamplingsBySamplingCollectionId']('sc1');

					expect(actual).toEqual([mockSampling1, mockSampling2]);
				});
			});
		});

		describe('getSamplesBySamplingCollectionId and getSamplesBySamplingCollectionIds', () => {
			const store = createTestStore();
			const mockSamplingCollection1 = createMockSamplingCollection({
				id: 'sc1',
				members: ['sampling1', 'sampling2'],
			});
			const mockSamplingCollection2 = createMockSamplingCollection({
				id: 'sc2',
				members: ['sampling3', 'sampling4'],
			});
			const mockSampling1 = createMockSampling({ id: 'sampling1', results: ['s1'] });
			const mockSampling2 = createMockSampling({ id: 'sampling2', results: ['s2'] });
			const mockSampling3 = createMockSampling({ id: 'sampling3', results: ['s3'] });
			const mockSampling4 = createMockSampling({ id: 'sampling4', results: ['s4'] });
			const mockSample1 = createMockSample({ id: 's1' });
			const mockSample2 = createMockSample({ id: 's2' });
			const mockSample3 = createMockSample({ id: 's3' });
			const mockSample4 = createMockSample({ id: 's4' });
			store.replaceState({
				...store.state,
				resources: createMockResourcesState({
					samplingCollections: [mockSamplingCollection1, mockSamplingCollection2],
					samplings: [mockSampling1, mockSampling2, mockSampling3, mockSampling4],
					samples: [mockSample1, mockSample2, mockSample3, mockSample4],
				}),
			});

			describe('getSamplesBySamplingCollectionIds', () => {
				it('returns an array of the Samples referenced by the samplings referenced by the sampling collections with the given ids', () => {
					const actual = store.getters['resources/getSamplesBySamplingCollectionIds'](['sc1', 'sc2']);

					expect(actual).toEqual([mockSample1, mockSample2, mockSample3, mockSample4]);
				});
			});

			describe('getSamplesBySamplingCollectionId', () => {
				it('returns an array of the Samples referenced by the samplings referenced by the sampling collections with the given ids', () => {
					const actual = store.getters['resources/getSamplesBySamplingCollectionIds']('sc1');

					expect(actual).toEqual([mockSample1, mockSample2]);
				});
			});
		});

		describe('getLocationsBySamplingCollectionId', () => {
			it('returns an array of the locations belonging to the location collection referenced by the sampling collection with the given id', () => {
				const store = createTestStore();
				const mockSamplingCollection = createMockSamplingCollection({
					id: 'sc1',
					object: 'locationCollection1',
				});
				const mockLocationCollection = createMockLocationCollection({
					id: 'locationCollection1',
					features: ['loc1', 'loc2'],
				});
				const mockLocation1 = createMockLocation({ id: 'loc1' });
				const mockLocation2 = createMockLocation({ id: 'loc2' });
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						isLoaded: true,
						samplingCollections: [mockSamplingCollection],
						locationCollections: [mockLocationCollection],
						locations: [mockLocation1, mockLocation2],
					}),
				});

				const actual = store.getters['resources/getLocationsBySamplingCollectionId']('sc1');

				expect(actual).toEqual([mockLocation1, mockLocation2]);
			});

			it('returns an empty array if isLoaded is false', () => {
				const store = createTestStore();
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({ isLoaded: false }),
				});

				const actual = store.getters['resources/getLocationsBySamplingCollectionId']('sc1');

				expect(actual).toEqual([]);
			});
		});

		describe('getLocationsBySamplingCollectionIds', () => {
			it('returns an array of the locations belonging to the location collections referenced by the sampling collections with the given ids', () => {
				const store = createTestStore();
				const mockSamplingCollection1 = createMockSamplingCollection({
					id: 'sc1',
					object: 'locationCollection1',
				});
				const mockSamplingCollection2 = createMockSamplingCollection({
					id: 'sc2',
					object: 'locationCollection2',
				});
				const mockLocationCollection1 = createMockLocationCollection({
					id: 'locationCollection1',
					features: ['loc1', 'loc2'],
				});
				const mockLocationCollection2 = createMockLocationCollection({
					id: 'locationCollection2',
					features: ['loc3', 'loc4'],
				});
				const mockLocation1 = createMockLocation({ id: 'loc1' });
				const mockLocation2 = createMockLocation({ id: 'loc2' });
				const mockLocation3 = createMockLocation({ id: 'loc3' });
				const mockLocation4 = createMockLocation({ id: 'loc4' });
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						isLoaded: true,
						samplingCollections: [mockSamplingCollection1, mockSamplingCollection2],
						locationCollections: [mockLocationCollection1, mockLocationCollection2],
						locations: [mockLocation1, mockLocation2, mockLocation3, mockLocation4],
					}),
				});

				const actual = store.getters['resources/getLocationsBySamplingCollectionIds'](['sc1', 'sc2']);

				expect(actual).toEqual([mockLocation1, mockLocation2, mockLocation3, mockLocation4]);
			});

			it('returns an empty array if isLoaded is false', () => {
				const store = createTestStore();
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({ isLoaded: false }),
				});

				const actual = store.getters['resources/getLocationsBySamplingCollectionIds'](['sc1', 'sc2']);

				expect(actual).toEqual([]);
			});
		});

		describe('getFieldStatusById', () => {
			it('returns null if no field matches the given id', () => {
				const store = createTestStore();

				const actual = store.getters['resources/getFieldStatusById']('field1');

				expect(actual).toBeNull();
			});

			describe('when the field exists but has not yet been stratified', () => {
				const store = createTestStore();
				const mockField = createMockField({
					id: 'field1',
					meta: createMockResourceMeta({
						submittedAt: new Date(123).toISOString(),
					}),
				});
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField],
					}),
				});

				const actual = store.getters['resources/getFieldStatusById']('field1');

				it('returns "posted" as the status.state', () => {
					expect(actual.state).toBe('posted');
				});

				it("returns the field's submittedAt as status.timestamp", () => {
					expect(actual.timestamp).toBe(mockField.meta.submittedAt);
				});
			});

			describe('when the field exists and has been stratified but has not been sampled yet', () => {
				const store = createTestStore();
				const mockField = createMockField({
					id: 'field1',
					areas: ['area1'],
					meta: createMockResourceMeta({
						submittedAt: new Date(123).toISOString(),
					}),
				});
				const mockStratification = createMockStratification({
					id: 'strat1',
					object: 'area1',
					meta: createMockResourceMeta({
						submittedAt: new Date(456).toISOString(),
					}),
				});
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField],
						stratifications: [mockStratification],
					}),
				});

				const actual = store.getters['resources/getFieldStatusById']('field1');

				it('returns "stratified" as the status.state', () => {
					expect(actual.state).toBe('stratified');
				});

				it("returns the stratification's submittedAt as status.timestamp", () => {
					expect(actual.timestamp).toBe(mockStratification.meta.submittedAt);
				});
			});

			describe('when the field exists and has been sampled', () => {
				const store = createTestStore();
				const mockField = createMockField({
					id: 'field1',
					areas: ['area1'],
					meta: createMockResourceMeta({
						submittedAt: new Date(123).toISOString(),
					}),
				});
				const mockStratification = createMockStratification({
					id: 'strat1',
					object: 'area1',
					meta: createMockResourceMeta({
						submittedAt: new Date(456).toISOString(),
					}),
				});
				const mockSamplingCollection = createMockSamplingCollection({
					id: 'sc1',
					featureOfInterest: 'field1',
					meta: createMockResourceMeta({
						submittedAt: new Date(789).toISOString(),
					}),
				});
				store.replaceState({
					...store.state,
					resources: createMockResourcesState({
						fields: [mockField],
						stratifications: [mockStratification],
						samplingCollections: [mockSamplingCollection],
					}),
				});

				const actual = store.getters['resources/getFieldStatusById']('field1');

				it('returns "sampled" as the status.state', () => {
					expect(actual.state).toBe('sampled');
				});

				it("returns the sampling collection's submittedAt as status.timestamp", () => {
					expect(actual.timestamp).toBe(mockSamplingCollection.meta?.submittedAt);
				});
			});
		});
	});

	describe('hooks', () => {
		describe('useAllResources', () => {
			it('returns computedRefs of resources state', () => {
				const store = createTestStore();
				const resources = createMockResourcesState({
					isLoaded: true,
					fields: [createMockField()],
					areas: [createMockArea()],
					stratifications: [createMockStratification()],
					locationCollections: [createMockLocationCollection()],
					locations: [createMockLocation()],
				});
				store.replaceState({
					...store.state,
					resources,
				});

				const actual = useAllResources(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(true);
				expect(actual.hasError.value).toEqual(false);
				expect(actual.fields.value).toEqual([createMockField()]);
				expect(actual.areas.value).toEqual([createMockArea()]);
				expect(actual.stratifications.value).toEqual([createMockStratification()]);
				expect(actual.locationCollections.value).toEqual([createMockLocationCollection()]);
				expect(actual.locations.value).toEqual([createMockLocation()]);
			});

			it('dispatches "resources/getAllResources" if resources are not loaded or loading', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				const resources = createMockResourcesState({
					isLoading: false,
					isLoaded: false,
				});
				store.replaceState({
					...store.state,
					resources,
				});

				useAllResources(store);

				expect(store.dispatch).toHaveBeenCalledWith('resources/getAllResources');
			});

			it('does NOT dispatch "resources/getAllResources" if resources are loading', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				const resources = createMockResourcesState({
					isLoading: true,
					isLoaded: false,
				});
				store.replaceState({
					...store.state,
					resources,
				});

				useAllResources(store);

				expect(store.dispatch).not.toHaveBeenCalledWith('resources/getAllResources');
			});

			it('does NOT dispatch "resources/getAllResources" if resources are already loaded', () => {
				const store = createTestStore();
				store.dispatch = jest.fn();
				const resources = createMockResourcesState({
					isLoading: false,
					isLoaded: true,
				});
				store.replaceState({
					...store.state,
					resources,
				});

				useAllResources(store);

				expect(store.dispatch).not.toHaveBeenCalledWith('resources/getAllResources');
			});
		});
	});
});
