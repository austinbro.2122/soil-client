import { GetterTree } from 'vuex';
import { RootState } from '@/store/types';

const getters: GetterTree<RootState, RootState> = {
	getSamplingCollectionStatusByFieldId: (_, getters) => (fieldId: string) => {
		const draftSamplingCollections = getters['samplingCollections/getByFieldId'](fieldId);
		const submittedSamplingCollections = getters['resources/getSamplingCollectionsByFieldId'](fieldId);
		if (submittedSamplingCollections.length > 0) {
			return 'SUBMITTED';
		}
		if (draftSamplingCollections.length > 0) {
			return 'DRAFT';
		}
		return null;
	},
	getSamplingCollectionStatusBySamplingCollectionId: (_, getters) => (samplingCollectionId: string) => {
		const draftSamplingCollection = getters['samplingCollections/getById'](samplingCollectionId);
		const submittedSamplingCollection = getters['resources/getSamplingCollectionById'](samplingCollectionId);
		if (submittedSamplingCollection) {
			return 'SUBMITTED';
		}
		if (draftSamplingCollection) {
			return 'DRAFT';
		}
		return null;
	},
	getFieldGroupPath: (_, getters) => (fieldId: string) => {
		const field = getters['resources/getFieldById'](fieldId);
		const group = getters['groups/getById'](field.meta.groupId);
		return group ? group.path : null;
	},
	getIsFieldWithinGroupPath: (_, getters) => (fieldId: string, groupPath: string) => {
		const fieldGroupPath = getters['getFieldGroupPath'](fieldId);
		return fieldGroupPath ? fieldGroupPath.startsWith(groupPath) : false;
	},
};

export default getters;
