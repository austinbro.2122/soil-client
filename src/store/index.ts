import Vuex, { StoreOptions } from 'vuex';
import { RootState } from '@/store/types';
import samplingCollections from '@/store/modules/samplingCollections';
import samplings from '@/store/modules/samplings';
import samples from '@/store/modules/samples';
import offlineMaps from '@/store/modules/offlineMaps';
import auth from '@/store/modules/auth';
import invitation from '@/store/modules/invitation';
import createResourcesModule from '@/store/modules/resources';
import createGroupsModule from '@/store/modules/group';
import createReassignModule from '@/store/modules/reassign';
import createFieldsViewModule from '@/store/modules/fieldsView';
import caches from '@/store/plugins/caches';
import redirects from '@/store/plugins/redirects';
import cleanupDrafts from '@/store/plugins/cleanupDrafts';
import getters from './getters';

// Vuex mutates the options object used to create the store,
// which is why we have a function to create a new config object
// as opposed to an object literal here.
export function createStoreConfig() {
	const storeConfig: StoreOptions<RootState> = {
		state: {
			version: '0.0.1',
		},
		modules: {
			samplingCollections,
			samplings,
			samples,
			offlineMaps,
			auth,
			invitation,
			fieldsView: createFieldsViewModule(),
			resources: createResourcesModule(),
			groups: createGroupsModule(),
			reassign: createReassignModule(),
		},
		plugins: [caches, cleanupDrafts, redirects],
		strict: process.env.NODE_ENV !== 'production',
		getters,
	};
	return storeConfig;
}

export default function createStore() {
	const storeConfig = createStoreConfig();
	return new Vuex.Store<RootState>(storeConfig);
}
