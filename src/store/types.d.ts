import { Feature, Polygon, Point } from 'geojson';

type ReferenceIdOwners =
	| 'heartland'
	| 'soilstack-draft'
	| 'carbofarm'
	| 'flurosense-field'
	| 'flurosense-producer'
	| 'flurosense-program'
	| 'flurosense-year';

export interface ReferenceId {
	owner: ReferenceIdOwners;
	id: string;
}

interface ResourceMeta {
	referenceIds?: ReferenceId[];
	groupId: string;
	submissionId: string;
	submittedAt: string;
}

export interface RootState {
	version: string;
	samples?: SamplesState;
	samplings?: SamplingsState;
	samplingCollections?: SamplingCollectionsState;
	fields?: FieldsState;
	areas?: AreasState;
	offlineMaps?: OfflineMapsState;
	resources?: ResourcesState;
	groups?: GroupsState;
	auth?: any;
	reassign?: AsyncState;
	fieldsView?: FieldsViewState;
}

export interface Record {
	id: string;
}

export interface AsyncState {
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
}

export interface ResourceState extends AsyncState {
	records: Record[];
}

export interface Address {
	streetAddress: string;
	addressLocality: string;
	addressRegion: string;
	addressCountry: string;
	postalCode: string;
}

export interface ContactPoint {
	telephone: string;
	email: string;
	name: string;
	contactType: string;
	organization: string;
}

export interface Field extends Record {
	id: string;
	name: string;
	alternateName: string;
	producerName: string;
	address: Address;
	contactPoints: Array<ContactPoint>;
	areas: string[];
	meta: ResourceMeta;
}

export interface AreaProperties {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
	name?: string;
	description?: string;
	featureOfInterest?: string;
}

export interface Area extends Omit<Feature<Polygon, AreaProperties>, 'id'> {
	id: string;
	meta?: ResourceMeta;
}

export interface Algorithm {
	name?: string;
	alternateName?: string;
	codeRepository?: string;
	version?: string;
	doi?: string;
}

export interface StratificationInput {
	name?: string;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	value: string | number | Array<any>;
}

export interface StratificationResult {
	name?: string;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	value: string | number | Array<any>;
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface Location extends Omit<Feature<Point, any>, 'id'> {
	id: string;
	meta?: ResourceMeta;
}
export interface Stratification {
	id: string;
	name: string;
	agent: string;
	dateCreated: string;
	provider: string;
	object: string; // Area id
	featureOfInterest: string; // Field id
	algorithm: Algorithm;
	input: StratificationInput[];
	result: StratificationResult[];
	meta: ResourceMeta;
}

export interface LocationCollection {
	id: string;
	object: string;
	resultOf: string;
	featureOfInterest: string;
	features: string[];
	meta?: ResourceMeta;
}

export interface SoDepthValue {
	value: number;
	unit: string;
}

export interface SoDepth {
	minValue: SoDepthValue;
	maxValue: SoDepthValue;
}

export interface Sample {
	id: string;
	name: string; // bag id
	sampleOf: string; // Field id
	results: string[]; // lab result(s)
	resultOf: string; // Sampling id
	soDepth: SoDepth;
	meta?: ResourceMeta;
}

export interface Sampling {
	name?: string;
	id: string;
	resultTime: string;
	geometry: Point;
	featureOfInterest: string; // Location id
	memberOf?: string; // SamplingCollection id
	results?: string[]; // Sample ids
	comment?: string;
	procedures?: string[];
	properties: {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		[key: string]: any;
	};
	soDepth: SoDepth;
	meta?: ResourceMeta;
}

export interface SamplingCollection {
	name: string;
	id: string;
	comment?: string;
	creator?: string; // user that created
	featureOfInterest: string; // Field id (check)
	participants?: string[]; // not used yet
	object: string; // Location Collection id
	members: string[]; // Sampling ids
	soDepth: SoDepth;
	sampleDepths: SoDepth[];
	dateCreated?: string;
	dateModified?: string;
	meta?: ResourceMeta;
}

type SamplingCollectionStatus = 'SUBMITTED' | 'DRAFT' | null;
export interface LabResult {
	id: string;
	name: string;
	procedure: string;
	observedProperty: string;
	featureOfInterest: string;
	result: {
		value: number;
		unit: string;
	};
	resultTime?: string;
	phenomenonTime?: string;
}

export interface LabResultCollection {
	id: string;
	name: string;
	member: string[] | LabResult[];
}

export interface Project {
	id: string;
	name: string;
	description: string;
	contactPoint: ContactPoint | ContactPoint[];
	participant?: string[];
	object: string[];
}

export interface Producer {
	id: string;
	name?: string;
	fields: string[];
}

export interface RawGroup {
	id: string;
	name: string;
	path: string;
}

export interface Group extends RawGroup {
	level: number;
	children: Group[];
}

export type StatusState = 'posted' | 'stratified' | 'sampled';
export interface FieldStatus {
	state: StatusState;
	timestamp: string;
}

export type StatusOptions = StatusState | null;
