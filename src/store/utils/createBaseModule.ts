import { RootState, ResourceState, Record } from '@/store/types';
import { ActionTree, MutationTree, Module, GetterTree } from 'vuex';
import IDBS from '@/services/indexedDBService';

export function createBaseModule<T extends ResourceState, R extends Record>(
	storeName: string,
	createInitialState: () => T,
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	service: any = IDBS
): Module<T, RootState> {
	const actions: ActionTree<T, RootState> = {
		async setRecords({ commit }, payload: R[]): Promise<void> {
			try {
				await service.putRecords(storeName, payload);
				const records = await service.getAllRecords(storeName);
				// Make sure Vuex Store is in sync with IndexedDB, overwrite Store
				commit('setAllRecords', records);
			} catch (error) {
				console.warn('error', error);
			}
		},
		async setRecord({ commit }, payload: R): Promise<void> {
			try {
				await service.putRecord(storeName, payload);
				commit('setRecord', payload);
			} catch (error) {
				console.warn('error', error);
			}
		},
		async getRecord({ commit }, id: string): Promise<void> {
			commit('requestGetRecord');
			try {
				const result = await service.getRecord(storeName, id);
				commit('requestGetRecordSuccess', result);
			} catch (error) {
				commit('requestGetRecordError', error);
			}
		},
		async getRecords(_, ids: string[]): Promise<R[] | undefined> {
			try {
				return await service.getRecords(storeName, ids);
			} catch (error) {
				console.warn('error', error);
			}
		},
		async getRecordsByIdWithIndex(_, { id, indexName }: { id: string; indexName: string }): Promise<R[] | undefined> {
			try {
				return await service.getRecordsByIdWithIndex(storeName, id, indexName);
			} catch (error) {
				console.warn('error', error);
			}
		},
		async getRecordsByIdsWithIndex(
			_,
			{ ids, indexName }: { ids: string[]; indexName: string }
		): Promise<R[] | undefined> {
			try {
				return await service.getRecordsByIdsWithIndex(storeName, ids, indexName);
			} catch (error) {
				console.warn('error', error);
			}
		},
		async getAllRecords({ commit }): Promise<void> {
			commit('requestGetAllRecords');
			try {
				const records = await service.getAllRecords(storeName);
				commit('requestGetAllRecordsSuccess', records);
			} catch (error) {
				commit('requestGetAllRecordsError', error);
			}
		},
		async removeRecord({ commit }, id: string): Promise<void> {
			try {
				const res = await service.deleteRecord(storeName, id);
				commit('removeRecord', id);
				return res;
			} catch (error) {
				console.warn('error', error);
			}
		},
		async removeRecords({ commit }, ids: string[]): Promise<void> {
			try {
				const res = await service.deleteRecords(storeName, ids);
				commit('removeRecords', ids);
				return res;
			} catch (error) {
				console.warn('error', error);
			}
		},
		async clearRecords({ commit }): Promise<void> {
			try {
				await service.clearRecords(storeName);
				commit('clearRecords');
			} catch (error) {
				console.warn('error', error);
			}
		},
	};

	const mutations: MutationTree<T> = {
		requestGetRecord(state) {
			state.isLoading = true;
			state.isLoaded = false;
			state.error = null;
		},
		requestGetRecordSuccess(state, record: R) {
			state.isLoading = false;
			state.isLoaded = true;
			state.records = [record, ...state.records.filter(({ id }) => id !== record.id)];
		},
		requestGetRecordError(state, error) {
			state.isLoading = false;
			state.isLoaded = false;
			state.error = error;
		},
		requestGetAllRecords(state) {
			state.isLoading = true;
			state.isLoaded = false;
			state.error = null;
		},
		requestGetAllRecordsSuccess(state, records: R[]) {
			state.isLoading = false;
			state.isLoaded = true;
			state.records = records;
		},
		requestGetAllRecordsError(state, error) {
			state.isLoading = false;
			state.isLoaded = false;
			state.error = error;
		},
		setAllRecords(state, records: R[]) {
			state.records = records;
		},
		setRecord(state, record: R) {
			const index = state.records.findIndex(({ id }) => id === record.id);
			if (index < 0) {
				state.records = [...state.records, record];
			} else {
				state.records.splice(index, 1, record);
			}
		},
		removeRecord(state, id: string) {
			state.records = state.records.filter((r) => r.id !== id);
		},
		removeRecords(state, ids: string[]) {
			state.records = state.records.filter((r) => !ids.some((id) => id === r.id));
		},
		clearRecords(state) {
			state.records = [];
		},
	};

	const getters: GetterTree<T, RootState> = {
		getById: (state) => (recordId: string) => state.records.find(({ id }: Record) => recordId === id),
		getByIds: (state) => (recordIds: string[]) =>
			state.records.filter(({ id }: Record) => recordIds.some((recordId) => recordId === id)),
		getIsLoaded: (state) => state.isLoaded,
		getIsLoading: (state) => state.isLoading,
		getHasError: (state) => Boolean(state.error),
		getRecords: (state) => state.records,
	};

	return {
		namespaced: true,
		actions,
		state: createInitialState(),
		mutations,
		getters,
	};
}
