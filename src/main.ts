import './mapboxMonkeyPatch';

import Vue from 'vue';
import Vuex from 'vuex';
import VueCompositionAPI from '@vue/composition-api';
import App from './App.vue';
import './registerServiceWorker';
import createRouter from './router';
import createStore from '@/store';
import vuetify from './plugins/vuetify';
import SurveystackService from '@/services/surveystackService';
import soilApiService from '@/services/soilApiService';
import { injectRouter } from '@/store/plugins/redirects';

SurveystackService.init(process.env.VUE_APP_SURVEYSTACK_API_URL);

Vue.config.productionTip = false;

Vue.use(VueCompositionAPI);

Vue.use(Vuex);

const store = createStore();

const router = createRouter(store);

soilApiService.injectStore(store);
injectRouter(router);

new Vue({
	router,
	store,
	vuetify,
	render: (h) => h(App),
}).$mount('#app');

export { router };
