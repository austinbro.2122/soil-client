import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

export default new Vuetify({
	breakpoint: {
		thresholds: {
			sm: 768,
			md: 840,
			lg: 1366,
		},
	},
});
