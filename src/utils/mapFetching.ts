import { fromLatLon, toLatLon } from 'utm';
import { bbox, center, AllGeoJSON, BBox, featureCollection } from '@turf/turf';
import { Area } from '@/store/types';
import config from '@/configuration';

interface XYZ {
	x: number; // tile x coordinate
	y: number; // tile y coordinate
	z: number; // zoom
}

const OFFLINE_MAPS_CACHE_NAME = 'offline-maps-runtime';
const MAPBOX_API_TOKEN = process.env.VUE_APP_MAPBOX_TOKEN as string;
const MAPBOX_API = 'api.mapbox.com';
export const MAPBOX_API_TILES_PATH = `${MAPBOX_API}/v4/mapbox.satellite`;
const MAPBOX_API_STYLES_PATH = `${MAPBOX_API}/styles/v1/mapbox`;
const MAPBOX_API_FONTS_PATH = `${MAPBOX_API}/fonts/v1/mapbox`;
const accessTokenURLParam = 'access_token';

export function lon2tileX(lon: number, zoom: number) {
	const tileX = Math.floor(((lon + 180) / 360) * Math.pow(2, zoom));
	if (Math.abs(tileX) === Infinity) {
		throw new Error('lon2tileX resulted in Infinity');
	}
	return tileX;
}

export function lat2tileY(lat: number, zoom: number) {
	const tileY = Math.floor(
		((1 - Math.log(Math.tan((lat * Math.PI) / 180) + 1 / Math.cos((lat * Math.PI) / 180)) / Math.PI) / 2) *
			Math.pow(2, zoom)
	);
	if (Math.abs(tileY) === Infinity) {
		throw new Error('lon2tileY resulted in Infinity');
	}
	return tileY;
}

export function range(a: number, b: number) {
	const [start, end] = [a, b].sort((a, b) => a - b);
	const length = Math.abs(end - start);
	return [...Array(length + 1).keys()].map((x) => x + start);
}

function UTMFromLatLon(bounds: number[]) {
	const [minLon, minLat, maxLon, maxLat] = bounds;

	/*
		Get UTM coordinates for 2 points, forcing the 2nd point to have coordinates relative to the zone number of the first.
		This allows us to do arithmetic with the UTM coordinates even if the points are in different UTM zones.
	*/
	const { easting: minXUTM, northing: minYUTM, zoneNum, zoneLetter: minZoneLetter } = fromLatLon(minLat, minLon);
	const { easting: maxXUTM, northing: maxYUTM, zoneLetter: maxZoneLetter } = fromLatLon(maxLat, maxLon, zoneNum);

	return {
		minXUTM,
		minYUTM,
		maxXUTM,
		maxYUTM,
		zoneNum,
		minZoneLetter,
		maxZoneLetter,
	};
}

export function padBounds(bounds: BBox): BBox {
	const { minXUTM, minYUTM, maxXUTM, maxYUTM, zoneNum, minZoneLetter, maxZoneLetter } = UTMFromLatLon(bounds);

	const dxUTM = maxXUTM - minXUTM;
	const dyUTM = maxYUTM - minYUTM;

	const padFactor = config.map.padBoundsPercentage;

	let paddedMinXUTM, paddedMaxXUTM, paddedMinYUTM, paddedMaxYUTM;

	if (dxUTM > dyUTM) {
		paddedMinXUTM = minXUTM - dxUTM * padFactor;
		paddedMaxXUTM = maxXUTM + dxUTM * padFactor;
		const paddedDx = paddedMaxXUTM - paddedMinXUTM;
		paddedMinYUTM = minYUTM + dyUTM / 2 - paddedDx / 2;
		paddedMaxYUTM = minYUTM + dyUTM / 2 + paddedDx / 2;
	} else {
		paddedMinYUTM = minYUTM - dyUTM * padFactor;
		paddedMaxYUTM = maxYUTM + dyUTM * padFactor;
		const paddedDy = paddedMaxYUTM - paddedMinYUTM;
		paddedMinXUTM = minXUTM + dxUTM / 2 - paddedDy / 2;
		paddedMaxXUTM = minXUTM + dxUTM / 2 + paddedDy / 2;
	}

	const { latitude: paddedMinLat, longitude: paddedMinLon } = toLatLon(
		paddedMinXUTM,
		paddedMinYUTM,
		zoneNum,
		minZoneLetter
	);
	const { latitude: paddedMaxLat, longitude: paddedMaxLon } = toLatLon(
		paddedMaxXUTM,
		paddedMaxYUTM,
		zoneNum,
		maxZoneLetter
	);

	return [paddedMinLon, paddedMinLat, paddedMaxLon, paddedMaxLat];
}

export function calculateZoomToFitBounds(width: number, height: number, bounds: number[], bcenter: number[]) {
	const { minXUTM, minYUTM, maxXUTM, maxYUTM } = UTMFromLatLon(bounds);

	const dxUTM = maxXUTM - minXUTM;
	const dyUTM = maxYUTM - minYUTM;

	const sizeX = width;
	const sizeY = height;

	const scaleX = sizeX / dxUTM;
	const scaleY = sizeY / dyUTM;

	const resolution = scaleX > scaleY ? dyUTM / sizeY : dxUTM / sizeX;
	const averageRadiusOfEarth = 6371008.8;
	const C = averageRadiusOfEarth * 2 * Math.PI;
	const bcenterLat = bcenter[1];

	const mapboxTileSizePx = 256;
	const zoom = Math.log((C * Math.abs(Math.cos(bcenterLat))) / (resolution * mapboxTileSizePx)) / Math.LN2;

	return zoom;
}

export function getTileXYZPairs(bounds: number[], zoom: number): XYZ[] {
	const [minLon, minLat, maxLon, maxLat] = bounds;

	// lat2tileY inverts min and max lat
	const minY = lat2tileY(maxLat, zoom);
	const maxY = lat2tileY(minLat, zoom);

	const minX = lon2tileX(minLon, zoom);
	const maxX = lon2tileX(maxLon, zoom);

	const xRange = range(minX, maxX);
	const yRange = range(minY, maxY);

	const pairs: XYZ[] = [];
	xRange.forEach((x) => {
		yRange.forEach((y) => pairs.push({ x, y, z: zoom }));
	});

	return pairs;
}

export function xyzToTileURL({ x, y, z }: XYZ) {
	const tokenString = new URLSearchParams({ [accessTokenURLParam]: MAPBOX_API_TOKEN }).toString();
	// https://github.com/mapbox/mapbox-gl-js/blob/ef7b2cb243f983fef7482502021e26bda93896e2/src/source/raster_tile_source.js#L113
	const use2x = window.devicePixelRatio >= 2;
	return `https://${MAPBOX_API_TILES_PATH}/${z}/${x}/${y}${use2x ? '@2x' : ''}.webp?${tokenString}`;
}

export async function getCachedXYZs(): Promise<XYZ[]> {
	const cache = await window.caches.open(OFFLINE_MAPS_CACHE_NAME);
	const cachedRequests = await cache.keys();
	const cachedRequestUrls = cachedRequests.map((req) => req.url);
	return (
		cachedRequestUrls
			.filter((url) => url.includes(`${MAPBOX_API_TILES_PATH}/`))
			.map((url) => url.split('mapbox.satellite/')[1].split('.webp')[0].split('/'))
			// y can be a string like '400@2x'. parseInt ignores the '@2x' and returns 400, so this doesn't cause a bug.
			// It does make this function indifferent to whether assets are 2x or not, though.
			// In the future, this function should probably return whether cached tiles are 2x or not, as the current behavior is not obvious.
			.map(([z, x, y]) => {
				return { x: parseInt(x, 10), y: parseInt(y, 10), z: parseInt(z, 10) };
			})
	);
}

export function getRequiredXYZs(area: Area): XYZ[] {
	const bounds = bbox(area);
	const boundsCenter = center(area).geometry?.coordinates ?? [0, 0];
	const paddedBounds = padBounds(bounds);
	const zoomToFitBounds = calculateZoomToFitBounds(
		window.innerWidth,
		Math.floor(window.innerHeight * 0.6),
		paddedBounds,
		boundsCenter
	);
	const flooredZoom = Math.floor(zoomToFitBounds);
	// skip every other zoom level to save some cache storage, as map will use tiles where zoom=N at zoom level N+1 if no zoom=N+1 tiles are available
	const extraZoomLevels = [...Array(config.map.offline.extraZoomLevels).keys()].map((i) => flooredZoom + 2 * (i + 1));
	const zoomLevels = [flooredZoom, ...extraZoomLevels];
	return zoomLevels.map((z) => getTileXYZPairs(paddedBounds, z)).flat();
}

async function areAreaTilesAvailableOffline(area: Area): Promise<boolean> {
	let cachedXYZs: XYZ[];
	try {
		cachedXYZs = await getCachedXYZs();
	} catch (e) {
		return false;
	}
	const requiredXYZs = getRequiredXYZs(area);
	return requiredXYZs.every(({ x, y, z }) =>
		cachedXYZs.some(({ x: cx, y: cy, z: cz }) => cx === x && cy === y && cz === z)
	);
}

// get map options suitable for a single field that may be available offline
// - restrict max bounds to area available offline
export function getMapOptionsForField(area: AllGeoJSON) {
	const bounds = bbox(area);
	const boundsCenter = center(area).geometry?.coordinates ?? [0, 0];
	const paddedBounds = padBounds(bounds);
	const zoomToFitBounds = calculateZoomToFitBounds(
		window.innerWidth,
		Math.floor(window.innerHeight * 0.6),
		paddedBounds,
		boundsCenter
	);

	const [minLon, minLat, maxLon, maxLat] = paddedBounds;

	const options = {
		maxBounds: [
			[minLon, minLat],
			[maxLon, maxLat],
		],
		zoom: zoomToFitBounds,
		center: boundsCenter,
	};
	return options;
}

// TODO: This doesn't have to do with map fetching (the name of this file),
// but is similar to getMapOptionsForField above.
// There is probably a better way to organize these functions
export function getMapOptionsForFieldsView(areas: Area[]) {
	const areasCollection = featureCollection(areas);
	const bounds = bbox(areasCollection);
	// This case protects against areas with nonsensical coordinates like [0, 0]
	if (bounds.includes(0) || bounds.includes(Infinity) || bounds.includes(-Infinity)) {
		return {
			maxBounds: null,
			zoom: 9,
			center: [-84.512016, 39.103119],
		};
	}
	const boundsCenter = center(areasCollection).geometry?.coordinates ?? [-84.512016, 39.103119];
	const zoomToFitBounds = calculateZoomToFitBounds(
		// TODO: make fieldsView map dimensions accurate.
		// These are just initial estimates to start
		window.innerWidth / 2,
		Math.floor(window.innerHeight * 0.9),
		bounds,
		boundsCenter
	);

	const options = {
		maxBounds: null,
		// TODO: investigate why calculateZoomToFitBounds returns NaN when given very large bounds (e.g. they span multiple continents)
		zoom: isNaN(zoomToFitBounds) ? 1 : zoomToFitBounds,
		center: boundsCenter,
	};
	return options;
}

export const requiredMapResourceURLs = (use2x = false) => [
	`https://${MAPBOX_API_TILES_PATH}.json?${new URLSearchParams({
		secure: '',
		[accessTokenURLParam]: MAPBOX_API_TOKEN,
	}).toString()}`,
	`https://${MAPBOX_API_STYLES_PATH}/${config.mapbox.tileset}?${new URLSearchParams({
		[accessTokenURLParam]: MAPBOX_API_TOKEN,
	}).toString()}`,
	`https://${MAPBOX_API_STYLES_PATH}/${config.mapbox.tileset}/sprite${use2x ? '@2x' : ''}.json?${new URLSearchParams({
		[accessTokenURLParam]: MAPBOX_API_TOKEN,
	}).toString()}`,
	`https://${MAPBOX_API_STYLES_PATH}/${config.mapbox.tileset}/sprite${use2x ? '@2x' : ''}.png?${new URLSearchParams({
		[accessTokenURLParam]: MAPBOX_API_TOKEN,
	}).toString()}`,
	`https://${MAPBOX_API_FONTS_PATH}/Open%20Sans%20Regular,Arial%20Unicode%20MS%20Regular/0-255.pbf?${new URLSearchParams(
		{ [accessTokenURLParam]: MAPBOX_API_TOKEN }
	).toString()}`,
];

export async function areRequiredMapResourcesAvailableOffline(): Promise<boolean> {
	const cache = await window.caches.open(OFFLINE_MAPS_CACHE_NAME);
	const cachedRequests = await cache.keys();
	const cachedRequestUrls = cachedRequests.map((req) => req.url);
	const use2x = window.devicePixelRatio >= 2;
	return requiredMapResourceURLs(use2x).every((url) => cachedRequestUrls.includes(url));
}

export async function isAreaAvailableOffline(area: Area): Promise<boolean> {
	const hasTiles = await areAreaTilesAvailableOffline(area);
	const hasResources = await areRequiredMapResourcesAvailableOffline();
	return hasTiles && hasResources;
}

export async function fetchCommonOfflineMapResources() {
	const commonResourceURLs = requiredMapResourceURLs(window.devicePixelRatio >= 2);
	return Promise.all(commonResourceURLs.map((url) => fetch(url)));
}

export async function fetchOfflineMapResources(area: Area) {
	const tileURLs = getRequiredXYZs(area).map(xyzToTileURL);
	const use2x = window.devicePixelRatio >= 2;
	return Promise.all([...tileURLs, ...requiredMapResourceURLs(use2x)].map((url) => fetch(url)));
}
