import {
	lon2tileX,
	lat2tileY,
	range,
	xyzToTileURL,
	getCachedXYZs,
	requiredMapResourceURLs,
	areRequiredMapResourcesAvailableOffline,
	fetchCommonOfflineMapResources,
	fetchOfflineMapResources,
	MAPBOX_API_TILES_PATH,
} from './mapFetching';
import { createMockArea } from '../../tests/mockGenerators';

describe('mapFetching utils', () => {
	describe('lon2tileX', () => {
		it('output matches snapshot', () => {
			expect(lon2tileX(-84.34167480138744, 12)).toMatchSnapshot();
			expect(lon2tileX(-84.34167480138744, 5)).toMatchSnapshot();
			expect(lon2tileX(-121.95012663384324, 9)).toMatchSnapshot();
			expect(lon2tileX(-121.95012663384324, 11)).toMatchSnapshot();
		});
	});

	describe('lat2tileY', () => {
		it('output matches snapshot', () => {
			expect(lat2tileY(45.38752835274175, 12)).toMatchSnapshot();
			expect(lat2tileY(45.38752835274175, 5)).toMatchSnapshot();
			expect(lat2tileY(42.77909705916853, 9)).toMatchSnapshot();
			expect(lat2tileY(42.77909705916853, 11)).toMatchSnapshot();
		});
	});

	describe('range', () => {
		it('returns range', () => {
			expect(range(1, 4)).toEqual([1, 2, 3, 4]);
		});

		it('returns the same result regardless of input order', () => {
			expect(range(1, 10)).toEqual(range(10, 1));
		});

		it('returns range inclusive of both input values', () => {
			const result = range(1, 10);

			expect(result).toContain(1);
			expect(result).toContain(10);
		});

		it('handles negatives inputs', () => {
			expect(range(-2, 1)).toEqual([-2, -1, 0, 1]);
			expect(range(-4, -2)).toEqual([-4, -3, -2]);
		});
	});

	describe('xyzToTileUrl', () => {
		it('requests 2x assets when devicePixelRatio >= 2', () => {
			global.devicePixelRatio = 2;
			expect(xyzToTileURL({ x: 1, y: 1, z: 1 })).toContain('@2x');
		});

		it('does not request 2x assets when devicePixelRatio < 2', () => {
			global.devicePixelRatio = 1.9;
			expect(xyzToTileURL({ x: 1, y: 1, z: 1 })).not.toContain('@2x');
		});
	});

	describe('getCachedXYZs', () => {
		it('returns XYZs for tile urls in the cache', async () => {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(global.caches as any) = {
				open: () =>
					Promise.resolve({
						keys: () =>
							Promise.resolve([
								{ url: 'https://api.mapbox.com/v4/mapbox.satellite/14/2644/6050@2x.webp?access_token=ABC123' },
								{ url: 'https://api.mapbox.com/v4/mapbox.satellite/12/660/1511.webp?access_token=ABC123' },
								{ url: 'https://api.mapbox.com/something/else' },
							]),
					}),
			};

			const result = await getCachedXYZs();

			expect(result).toEqual([
				{ x: 2644, y: 6050, z: 14 },
				{ x: 660, y: 1511, z: 12 },
			]);
		});
	});

	describe('areRequiredMapResourcesAvailableOffline', () => {
		it('returns true if required resource urls are in the cache', async () => {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(global.caches as any) = {
				open: () =>
					Promise.resolve({
						keys: () => Promise.resolve(requiredMapResourceURLs().map((url) => ({ url }))),
					}),
			};
			global.devicePixelRatio = 1;

			expect(await areRequiredMapResourcesAvailableOffline()).toBe(true);
		});

		it('returns false if required resource urls in the cache are 2x when devicePixelRatio is < 2', async () => {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(global.caches as any) = {
				open: () =>
					Promise.resolve({
						keys: () => Promise.resolve(requiredMapResourceURLs(true).map((url) => ({ url }))),
					}),
			};
			global.devicePixelRatio = 1;

			expect(await areRequiredMapResourcesAvailableOffline()).toBe(false);
		});

		it('returns true if required resource urls in the cache are 2x when devicePixelRatio >= 2', async () => {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(global.caches as any) = {
				open: () =>
					Promise.resolve({
						keys: () => Promise.resolve(requiredMapResourceURLs(true).map((url) => ({ url }))),
					}),
			};
			global.devicePixelRatio = 2;

			expect(await areRequiredMapResourcesAvailableOffline()).toBe(true);
		});

		it('returns false if required resource urls in the cache are not 2x when devicePixelRatio is >= 2', async () => {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			(global.caches as any) = {
				open: () =>
					Promise.resolve({
						keys: () => Promise.resolve(requiredMapResourceURLs().map((url) => ({ url }))),
					}),
			};
			global.devicePixelRatio = 2;

			expect(await areRequiredMapResourcesAvailableOffline()).toBe(false);
		});
	});

	describe('fetchCommonOfflineMapResources', () => {
		beforeEach(() => {
			global.fetch = jest.fn();
		});

		it('fetches 2x resources when devicePixelRatio >= 2', async () => {
			global.devicePixelRatio = 2;

			await fetchCommonOfflineMapResources();

			expect((global.fetch as jest.Mock).mock.calls.some(([url]) => url.indexOf('@2x') !== -1)).toBe(true);
		});

		it('fetches non 2x resources when devicePixelRatio < 2', async () => {
			global.devicePixelRatio = 1;

			await fetchCommonOfflineMapResources();

			expect((global.fetch as jest.Mock).mock.calls.every(([url]) => url.indexOf('@2x') === -1)).toBe(true);
		});

		it('fetches required map resources', async () => {
			global.devicePixelRatio = 1;

			await fetchCommonOfflineMapResources();

			const requiredURLs = requiredMapResourceURLs();
			requiredURLs.forEach((requiredURL) => {
				expect((global.fetch as jest.Mock).mock.calls).toContainEqual([requiredURL]);
			});
		});
	});

	describe('fetchOfflineMapResources', () => {
		beforeEach(() => {
			global.fetch = jest.fn();
		});

		it('fetches 2x resources when devicePixelRatio >= 2', async () => {
			const area = createMockArea();
			global.devicePixelRatio = 2;

			await fetchOfflineMapResources(area);

			expect((global.fetch as jest.Mock).mock.calls.some(([url]) => url.indexOf('@2x') !== -1)).toBe(true);
		});

		it('fetches non 2x resources when devicePixelRatio < 2', async () => {
			const area = createMockArea();
			global.devicePixelRatio = 1;

			await fetchOfflineMapResources(area);

			expect((global.fetch as jest.Mock).mock.calls.every(([url]) => url.indexOf('@2x') === -1)).toBe(true);
		});

		it('fetches tiles', async () => {
			const area = createMockArea();

			await fetchOfflineMapResources(area);

			expect((global.fetch as jest.Mock).mock.calls).toContainEqual([
				expect.stringContaining(`${MAPBOX_API_TILES_PATH}/`),
			]);
		});

		it('fetches required map resources', async () => {
			const area = createMockArea();
			global.devicePixelRatio = 1;

			await fetchOfflineMapResources(area);

			const requiredURLs = requiredMapResourceURLs();
			requiredURLs.forEach((requiredURL) => {
				expect((global.fetch as jest.Mock).mock.calls).toContainEqual([requiredURL]);
			});
		});
	});
});
