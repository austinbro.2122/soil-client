import { openDB, deleteDB } from 'idb';

const DB_NAME = 'SoilApp';

const dbPromise = () =>
	openDB(DB_NAME, 1, {
		upgrade(db) {
			db.createObjectStore('samplingCollections', { keyPath: 'id' });

			const samplingsStore = db.createObjectStore('samplings', {
				keyPath: 'id',
			});
			samplingsStore.createIndex('location', 'featureOfInterest', {
				unique: false,
			});
			db.createObjectStore('samples', { keyPath: 'id' });
		},
	});

export async function putRecords<T>(storeName: string, data: Array<T>) {
	try {
		const db = await dbPromise();
		const tx = db.transaction(storeName, 'readwrite');
		const store = tx.objectStore(storeName);
		data.forEach((d) => {
			store.put(d);
		});
		return tx.done;
	} catch (error) {
		return error;
	}
}

export async function getAllRecords(storeName: string) {
	const db = await dbPromise();
	const tx = db.transaction(storeName, 'readwrite');
	const store = tx.objectStore(storeName);
	return store.getAll();
}

export async function getRecordsByIdWithIndex(storeName: string, id: string, indexName: string) {
	const db = await dbPromise();

	return db.getAllFromIndex(storeName, indexName, id);
}

export async function getRecordsByIdsWithIndex(storeName: string, ids: string[], indexName: string) {
	try {
		const db = await dbPromise();
		const records = [];

		let cursor = await db.transaction(storeName).store.index(indexName).openCursor();
		while (cursor) {
			if (ids.some((id: string) => id === cursor?.key)) {
				records.push({ ...cursor.value });
			}
			cursor = await cursor.continue();
		}
		return records;
	} catch (error) {
		return error;
	}
}

export async function getRecords(storeName: string, ids: string[]) {
	try {
		const db = await dbPromise();
		const tx = db.transaction(storeName, 'readwrite');
		const store = tx.objectStore(storeName);
		return Promise.all(ids.map((id) => store.get(id)));
	} catch (error) {
		console.warn('error', new Error(String(error)));
		return error;
	}
}

export async function getRecord(storeName: string, id: string) {
	return (await dbPromise()).get(storeName, id);
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function putRecord(storeName: string, value: any, key?: string) {
	return (await dbPromise()).put(storeName, value, key);
}

export async function deleteRecord(storeName: string, key: string) {
	return (await dbPromise()).delete(storeName, key);
}

export async function deleteRecords(storeName: string, keys: string[]) {
	try {
		const db = await dbPromise();
		const tx = db.transaction(storeName, 'readwrite');
		const store = tx.objectStore(storeName);
		keys.forEach((k) => {
			store.delete(k);
		});
		return tx.done;
	} catch (error) {
		return error;
	}
}

export async function clearRecords(storeName: string) {
	try {
		const db = await dbPromise();
		const tx = db.transaction(storeName, 'readwrite');
		const store = tx.objectStore(storeName);
		return store.clear();
	} catch (error) {
		console.warn('error', error);
	}
}

export async function clearAllStores() {
	const db = await dbPromise();
	const storeNames = [...db.objectStoreNames];
	const tx = db.transaction(storeNames, 'readwrite');
	return Promise.all(storeNames.map((storeName) => tx.objectStore(storeName).clear()));
}

export async function deleteDb(successCb: () => void, errorCb: () => void) {
	return deleteDB(DB_NAME, {
		blocked() {
			console.warn('delete db blocked');
			errorCb();
		},
	}).then(() => successCb());
}

export default {
	getAllRecords,
	getRecords,
	putRecords,
	getRecord,
	clearRecords,
	putRecord,
	deleteRecord,
	deleteRecords,
	getRecordsByIdWithIndex,
	getRecordsByIdsWithIndex,
};
