import axios from 'axios';
import { Store } from 'vuex';
import { RootState } from '@/store/types';

let _store: Store<RootState>;

const getAllResources = async () => {
	const response = await axios.get(`${process.env.VUE_APP_SOIL_API_URL}/resources`, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});
	return response?.data;
};

const submitSamplingCollection = async (populatedSamplingCollection: object, groupId: string) => {
	const response = await axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/sampling-collections?groupId=${groupId}`,
		populatedSamplingCollection,
		{
			headers: {
				'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
			},
		}
	);
	return response?.data;
};

const getGroups = async () => {
	const response = await axios.get(`${process.env.VUE_APP_SOIL_API_URL}/groups`, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});
	return response?.data;
};

const reassign = async ({ groupId, fieldId }: { groupId: string; fieldId: string }) =>
	await axios.patch(
		`${process.env.VUE_APP_SOIL_API_URL}/reassign`,
		{ groupId, fieldId },
		{
			headers: {
				'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
			},
		}
	);

const injectStore = (store: Store<RootState>) => {
	_store = store;
};

const soilApiService = {
	injectStore,
	getAllResources,
	submitSamplingCollection,
	getGroups,
	reassign,
};

export default soilApiService;
