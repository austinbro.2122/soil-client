/* eslint-disable @typescript-eslint/no-explicit-any */

const AUTH_STATUS_KEY = 'auth_status';
const AUTH_USER_KEY = 'auth_user';
const AUTH_HEADER_KEY = 'auth_header';
const USER_MEMBERSHIP_KEY = 'user_memberships';
const USER_MEMBERSHIP_STATUS_KEY = 'user_memberships_status';
const USER_ACTIVE_GROUP_KEY = 'user_active_group';

const AuthService: any = {
	getStatus() {
		return localStorage.getItem(AUTH_STATUS_KEY) || '';
	},
	saveStatus(status: any) {
		localStorage.setItem(AUTH_STATUS_KEY, status);
	},
	getUser() {
		return JSON.parse(localStorage.getItem(AUTH_USER_KEY) as any) || {};
	},
	saveUser(user: any) {
		localStorage.setItem(AUTH_USER_KEY, JSON.stringify(user));
	},
	getHeader() {
		return localStorage.getItem(AUTH_HEADER_KEY) || '';
	},
	saveHeader(header: any) {
		localStorage.setItem(AUTH_HEADER_KEY, header);
	},
	clear() {
		localStorage.removeItem(AUTH_STATUS_KEY);
		localStorage.removeItem(AUTH_USER_KEY);
		localStorage.removeItem(AUTH_HEADER_KEY);
	},
};

const MembershipService: any = {
	getStatus() {
		return localStorage.getItem(USER_MEMBERSHIP_STATUS_KEY) || '';
	},
	saveStatus(status: any) {
		localStorage.setItem(USER_MEMBERSHIP_STATUS_KEY, status);
	},
	getUserMemberships() {
		return JSON.parse(localStorage.getItem(USER_MEMBERSHIP_KEY) as any) || [];
	},
	saveMemberships(memberships = []) {
		localStorage.setItem(USER_MEMBERSHIP_KEY, JSON.stringify(memberships));
	},
	clear() {
		localStorage.removeItem(USER_MEMBERSHIP_STATUS_KEY);
		localStorage.removeItem(USER_MEMBERSHIP_KEY);
	},
};

const GroupService: any = {
	saveActiveGroup(group: any) {
		localStorage.setItem(USER_ACTIVE_GROUP_KEY, JSON.stringify(group));
	},
	getActiveGroup() {
		return JSON.parse(localStorage.getItem(USER_ACTIVE_GROUP_KEY) as any) || null;
	},
	clear() {
		localStorage.removeItem(USER_ACTIVE_GROUP_KEY);
	},
};

export { AuthService, MembershipService, GroupService };
