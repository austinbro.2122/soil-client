import axios from 'axios';
import soilApiService from './soilApiService';
import createTestStore from '../../tests/createTestStore';

jest.mock('axios');

describe('soilApiService', () => {
	beforeEach(() => {
		const store = createTestStore();
		soilApiService.injectStore(store);
		store.replaceState({
			...store.state,
			auth: {
				...store.state.auth,
				header: 'mock header value',
			},
		});
	});

	describe('getAllResources', () => {
		it("calls the Soil API's GET /resources endpoint with X-Authorization header", async () => {
			await soilApiService.getAllResources();

			expect(axios.get).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/resources`, {
				headers: { 'X-Authorization': 'mock header value' },
			});
		});

		it("returns resources from the response from the Soil API's GET /resources endpoint", async () => {
			(axios.get as jest.Mock).mockResolvedValueOnce({
				data: {
					fields: [],
					areas: [],
					stratifications: [],
					locationCollections: [],
					locations: [],
					samples: [],
					samplings: [],
					samplingCollections: [],
				},
			});

			const actual = await soilApiService.getAllResources();

			expect(actual).toEqual({
				fields: [],
				areas: [],
				stratifications: [],
				locationCollections: [],
				locations: [],
				samples: [],
				samplings: [],
				samplingCollections: [],
			});
		});
	});

	describe('submitSamplingCollection', () => {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let actual: any;

		beforeEach(async () => {
			(axios.post as jest.Mock).mockResolvedValueOnce({
				data: {
					samples: [],
					samplings: [],
					samplingCollections: [],
				},
			});

			actual = await soilApiService.submitSamplingCollection({}, '123');
		});

		it("calls the Soil API's POST /sampling-collections endpoint with X-Authorization header", () => {
			expect((axios.post as jest.Mock).mock.calls[0][2]).toEqual({
				headers: { 'X-Authorization': 'mock header value' },
			});
		});

		it("calls the Soil API's POST /sampling-collections endpoint groupId query parameter", () => {
			expect((axios.post as jest.Mock).mock.calls[0][0]).toEqual(
				`${process.env.VUE_APP_SOIL_API_URL}/sampling-collections?groupId=123`
			);
		});

		it("calls the Soil API's POST /sampling-collections endpoint, passing the populatedSamplingCollection as the POST body", () => {
			expect((axios.post as jest.Mock).mock.calls[0][1]).toEqual({});
		});

		it("returns resources from the response from the Soil API's POST /sampling-collections endpoint", () => {
			expect(actual).toEqual({
				samples: [],
				samplings: [],
				samplingCollections: [],
			});
		});
	});

	describe('getGroups', () => {
		it("calls the Soil API's GET /groups endpoint with X-Authorization header", async () => {
			await soilApiService.getGroups();

			expect(axios.get).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/groups`, {
				headers: { 'X-Authorization': 'mock header value' },
			});
		});

		it("returns groups from the response from the Soil API's GET /resources endpoint", async () => {
			(axios.get as jest.Mock).mockResolvedValueOnce({
				data: [
					{ id: 'mock group id 1', name: 'mock group 1', path: '/mock-group-1/' },
					{ id: 'mock group id 2', name: 'mock group 2', path: '/mock-group-2/' },
				],
			});

			const actual = await soilApiService.getGroups();

			expect(actual).toEqual([
				{ id: 'mock group id 1', name: 'mock group 1', path: '/mock-group-1/' },
				{ id: 'mock group id 2', name: 'mock group 2', path: '/mock-group-2/' },
			]);
		});
	});

	describe('reassign', () => {
		beforeEach(async () => await soilApiService.reassign({ groupId: 'mock group id', fieldId: 'mock field id' }));

		it("calls the Soil API's PATCH /reassign endpoint", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/reassign`,
				expect.anything(),
				expect.anything()
			);
		});

		it("calls the Soil API's PATCH /reassign endpoint with X-Authorization header", async () => {
			expect(axios.patch).toHaveBeenCalledWith(`${process.env.VUE_APP_SOIL_API_URL}/reassign`, expect.anything(), {
				headers: { 'X-Authorization': 'mock header value' },
			});
		});

		it("calls the Soil API's PATCH /reassign endpoint with body containing groupId and fieldId", async () => {
			expect(axios.patch).toHaveBeenCalledWith(
				`${process.env.VUE_APP_SOIL_API_URL}/reassign`,
				{ groupId: 'mock group id', fieldId: 'mock field id' },
				expect.anything()
			);
		});
	});
});
