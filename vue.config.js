module.exports = {
	transpileDependencies: ['vuetify'],
	// enable https for testing geolocation locally
	// devServer: {
	// 	open: process.platform === 'darwin',
	// 	host: '0.0.0.0',
	// 	port: 8080,
	// 	https: true,
	// 	hotOnly: false,
	// },
	pwa: {
		name: 'soilstack.io',
		themeColor: '#35b45f',
		iconPaths: {
			favicon32: 'img/icons/favicon-32x32.png',
			favicon16: 'img/icons/favicon-16x16.png',
			appleTouchIcon: 'img/icons/apple-touch-icon.png',
			maskIcon: null,
			msTileImage: 'img/icons/msapplication-icon-144x144.png',
		},
		workboxPluginMode: 'GenerateSW',
		workboxOptions: {
			clientsClaim: true,
			skipWaiting: true,
			runtimeCaching: [
				{
					urlPattern: (context) =>
						[
							'cwed398ql9.execute-api.us-east-1.amazonaws.com',
							'nz6tet9zfh.execute-api.us-east-1.amazonaws.com',
							'c5m14nm4m1.execute-api.us-east-1.amazonaws.com',
							'test.api.soilstack.io',
							'stage.api.soilstack.io',
							'api.soilstack.io',
						].includes(context.url.host),
					handler: 'NetworkFirst',
				},
				{
					urlPattern: (context) => context.url.host.indexOf('cdn.jsdelivr.net') !== -1,
					handler: 'CacheFirst',
					options: {
						matchOptions: {
							ignoreSearch: true,
							ignoreMethod: false,
							ignoreVary: false,
						},
					},
				},
				{
					urlPattern: (context) => context.url.host.indexOf('api.mapbox.com') !== -1,
					handler: 'CacheFirst',
					options: {
						matchOptions: {
							ignoreSearch: true,
							ignoreMethod: false,
							ignoreVary: false,
						},
						cacheName: 'offline-maps-runtime',
						expiration: {
							maxAgeSeconds: 60 * 60 * 24 * 30, // 30 days
						},
					},
				},
			],
		},
	},
};
